package com.tiantian.leader.service;

import com.tiantian.leader.application.Result;

/**
 *
 */
public interface AdviceService {
    Result userAdvice(String userId, String content);
}
