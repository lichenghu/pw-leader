package com.tiantian.leader.service;

import com.tiantian.leader.application.Result;
import com.tiantian.leader.model.Leader;
import com.tiantian.leader.model.UserProps;

import java.util.List;

/**
 *
 */
public interface UserService {
    Result createUser(String mobile, String pwd, String code, String deviceToken, String platform);
    Leader getLeaderByMobile(String mobile);
    boolean updateTokenByMobile(String mobile, String token);
    Result sendRegisterCode(String mobile);
    Result sendUpdatePwdCode(String mobile);
    Result updatePwd(String mobile, String pwd, String code);
    Result updateNickName(String userId, String nickName);
    Result updateEmail(String leaderId, String email);
    Result updateAvatarUrl(String leaderId, String url);
    Result getVersion(String version, String platform, String status);
    boolean resetUserVip(String leaderId);
    Leader getLeaderById(String leaderId);
    Result vipInfo(String leaderId);
    List<UserProps> getUserProps(String leaserId);
    Result giveProps(String leaderId, String otherUserId, String propCode, int num);
}
