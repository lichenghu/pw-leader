package com.tiantian.leader.service;

import com.tiantian.leader.model.IosPurchase;
import com.tiantian.leader.model.Order;

import java.util.List;

/**
 *
 */
public interface PayService {
    List<IosPurchase> iosPurchase();

    IosPurchase getPurchase(String code);

    Order saveOrder(Order order);

    boolean updateOrderPaySucc(String orderId);

    String orderSend(Order order);
}
