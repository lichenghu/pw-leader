package com.tiantian.leader.service;

import com.tiantian.leader.application.Result;

/**
 *
 */
public interface GroupService {
   Result getBuyInList();

   Result createGroup(int buyIndex, double hours, boolean safe, String leaderId);

    Result getNotEndGroups(String leaderId);

    Result getEndGroups(String leaderId, long lastCreateDate, int count);

    Result startGroupGame(String groupId, String leaderId, String leaderName);

    Result groupUsers(String groupId);

    Result getApplications(String groupId);

    Result getAllApplications(String leaderId);

    Result getAppCounts(String leaderId);

    Result agreeBuyIn(String groupId, String userId, String leaderId, String leaderName);

    Result getAllOdds();

    Result getScoreboard(String groupId);

    Result forceStandUp(String groupId, String userId, String $leaderId);

    Result forceCloseGame(String groupId, String $leaderId);

    Result getAddMoneyInfo(String groupId, String userId, String $leaderId);

    Result addUserMoney(String groupId, String userId, String $leaderId, int buyCnt, String leaderName);

    Result ignoreUserApply(String groupId, String userId, String leaderId, String leaderName);

    Result getDayWinStatic(String leaderId);

    Result getWeekWinStatic(String leaderId);

    Result getMonthWinStatic(String leaderId);

    Result getDayBrief(String leaderId, long day);

    Result exeTask(String day);

}
