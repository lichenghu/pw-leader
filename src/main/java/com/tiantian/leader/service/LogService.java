package com.tiantian.leader.service;

import com.tiantian.leader.application.Result;

/**
 *
 */
public interface LogService {
    void saveOpLogs(String leaderId, String leaderName, String groupId, String type, String content);
    Result getOpLogList(String groupId, long lastCreateTime, int count);
}
