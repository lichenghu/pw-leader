package com.tiantian.leader.service;

import com.tiantian.leader.application.Result;
import com.tiantian.leader.model.Club;
import com.tiantian.leader.model.ClubUser;
import com.tiantian.leader.model.MttRoomInfo;
import com.tiantian.leader.model.SngRoom;
import java.util.List;
import java.util.Map;

/**
 *
 */
public interface ClubService {
    List<Club> getUserClubs(String leaderId);

    Result saveUserClubs(String leaderId, String clubName, String clubLogo, String clubMark);

    Result createClubSNGRoom(String leaderId, String roomName, String roomDesc, long fee,
                             String[] phRewards, long[] scores, int maxTableNums, int tableUsers,
                             String[] awardIds);

    Result createClubMttRoom(String leaderId, String gameName, long taxFee,
                             int minUsers, int maxUsers, long startTime,
                             int startBuyIn, int buyInCnt, String rebuyDesc,
                             String rewardMark, int signDelayMins, int maxRebuyBlindLvl,
                             int upgradeBlindMins,
                             String[] phRewards, long[] scores,String[] awardIds);

    List<SngRoom> getSngRooms(String leaderId, long lastCreateDate, int count);

    Result publishMtt(String roomId, String leaderId);

    List<MttRoomInfo> getMttRooms(String leaderId, long lastCreateDate, int count);

    Map<String, Object> getRoomDetail(String roomId);

    List<ClubUser> getClubUsers(String clubId);

    boolean closeRoom(String roomId);

    boolean reduceUserScore(String leaderId, String userId, long score, String agentId);

    boolean addUserScore(String leaderId, String userId, long score, String agentId);

    Result createClubAward(String leaderId, String awardName);

    Result getClubAllAwards(String leaderId);

    Result getClubAwards(String leaderId, String awardId, long lastCreateDate, int count, int status);

    Result validQRCode(String id, String signature, long time, String leaderId);

    Result viewQRAward(String id, String signature, long time, String leaderId);

    Result gameMark(String roomId);

    Result mttRules(String roomId);

    Result mttRewards(String roomId);

    Result mttRankings(String roomId);

    Result mttGameDetail(String roomId);

    Result mttTableUsersInfo(String roomId);

    Result setRewardTemplate(String leaderId, String id , String name,
                             String[] rankingNames,
                             String[] rankingValues);

    Result rewardTemplates(String leaderId);

    Result setMttReward(String leaderId, String roomId, String[] rankingNames, String[] rankingValues);

    Result addClubAgent(String leaderId, String name);

    Result getClubAgents(String leaderId);

    Result delClubAgent(String agentId);

    Result getUserScoreLog(String leaderId, String agentId, long lastCreateDate, int count);
}
