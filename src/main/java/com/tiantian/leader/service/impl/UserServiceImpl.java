package com.tiantian.leader.service.impl;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.db.IDUtils;
import com.tiantian.leader.db.JdbcTemplate;
import com.tiantian.leader.model.Leader;
import com.tiantian.leader.model.LeaderAppVersion;
import com.tiantian.leader.model.LoginSession;
import com.tiantian.leader.model.UserProps;
import com.tiantian.leader.service.ClubService;
import com.tiantian.leader.service.LoginService;
import com.tiantian.leader.service.UserService;
import com.tiantian.leader.utils.*;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;
import org.springframework.cglib.core.Local;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 *
 */
public class UserServiceImpl implements UserService {
    @Inject
    private JdbcTemplate jdbcTemplate;

    @Inject
    private LoginService loginService;

    @Inject
    private ClubService clubService;

    public Result createUser(String mobile, String pwd, String code,
                             String deviceToken, String platform) {
        Validate.notNull(mobile, "手机号不能为空");
        Validate.notNull(pwd, "密码不能为空");
        Validate.notNull(code, "验证码不能为空");
        Validate.isTrue(pwd.length() >= 6, "密码不能少于6位");
        Leader leader = getLeaderByMobile(mobile);
        if (leader != null) {
            return new Result(-1, "该手机号已经注册!");
        }
        // 验证码确认
        String validCode = getShortMessage(mobile, "register_leader_code");
        if (validCode == null) {
            return new Result(-2, "验证码不存在或者已经失效");
        }
        if(!validCode.equalsIgnoreCase(code)) {
            return new Result(-3, "验证码不正确");
        }
        updateMessageValid(mobile, "register_leader_code", code);
        leader = new Leader();
        leader.setLeaderId(IDUtils.UUID());
        leader.setMobile(mobile);
        leader.setNickName(NameRandom.randomName()); // 暂时不唯一校验
        leader.setPwd(MD5.encode(pwd));
        leader.setCreateDate(new Date());
        leader.setVipLvl(5); // 默认普通  前期3个月的皇冠
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH, month + 3);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        leader.setVipEndTime(new Date(calendar.getTimeInMillis() + 24 * 3600000));
        leader.setStatus("normal");
        leader.setGender("1"); // 男
        leader = save(leader);
        if (leader != null) {
            Result result = loginService.login(mobile, pwd, deviceToken, platform);
            return new Result(0, "注册成功!", result.getData());
        }
        return new Result(-1, "注册失败");
    }

    public Result sendRegisterCode(String mobile) {
        Validate.notNull(mobile, "手机号不能为空");
        String code = RandomStringUtils.randomNumeric(4);
        String content = SmsUtils.sendRegisterCode(mobile, code);
        if (content != null) {
            return new Result(0, "发送成功");
        }
        return new Result(-1,"发送验证码失败");
    }

    public Result sendUpdatePwdCode(String mobile) {
        Validate.notNull(mobile, "手机号不能为空");
        String code = RandomStringUtils.randomNumeric(4);
        String content = SmsUtils.sendUpdatePwdCode(mobile, code);
        if (content != null) {
            return new Result(0, "发送成功");
        }
        return new Result(-1,"发送验证码失败");
    }

    public Result updatePwd(String mobile, String pwd, String code) {
        Validate.notNull(mobile, "手机号不能为空");
        Validate.notNull(pwd, "密码不能为空");
        Validate.notNull(code, "验证码不能为空");
        Validate.isTrue(pwd.length() >= 6, "密码不能少于6位");
        // 验证码确认
        String validCode = getShortMessage(mobile, "leader_update_pwd_code");
        if (validCode == null) {
            return new Result(-2, "验证码不存在或者已经失效");
        }
        if(!validCode.equalsIgnoreCase(code)) {
            return new Result(-3, "验证码不正确");
        }
        updateMessageValid(mobile, "leader_update_pwd_code", code);
        boolean ret = updatePwd(mobile, pwd);
        if (!ret) {
            return new Result(-1, "更新密码失败");
        }
        return new Result(0, "更新成功");
    }

    public Result updateNickName(String userId, String nickName) {
        Validate.notNull(nickName, "昵称不能为空");
        nickName = nickName.trim();
        Leader leader = getLeaderById(userId);
        if (leader == null || StringUtils.isBlank(leader.getLeaderId())) {
            return new Result(-1, "用户信息不存在");
        }
        Leader $leader = getLeaderByNickName(nickName);
        if ($leader != null &&  StringUtils.isNotBlank($leader.getLeaderId())
                && !$leader.getLeaderId().equalsIgnoreCase(userId)) {
            return new Result(-2, "该昵称已经存在");
        }
        if (updateLeaderNickName(userId, nickName)) {
            return new Result(0, "修改成功");
        }
        return new Result(-3, "修改失败");
    }

    public Result updateEmail(String leaderId, String email) {
        Validate.notNull(email, "邮箱不能为空");
        Leader leader = getLeaderById(leaderId);
        if (leader == null || StringUtils.isBlank(leader.getLeaderId())) {
            return new Result(-1, "用户信息不存在");
        }
        Leader $leader = getLeaderByEmail(email);
        if ($leader != null &&  StringUtils.isNotBlank($leader.getLeaderId())
                && !$leader.getLeaderId().equalsIgnoreCase(leaderId)) {
            return new Result(-2, "该邮箱已经存在");
        }
        if (updateLeaderEmail(leaderId, email)) {
            return new Result(0, "修改成功");
        }
        return new Result(-3, "修改失败");
    }

    public Result updateAvatarUrl(String leaderId, String url) {
        Validate.notNull(url, "url不能为空");
        Leader leader = getLeaderById(leaderId);
        if (leader == null || StringUtils.isBlank(leader.getLeaderId())) {
            return new Result(-1, "用户信息不存在");
        }
        if (updateLeaderAvatarUrl(leaderId, url)) {
            return new Result(url);
        }
        return new Result(-3, "修改失败");
    }

    public Result getVersion(String version, String platform, String status) {
        if(StringUtils.isBlank(status)) {
            status = "normal";
        }
        List<LeaderAppVersion> appVersionList = LocalCache.getAllVersions();
        LeaderAppVersion result = new LeaderAppVersion();
        result.setVersion(version);
        result.setPlatform(platform.toUpperCase());
        result.setForce(0);
        result.setDownUrl("");
        result.setStatus("normal");
        if (appVersionList != null) {
            for (LeaderAppVersion appVersion : appVersionList) {
                if (appVersion.getPlatform().equals(platform) && appVersion.getVersion().compareTo(version) > 0) {
                    if ("test".equalsIgnoreCase(status)) { //测试状态不需要校验状态
                        result = appVersion;
                        if (appVersion.getForce() == 1) {
                            break;
                        }
                    }
                    else {
                        // 正常状态，检测状态是不是normal
                        if (appVersion.getStatus().equalsIgnoreCase(status)) {
                            result = appVersion;
                            if (appVersion.getForce() == 1) {
                                break;
                            }
                        }
                    }
                }
            }
        }
        return new Result(result);
    }

    public boolean resetUserVip(String leaderId) {
        String sql = "update leader_users set vip_lvl = 0, vip_end_time = null where leader_id = ?";
        try {
            return jdbcTemplate.update(sql, leaderId) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Leader save(Leader leader) {
        String sql = "insert into leader_users(leader_id, mobile, nick_name, pwd, vip_lvl, vip_end_time, status," +
                " create_date) values(?,?,?,?,?,?,?,?)";
        try {
           return jdbcTemplate.insertBean(sql, Leader.class, leader.getLeaderId(), leader.getMobile(),
                   leader.getNickName(), leader.getPwd(), leader.getVipLvl(),
                   new java.sql.Date(leader.getVipEndTime().getTime()), leader.getStatus(),
                   new java.sql.Date(System.currentTimeMillis()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getShortMessage(String mobile, String type) {
        String sql = "select short_code from messages where mobile = ? and type = ? and create_date > ? and valid_status = 0 order by " +
                " create_date desc limit 1";
        try {
            return jdbcTemplate.query(sql, rs -> {
                if(rs.next()) {
                    return rs.getString("short_code");
                }
                return null;
            }, mobile, type, System.currentTimeMillis() - 5 * 60 * 1000);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Leader getLeaderByMobile(String mobile) {
        String sql = "select * from leader_users where mobile = ?";
        try {
            return jdbcTemplate.queryBean(sql, Leader.class, mobile);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean updateTokenByMobile(String mobile, String token) {
        String sql = "update leader_users set token = ? where mobile = ?";
        try {
            return jdbcTemplate.update(sql, token, mobile) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean updatePwd(String mobile, String pwd) {
        String sql = "update leader_users set pwd = ? where mobile = ?";
        try {
            return jdbcTemplate.update(sql, MD5.encode(pwd), mobile) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean updateLeaderNickName(String id, String nickName) {
        String sql = "update leader_users set nick_name = ? where leader_id = ?";
        try {
            return jdbcTemplate.update(sql,  nickName, id) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean updateLeaderEmail(String id, String email) {
        String sql = "update leader_users set email = ? where leader_id = ?";
        try {
            return jdbcTemplate.update(sql,  email, id) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    private boolean updateMessageValid(String mobile,String type, String code) {
        String sql = "update messages set valid_status = 1 where mobile = ? and type = ? and short_code = ?";
        try {
            return jdbcTemplate.update(sql, mobile, type, code) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean updateLeaderAvatarUrl(String id, String url) {
        String sql = "update leader_users set avatar_url = ? where leader_id = ?";
        try {
            return jdbcTemplate.update(sql,  url, id) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Leader getLeaderById(String leaderId) {
        String sql = "select * from leader_users where leader_id = ?";
        try {
            return jdbcTemplate.queryBean(sql, Leader.class, leaderId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Result vipInfo(String leaderId) {
        LoginSession loginSession = LocalCache.getSessionById(leaderId);
        Map<String, Object> resultMap = Maps.newHashMap();
        resultMap.put("vips", VipUtils.INSTANCE.get());
        resultMap.put("user_vip_lvl", loginSession.getVipLvl());
        int vipDate = -1;
        if (loginSession.getVipEndDate() != null) {
            vipDate = DateUtils.daysBetween(new Date(), loginSession.getVipEndDate());
        }
        resultMap.put("user_vip_date", vipDate);
        resultMap.put("wchat", "pokerwinner2016"); //微信号
        resultMap.put("has_club", clubService.getUserClubs(leaderId).size() > 0); //是否有俱乐部
        return new Result(resultMap);
    }

    public List<UserProps> getUserProps(String leaserId) {
        String sql = "select * from leader_user_props where user_id = ?";
        try {
            return jdbcTemplate.queryBeanList(sql, UserProps.class, leaserId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public Result giveProps(String leaderId, String otherUserId, String propCode, int num) {
        boolean ret = reduceProps(leaderId, propCode, num);
        if (!ret) {
            return new Result(-1, "道具数量足");
        }
        boolean addRet = false;
        if ("yanshibao".equalsIgnoreCase(propCode)) {
            try {
                 addRet = AccountIface.instance().iface().saveOrUpdateUserProps(otherUserId, "shalou", "沙漏", "",
                        "朋友场延时物品", num);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if ("friend_ticket".equalsIgnoreCase(propCode)) {
            try {
                  addRet = AccountIface.instance().iface().saveOrUpdateUserFriendTicketsWithNum(otherUserId,
                        "pyc_rcq1h", "friend_ticket", "用于组建朋友场比赛", num);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!addRet) {
            addProps(leaderId, propCode, num);
            return new Result(-2, "赠送失败");
        }
        //TODO 发送消息
        return new Result(0, "赠送成功");
    }

    private boolean reduceProps(String leaderId, String propCode, long num) {
        String sql = "update leader_user_props set prop_nums = prop_nums - ? where " +
                     " prop_nums - ? > 0 and user_id = ? and prop_code = ?";
        try {
            return jdbcTemplate.update(sql,  num, num, leaderId, propCode) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean addProps(String leaderId, String propCode, long num) {
        String sql = "update leader_user_props set prop_nums = prop_nums + ? where " +
                     "user_id = ? and prop_code = ?";
        try {
            return jdbcTemplate.update(sql, num, leaderId, propCode) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Leader getLeaderByNickName(String nickName) {
        String sql = "select * from leader_users where nick_name = ?";
        try {
            return jdbcTemplate.queryBean(sql, Leader.class, nickName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Leader getLeaderByEmail(String email) {
        String sql = "select * from leader_users where email = ?";
        try {
            return jdbcTemplate.queryBean(sql, Leader.class, email);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
