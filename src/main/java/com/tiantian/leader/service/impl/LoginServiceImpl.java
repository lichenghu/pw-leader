package com.tiantian.leader.service.impl;

import com.google.inject.Inject;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.model.Leader;
import com.tiantian.leader.model.LoginSession;
import com.tiantian.leader.service.LoginService;
import com.tiantian.leader.service.UserService;
import com.tiantian.leader.utils.MD5;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;

import java.util.Date;
import java.util.UUID;

/**
 *
 */
public class LoginServiceImpl implements LoginService {
    @Inject
    UserService userService;

    private static final String[] IOS_TEST = new String[] {"16088889999", "16188889999"};

    public Result login(String mobile, String pwd, String deviceToken, String platform) {
        Validate.notNull(mobile, "手机号不能为空");
        Validate.notNull(pwd, "秘密不能为空");
        Leader leader = userService.getLeaderByMobile(mobile);
        if (leader == null) {
            return new Result(-1, "手机号或者密码不正确");
        }
        String md5Pwd = MD5.encode(pwd);
        if(md5Pwd == null || !md5Pwd.equalsIgnoreCase(leader.getPwd())) {
            return new Result(-2, "手机号或者密码不正确");
        }
        if ("forbidden".equalsIgnoreCase(leader.getStatus())) {
            return new Result(-3, "该账号已经被禁用");
        }

        // 判断leader会员
        updateVip(leader);

        String token = UUID.randomUUID().toString().replace("-", "");
        boolean ret = userService.updateTokenByMobile(mobile, token);
        if (!ret) {
            return new Result(-4, "登陆失败");
        }
        LocalCache.exit(leader.getLeaderId());
        LoginSession loginSession = new LoginSession();
        loginSession.setLeaderId(leader.getLeaderId());
        loginSession.setAvatarUrl(leader.getAvatarUrl());
        loginSession.setNickName(leader.getNickName());
        loginSession.setToken(token);
        loginSession.setCreateDate(new Date());
        loginSession.setEmail(leader.getEmail());
        loginSession.setVipLvl(leader.getVipLvl());
        loginSession.setVipEndDate(leader.getVipEndTime());
        loginSession.setDeviceToke(deviceToken);
        loginSession.setPlatform(platform);
        loginSession.setIosTest(ArrayUtils.contains(IOS_TEST, mobile));
        // 设置session
        LocalCache.set(token, loginSession);
        //TODO 保存登陆日志
        return new Result("登陆成功", loginSession);
    }


    private void updateVip(Leader leader) {
        Date vipEndTime = leader.getVipEndTime();
        if (vipEndTime != null && vipEndTime.before(new Date())) { // vip过期
            if(leader.getVipLvl() != 0) {
               userService.resetUserVip(leader.getLeaderId());
               leader.setVipEndTime(null);
               leader.setVipLvl(0);
            }
        }
    }

    public Result login(String token, String deviceToken, String platform) {
        LoginSession loginSession = LocalCache.get(token);
        if (loginSession == null) {
            return new Result(-1, "登陆已实效请重新登陆");
        }
        loginSession.setCreateDate(new Date());
        Leader leader = userService.getLeaderById(loginSession.getLeaderId());
        if (leader == null) {
            return new Result(-1, "用户信息不存在");
        }
        updateVip(leader);
        loginSession.setVipLvl(leader.getVipLvl());
        loginSession.setVipEndDate(leader.getVipEndTime());
        loginSession.setDeviceToke(deviceToken);
        loginSession.setPlatform(platform);
        //TODO 保存登陆日志
        return new Result("登陆成功", loginSession);
    }

    public Result exit(String token) {
        LocalCache.expire(token);
        return new Result(0, "退出成功");
    }
}
