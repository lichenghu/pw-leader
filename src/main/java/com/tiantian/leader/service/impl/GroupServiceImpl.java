package com.tiantian.leader.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.mongodb.*;
import com.mongodb.util.JSON;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.cache.MaintainInfo;
import com.tiantian.leader.model.*;
import com.tiantian.leader.schedule.LeaderDayBriefJob;
import com.tiantian.leader.schedule.LeaderWinJob;
import com.tiantian.leader.service.GroupService;
import com.tiantian.leader.service.LogService;
import com.tiantian.leader.service.UserService;
import com.tiantian.leader.utils.DateUtils;
import com.tiantian.leader.utils.VipUtils;
import com.tiantian.stranger.proxy_client.StrangerEventIface;
import com.tiantian.stranger.thrift.event.StrangerResponse;
import org.apache.commons.lang.*;
import org.apache.thrift.TException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.data.redis.core.StringRedisTemplate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 */
public class GroupServiceImpl implements GroupService {

    private static long[] BUY_INS = new long[]{200, 500, 1000, 2000, 5000, 10000, 3000, 50000, 100000, 1000000};
    private static long[] BIG_BLINDS = new long[]{2, 4, 10, 20, 50, 100, 200, 500, 1000, 10000};
    private static long[] SMALL_BLINDS = new long[]{1, 2, 5, 10, 25, 50, 100, 250, 500, 5000};
    private final String GROUPS_TABLE = "stranger_groups";
    private static final String STRANGER_WIN_LOSE_LOG = "stranger_win_lose_log";
    private static final String SHARE_URL = "【扑克赢家】长按复制这条信息,打开扑克赢家自动加入朋友场,<S %s>.";
    private static final TreeMap<String, String> ODDS_MAP;

    // 桌子在线玩家
    private static final String GROUP_TABLE_USER_ONLINE_KEY = "stranger_table_user_online_key:";
    // 桌子上玩家
    private static final String USER_GROUP_TABLE_KEY = "user_stranger_table_key:";
    private static final String STRANGER_LEADER_WIN_STATISTICAL = "stranger_leader_win_statistical";
    private static final String LEADER_DAY_BRIEF = "leader_day_brief";
    static {
        ODDS_MAP = Maps.newTreeMap();
        ODDS_MAP.put("1", "30");
        ODDS_MAP.put("2", "16");
        ODDS_MAP.put("3", "11");
        ODDS_MAP.put("4", "8");
        ODDS_MAP.put("5", "7");
        ODDS_MAP.put("6", "5.5");
        ODDS_MAP.put("7", "4.5");
        ODDS_MAP.put("8", "4");
        ODDS_MAP.put("9", "3.8");
        ODDS_MAP.put("10", "3.5");
        ODDS_MAP.put("11", "3.3");
        ODDS_MAP.put("12", "3");
        ODDS_MAP.put("13", "2.8");
        ODDS_MAP.put("14", "2.5");
        ODDS_MAP.put("15", "2.3");
    }

    @Inject
    @Named("mongoCore")
    private MongoTemplate mongoTemplate;

    @Inject
    private StringRedisTemplate stringRedisTemplate;

    @Inject
    private LogService logService;
    @Inject
    private UserService userService;

    public Result getBuyInList() {
        return new Result(new long[][]{BUY_INS, BIG_BLINDS, SMALL_BLINDS});
    }

    public Result createGroup(int buyIndex, double hours, boolean safe, String leaderId) {
        Validate.isTrue(buyIndex > 0, "买入信息不正确");
        Validate.isTrue(buyIndex <= BUY_INS.length, "买入信息不正确");
        Validate.isTrue(hours > 0, "时间不能小于0");
        buyIndex = buyIndex - 1;
        long buyIn = BUY_INS[buyIndex];
        long bigBlind = BIG_BLINDS[buyIndex];
        long smallBlind = SMALL_BLINDS[buyIndex];

        MaintainInfo maintainInfo = LocalCache.getAppStatus();
        if (maintainInfo != null) {
            long beginDate = maintainInfo.getBeginDate();
            if (beginDate <= System.currentTimeMillis()) {
                return new Result(-1, "服务器正在维护中不能组局");
            }
            long maintainHours = maintainInfo.getHours();
            return new Result(-1, "服务器正在维护中不能组局,维护时间" + maintainHours + "小时");
        }
        Leader leader = userService.getLeaderById(leaderId);
        if (leader == null) {
            return new Result(-1, "用户信息错误");
        }
        String nickName = leader.getNickName();
        String avatarUrl = leader.getAvatarUrl();
        int vipLvl = leader.getVipLvl();
        Date vipEndDate = leader.getVipEndTime();
        if (vipEndDate != null && vipEndDate.before(new Date())) {
            vipLvl = 0;
            // 更新vip
            userService.resetUserVip(leaderId);
            LoginSession loginSession = LocalCache.getSessionById(leaderId);
            if (loginSession != null) {
                loginSession.setVipLvl(0);
                loginSession.setVipEndDate(null);
            }
        }
        // 判断玩家是否能够创建桌子
        Vip vip = VipUtils.INSTANCE.get(vipLvl);
        if (vip == null) {
            return new Result(-1, "vip信息错误");
        }
        int todayCreateCnt = getUserCreateGroupCnt(leaderId);
        if (vip.getLimitTableNum() > 0 && todayCreateCnt >= vip.getLimitTableNum()) {
            return new Result(-1, "已经达到当天能组局的最大数"+vip.getLimitTableNum() + "局");
        }

        if(smallBlind > vip.getMaxSmallBlind()) {
           return new Result(-1, "最小盲注超过了最大能选择的盲注" + vip.getMaxSmallBlind());
        }
        List<Group> groups = getGroups(leaderId);
        if (groups != null && groups.size() >= vip.getMaxTableNum()) {
            return new Result(-1, "已经达到最大的同时组局数" + vip.getMaxTableNum() + "局");
        }

        Group group = new Group();
        group.setMasterId(leaderId);
        group.setMasterName(nickName);
        group.setGroupCode(generateCode(leaderId));
        group.setMasterAvatarUrl(avatarUrl);
        group.setBigBlind(bigBlind);
        group.setSmallBlind(smallBlind);
        group.setBuyIn(buyIn);
        group.setCreateDate(System.currentTimeMillis());
        group.setFee(0);
        group.setHours(hours);
        group.setForceOver(0);
        group.setSafe(safe);
        long startMills = System.currentTimeMillis() + 15 * 60000; // 延时15分钟
        group.setStartDate(startMills);
        group.setGroupMembers(new ArrayList<>());
        group.setStarted("0");
        group.setEnded("0");
        mongoTemplate.save(group, GROUPS_TABLE);
        return new Result(0, "创建成功");
    }

    public Result getNotEndGroups(String leaderId) {
        List<Group> groups = getGroups(leaderId);
        List<GroupDetail> groupDetails = new ArrayList<>();
        if (groups != null) {
            for (Group group : groups) {
                GroupDetail groupDetail = new GroupDetail();
                groupDetail.setGroupId(group.getGroupId());
                groupDetail.setGroupCode(group.getGroupCode());
                groupDetail.setNickName(group.getMasterName());
                groupDetail.setAvatarUrl(group.getMasterAvatarUrl());
                groupDetail.setSmallBlind(group.getSmallBlind());
                groupDetail.setBigBlind(group.getBigBlind());
                groupDetail.setHours(group.getHours());
                groupDetail.setSafe(group.isSafe());
                Map<String, Long> userWinAndSafe = getUserWinAndSafeWin(group.getGroupId());
                Long userWin = userWinAndSafe.get("userWin");
                Long safeWin = userWinAndSafe.get("safeWin");
                Long safeCost = userWinAndSafe.get("safeCost");
                groupDetail.setUsersWin(userWin);
                groupDetail.setSafeWin(safeCost - safeWin);
                int[] sitAndViews = getSitOrViewNum(group.getGroupId());
                groupDetail.setSitNum(sitAndViews[0]);
                groupDetail.setViewNum(sitAndViews[1]);
                groupDetail.setGameStatus("1".equalsIgnoreCase(group.getStarted()) ? "gaming" : "waiting");
                groupDetail.setShareUrl(String.format(SHARE_URL, group.getGroupId()));
                groupDetail.setStartDate("1".equalsIgnoreCase(group.getStarted()) ? group.getStartDate() : 0);
                groupDetail.setCreateDate(group.getCreateDate());
                groupDetail.setSafe(group.isSafe());
                groupDetails.add(groupDetail);
            }
        }
        return new Result(groupDetails);
    }

    private int[] getSitOrViewNum(String groupId) {
        int sitNum = 0;
        int viewNum = 0;
        Map<Object, Object> tableOnlineMap = stringRedisTemplate.opsForHash().entries(GROUP_TABLE_USER_ONLINE_KEY + groupId);
        for (Object key : tableOnlineMap.keySet()) {
            String userId = (String) key;
            String status = (String) stringRedisTemplate.opsForHash().get(USER_GROUP_TABLE_KEY + userId, "status");
            if (StringUtils.isBlank(status)) {
                viewNum++;
            } else {
                sitNum++;
            }
        }
        return new int[]{sitNum, viewNum};
    }

    public Result getEndGroups(String leaderId, long lastCreateDate, int count) {
        List<Group> groups = getEndGroupsPage(leaderId, lastCreateDate, count);
        List<GroupDetail> groupDetails = new ArrayList<>();
        if (groups != null) {
            for (Group group : groups) {
                GroupDetail groupDetail = new GroupDetail();
                groupDetail.setGroupId(group.getGroupId());
                groupDetail.setGroupCode(group.getGroupCode());
                groupDetail.setNickName(group.getMasterName());
                groupDetail.setAvatarUrl(group.getMasterAvatarUrl());
                groupDetail.setSmallBlind(group.getSmallBlind());
                groupDetail.setBigBlind(group.getBigBlind());
                groupDetail.setHours(group.getHours());
                groupDetail.setSafe(group.isSafe());
                Map<String, Long> userWinAndSafe = getUserWinAndSafeWin(group.getGroupId());
                Long userWin = userWinAndSafe.get("userWin");
                Long safeWin = userWinAndSafe.get("safeWin");
                Long safeCost = userWinAndSafe.get("safeCost");
                groupDetail.setUsersWin(userWin);
                groupDetail.setSafeWin(safeCost - safeWin);
                List<GroupMember> groupMembers = group.getGroupMembers();
                int joinNums = 0;
                if (groupMembers != null && groupMembers.size() > 0) {
                    for (GroupMember groupMember : groupMembers) {
                        if (groupMember.getIsJoin() == 1) {
                            joinNums++;
                        }
                    }
                }
                groupDetail.setSitNum(joinNums);
                groupDetail.setViewNum(0);
                groupDetail.setGameStatus("end");
                groupDetail.setCreateDate(group.getCreateDate());
                groupDetail.setSafe(group.isSafe());
                groupDetails.add(groupDetail);
            }
        }
        return new Result(groupDetails);
    }

    public Result startGroupGame(String groupId, String leaderId, String leaderName) {
       //  StrangerResponse response = StrangerEventUtils.getInstance().startGame(groupId);
        StrangerResponse response = null;
        try {
            response = StrangerEventIface.instance().iface().startGame(groupId);
        } catch (TException e) {
            e.printStackTrace();
        }
        Group group = getGroup(groupId);
        if (response.getStatus() == 0) {
            logService.saveOpLogs(leaderId, leaderName, groupId, "start_game", "你开始了编号为" + group.getGroupCode() + "游戏");
        }
        return new Result(response.getStatus(), response.getMessage());
    }

    public Result groupUsers(String groupId) {
        List<GroupUserInfo> groupUserInfos = getGroupUserInfos(groupId);
        return new Result(groupUserInfos);
    }

    private List<Group> getGroups(String leaderId) {
        long currentMill = System.currentTimeMillis();
        BasicDBObject dbObject = (BasicDBObject) JSON.parse("{$where : \"this.masterId == '" + leaderId + "' &&  " +
                " (this.ended != '1' && this.forceOver != 1 && (this.startDate == 0 || this.startDate + this.hours * 3600000 >= " + (currentMill - 300000) + "))\"}");
        DBCollection dbCollection = mongoTemplate.getCollection(GROUPS_TABLE);
        DBCursor cursor = dbCollection.find(dbObject)
                .sort(new BasicDBObject("createDate", -1));
        List<Group> list = new ArrayList<>();
        if (cursor != null) {
            while (cursor.hasNext()) {
                BasicDBObject object = (BasicDBObject) cursor.next();
                Group group = getGroupFromDB(object);
                list.add(group);
            }
        }
        return list;
    }

    public Result getApplications(String groupId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("groupId").is(groupId).and("appMoneyStatus").ne(0);
        query.addCriteria(criteria);
        List<StrangerWinLoseLog> strangerWinLoseLogs = mongoTemplate.find(query, StrangerWinLoseLog.class, STRANGER_WIN_LOSE_LOG);
        List<UserAppInfo> userAppInfos = Lists.newArrayList();
        for (StrangerWinLoseLog strangerWinLoseLog : strangerWinLoseLogs) {
            long appMoneyBuyIn = strangerWinLoseLog.getAppMoneyBuyIn();
            if (appMoneyBuyIn > 0) {
                UserAppInfo userAppInfo = new UserAppInfo();
                userAppInfo.setBuy(appMoneyBuyIn);
                userAppInfo.setUserId(strangerWinLoseLog.getUserId());
                userAppInfo.setNickName(strangerWinLoseLog.getNickName());
                userAppInfo.setType("money");
                userAppInfo.setAppDate(strangerWinLoseLog.getAppMoneyBuyInDate());
                userAppInfos.add(userAppInfo);
            }
        }
        return new Result(userAppInfos);
    }

    public Result getAllApplications(String leaderId) {
        List<Group> groups = getGroups(leaderId);
        List<UserAppInfo> userAppInfos = Lists.newArrayList();
        if (groups != null && groups.size() > 0) {
            List<String> groupIds = Lists.newArrayList();
            Map<String, GroupMember> allMember = Maps.newConcurrentMap();
            Map<String, Group> allGroup = Maps.newConcurrentMap();
            for (Group group : groups) {
                 groupIds.add(group.getGroupId());
                 allGroup.put(group.getGroupId(), group);
                 List<GroupMember> groupMembers = group.getGroupMembers();
                 if (groupMembers != null && groupMembers.size() > 0) {
                     for (GroupMember groupMember : groupMembers) {
                          allMember.put(groupMember.getUserId(), groupMember);
                     }
                 }
            }
            Query query = new Query();
            Criteria criteria = Criteria.where("groupId").in(groupIds).and("appMoneyStatus").ne(0);
            query.addCriteria(criteria);
            List<StrangerWinLoseLog> strangerWinLoseLogs = mongoTemplate.find(query, StrangerWinLoseLog.class, STRANGER_WIN_LOSE_LOG);

            for (StrangerWinLoseLog strangerWinLoseLog : strangerWinLoseLogs) {
                long appMoneyBuyIn = strangerWinLoseLog.getAppMoneyBuyIn();
                if (appMoneyBuyIn > 0) {
                    UserAppInfo userAppInfo = new UserAppInfo();
                    userAppInfo.setBuy(appMoneyBuyIn);
                    userAppInfo.setUserId(strangerWinLoseLog.getUserId());
                    userAppInfo.setNickName(strangerWinLoseLog.getNickName());
                    userAppInfo.setType("money");
                    GroupMember groupMember = allMember.get(strangerWinLoseLog.getUserId());
                    if (groupMember != null) {
                        userAppInfo.setAvatarUrl(groupMember.getAvatarUrl());
                    }
                    else {
                        userAppInfo.setAvatarUrl("");
                    }
                    userAppInfo.setGroupId(strangerWinLoseLog.getGroupId());
                    userAppInfo.setGroupCode(allGroup.get(strangerWinLoseLog.getGroupId()).getGroupCode());
                    userAppInfo.setAppDate(strangerWinLoseLog.getAppMoneyBuyInDate());
                    userAppInfos.add(userAppInfo);
                }
            }
        }
        return new Result(userAppInfos);
    }

    private int getUserCreateGroupCnt(String leaderId) {
        Criteria matcher = Criteria.where("masterId").in(leaderId)
                .and("createDate").gt(DateUtils.todayBeginMills());
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(matcher),
                Aggregation.group("masterId").count().as("createCnt"));
        AggregationResults<CreateGroupCount> result = mongoTemplate.aggregate(aggregation, GROUPS_TABLE, CreateGroupCount.class);
        List<CreateGroupCount> createGroupCounts = result.getMappedResults();
        if (createGroupCounts != null && createGroupCounts.size() > 0) {
            return createGroupCounts.get(0).getCreateCnt();
        }
        return 0;
    }
    private static class CreateGroupCount{
        private String _id;
        private Integer createCnt;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public Integer getCreateCnt() {
            return createCnt;
        }

        public void setCreateCnt(Integer createCnt) {
            this.createCnt = createCnt;
        }
    }


    public Result getAppCounts(String leaderId) {
        List<Group> groups = getGroups(leaderId);
        List<Map<String, String>> countList = Lists.newArrayList();
        if (groups != null && groups.size() > 0) {
            List<String> groupIds = Lists.newArrayList();
            for (Group group : groups) {
                 groupIds.add(group.getGroupId());
            }
            Criteria matcher = Criteria.where("groupId").in(groupIds).and("appMoneyStatus").ne(0);
            Aggregation aggregation = Aggregation.newAggregation(
                    Aggregation.match(matcher),
                    Aggregation.group("groupId").count().as("appCount"));
            AggregationResults<AppCount> result = mongoTemplate.aggregate(aggregation, STRANGER_WIN_LOSE_LOG, AppCount.class);
            List<AppCount> appCounts = result.getMappedResults();
            if (appCounts != null) {
                for (AppCount appCount : appCounts) {
                     Map<String, String> countMaps =  Maps.newHashMap();
                     countMaps.put("groupId", appCount.get_id());
                     Integer count = appCount.getAppCount();
                     if (count == null) {
                         countMaps.put("count", "0");
                     } else {
                         countMaps.put("count", count.toString());
                     }
                     countList.add(countMaps);
                }
            }
        }
        return new Result(countList);
    }

    private static class AppCount{
        private String _id;
        private Integer appCount;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public Integer getAppCount() {
            return appCount;
        }

        public void setAppCount(Integer appCount) {
            this.appCount = appCount;
        }
    }

    private UserAppInfo getUserMoneyApplication(String groupId, String userId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("groupId").is(groupId).and("userId").is(userId).and("appMoneyStatus").ne(0);
        query.addCriteria(criteria);
        StrangerWinLoseLog strangerWinLoseLog = mongoTemplate.findOne(query, StrangerWinLoseLog.class, STRANGER_WIN_LOSE_LOG);
        if (strangerWinLoseLog != null) {
            long buyIn = strangerWinLoseLog.getAppMoneyBuyIn();
            if (buyIn > 0) {
                UserAppInfo userAppInfo = new UserAppInfo();
                userAppInfo.setBuy(buyIn);
                userAppInfo.setUserId(strangerWinLoseLog.getUserId());
                userAppInfo.setNickName(strangerWinLoseLog.getNickName());
                userAppInfo.setType("money");
                userAppInfo.setAppDate(strangerWinLoseLog.getAppMoneyBuyInDate());
                return userAppInfo;
            }
        }
        return null;
    }

    public Result agreeBuyIn(String groupId, String userId, String leaderId, String leaderName) {
        UserAppInfo userAppInfo = getUserMoneyApplication(groupId, userId);
        System.out.println(com.alibaba.fastjson.JSON.toJSONString(userAppInfo));
        //StrangerResponse response = StrangerEventUtils.getInstance().agreeUserBuyIn(groupId, userId);
        StrangerResponse response = null;
        try {
            response = StrangerEventIface.instance().iface().agreeUserBuyIn(groupId, userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        if (response.getStatus() == 0) {
            if (userAppInfo != null) {
                System.out.printf("agree_cash_log");
                logService.saveOpLogs(leaderId, leaderName, groupId, "agree_chips",
                        String.format("你同意了%s的%s额度买入申请", userAppInfo.getNickName(), userAppInfo.getBuy()));
            }
        }
        return new Result(response.getStatus(), response.getMessage());
    }

    public Result getAllOdds() {
        return new Result(ODDS_MAP);
    }

    public Result getScoreboard(String groupId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("groupId").is(groupId);
        query.addCriteria(criteria).with(new Sort(new Sort.Order(Sort.Direction.DESC, "win")));
        List<StrangerWinLoseLog> strangerWinLoseLogs = mongoTemplate.find(query, StrangerWinLoseLog.class, STRANGER_WIN_LOSE_LOG);
        if (strangerWinLoseLogs != null && strangerWinLoseLogs.size() > 0) {
            List<ScoreBoard> scoreBoards = Lists.newArrayList();
            Group group = getGroup(groupId);
            Map<String, String> userIdAndAvatarUrl = Maps.newHashMap();
            if (group != null && group.getGroupMembers() != null && group.getGroupMembers().size() > 0) {
                for (GroupMember member : group.getGroupMembers()) {
                    userIdAndAvatarUrl.put(member.getUserId(), StringUtils.isBlank(member.getAvatarUrl()) ? "" : member.getAvatarUrl());
                }
            }
            for (StrangerWinLoseLog strangerWinLoseLog : strangerWinLoseLogs) {
                ScoreBoard scoreBoard = new ScoreBoard();
                scoreBoard.setNickName(strangerWinLoseLog.getNickName());
                scoreBoard.setAvatarUrl(userIdAndAvatarUrl.get(strangerWinLoseLog.getUserId()));
                scoreBoard.setTotalBuyIn(strangerWinLoseLog.getChipsBuyTotal());
                scoreBoard.setWin(strangerWinLoseLog.getWin());
                scoreBoard.setLeftChips(strangerWinLoseLog.getLeftChips());
                scoreBoard.setSafeWin(strangerWinLoseLog.getSafeWin());
                scoreBoard.setSafeCost(strangerWinLoseLog.getSafeCost());
                scoreBoard.setMoneyBuyTotal(strangerWinLoseLog.getMoneyBuyTotal());
                scoreBoard.setMoneyLeft(strangerWinLoseLog.getMoneyLeft());
                scoreBoard.setJoinDate(strangerWinLoseLog.getCreateDate());
                scoreBoards.add(scoreBoard);
            }
            return new Result(scoreBoards);
        }
        return new Result();
    }

    public Result forceStandUp(String groupId, String userId, String $leaderId) {
        Group group = getGroup(groupId);
        if (group == null || !group.getMasterId().equalsIgnoreCase($leaderId)) {
            return new Result(-1, "你没有该权限");
        }
       //  boolean result = StrangerEventUtils.getInstance().standUp(groupId, userId);
        boolean result = false;
        try {
            result = StrangerEventIface.instance().iface().standUp(groupId, userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        if (result) {
            return new Result(0, "强制站起成功");
        } else {
            return new Result(-1, "强制站起失败");
        }
    }

    public Result forceCloseGame(String groupId, String $leaderId) {
        Group group = getGroup(groupId);
        if (group == null || !group.getMasterId().equalsIgnoreCase($leaderId)) {
            return new Result(-1, "你没有该权限");
        }
       // StrangerResponse result = StrangerEventUtils.getInstance().closeGame(groupId);
        StrangerResponse result = null;
        try {
            result = StrangerEventIface.instance().iface().closeGame(groupId);
        } catch (TException e) {
            e.printStackTrace();
        }
        return new Result(result.getStatus(), result.getMessage());
    }

    public Result getAddMoneyInfo(String groupId, String userId, String $leaderId) {
        Group group = getGroup(groupId);
        if (group == null || !group.getMasterId().equalsIgnoreCase($leaderId)) {
            return new Result(-1, "你没有该权限");
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("app_buy", 0L);
        resultMap.put("cnt", 0L);
        resultMap.put("buy_in", 0L);
        //判断是否有申请
        UserAppInfo userAppInfo = getUserMoneyApplication(groupId, userId);
        if (userAppInfo != null) {
            resultMap.put("app_buy", userAppInfo.getBuy());
            return new Result(resultMap);
        }
       // Map<String, Long> map = StrangerEventUtils.getInstance().getBuyInInfo(userId, groupId);
        Map<String, Long> map = null;
        try {
            map = StrangerEventIface.instance().iface().getBuyInInfo(userId, groupId);
        } catch (TException e) {
            e.printStackTrace();
        }
        resultMap.putAll(map);
        return new Result(resultMap);
    }

    public Result addUserMoney(String groupId, String userId, String leaderId, int buyCnt,
                               String leaderName) {
        Group group = getGroup(groupId);
        if (group == null || !group.getMasterId().equalsIgnoreCase(leaderId)) {
            return new Result(-1, "你没有该权限");
        }
        UserAppInfo userAppInfo = getUserMoneyApplication(groupId, userId);
        if (userAppInfo != null) {
            return new Result(-2, "当前玩家有未处理的申请,请先处理申请");
        }
        StrangerWinLoseLog strangerWinLoseLog = getUserStrangerWinLoseLog(groupId, userId);
        //StrangerResponse response = StrangerEventUtils.getInstance().leaderAddUserDepositMoney(groupId, userId, buyCnt, group.getBuyIn());
        StrangerResponse response = null;
        try {
            response = StrangerEventIface.instance().iface().leaderAddUserDepositMoney(groupId, userId, buyCnt, group.getBuyIn());
        } catch (TException e) {
            e.printStackTrace();
        }
        if (response.getStatus() == 0) {
            logService.saveOpLogs(leaderId, leaderName, groupId, "add_money",
                    String.format("你增加了%s%s额度", strangerWinLoseLog.getNickName(), buyCnt * group.getBuyIn()));
        }
        return new Result(response.getStatus(), response.getMessage());
    }

    private StrangerWinLoseLog getUserStrangerWinLoseLog(String groupId, String userId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("groupId").is(groupId).and("userId").is(userId);
        query.addCriteria(criteria);
        return mongoTemplate.findOne(query, StrangerWinLoseLog.class, STRANGER_WIN_LOSE_LOG);
    }

    public Result ignoreUserApply(String groupId, String userId, String leaderId, String leaderName) {
        Group group = getGroup(groupId);
        if (group == null || !group.getMasterId().equalsIgnoreCase(leaderId)) {
            return new Result(-1, "你没有该权限");
        }
        Query query = new Query();
        Criteria criteria = Criteria.where("groupId").is(groupId).and("userId").is(userId).and("appMoneyStatus").ne(0);
        query.addCriteria(criteria);
        StrangerWinLoseLog strangerWinLoseLog = mongoTemplate.findOne(query, StrangerWinLoseLog.class, STRANGER_WIN_LOSE_LOG);
        if (strangerWinLoseLog == null || StringUtils.isBlank(strangerWinLoseLog.getId())) {
            return new Result(0, "删除成功");
        }
        Update update = new Update();
        update.set("appMoneyStatus", 0);
        update.set("appMoneyBuyIn", 0L);
        mongoTemplate.updateMulti(query, update, STRANGER_WIN_LOSE_LOG);

        logService.saveOpLogs(leaderId, leaderName, groupId, "ignore_money",
                String.format("你忽略了%s的额度申请", strangerWinLoseLog.getNickName()));
        return new Result(0, "删除成功");
    }

    public Result getDayWinStatic(String leaderId) {
        // 向前推30天
        long beginDate = DateUtils.todayEndMills() - 2592000000l;
        long endDate = DateUtils.todayEndMills() - 86400000;
        List<DayWinStatic> list = getWinStatics(leaderId, beginDate, endDate);
        return new Result(list);
    }

    private List<DayWinStatic> getWinStatics(String leaderId, long beginDate, long endDate) {
        Criteria matcher = Criteria.where("userId").is(leaderId).and("statistDate").gte(beginDate)
                .andOperator(Criteria.where("statistDate").lte(endDate));
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(matcher),
                Aggregation.group("statistDate").sum("safeWin").as("safeWin").sum("win").as("win"),
                Aggregation.sort(new Sort(Sort.Direction.ASC, "_id"))
        );
        AggregationResults<LeaderStatics> result = mongoTemplate.
                aggregate(aggregation, STRANGER_LEADER_WIN_STATISTICAL, LeaderStatics.class);
        List<DayWinStatic> list = Lists.newArrayList();
        if (result != null) {
            for (LeaderStatics leaderStatics : result) {
                 DayWinStatic dayWinStatic = new DayWinStatic(leaderStatics.get_id(),
                        leaderStatics.getWin(), leaderStatics.getSafeWin());
                 list.add(dayWinStatic);
            }
        }
        return list;
    }

    public Result getWeekWinStatic(String leaderId) {
        long endDate = DateUtils.getLastWeekEndDayEnd();
        long beginDate = DateUtils.getBetweenMills(endDate, 84) + 1000; //12周前的时间 加上1s
        List<DayWinStatic> list = getWinStatics(leaderId, beginDate, endDate);
        Map<Long, DayWinStatic> dayWinStaticMap = Maps.newHashMap();
        if (list != null && list.size() > 0) {
            for (DayWinStatic dayWinStatic : list) {
                 dayWinStaticMap.put(dayWinStatic.getDay(), dayWinStatic);
            }
        }
        long begin = DateUtils.getBetweenMills(endDate, 84) + 1000;
        long safeWin = 0;
        long win = 0;
        List<Map<String, Object>> resultList = Lists.newArrayList();
        for (int i = 1; i <= 84; i++) {
             begin += (i > 1 ? 86400000 : 0);
             DayWinStatic dayWinStatic = dayWinStaticMap.get(begin);
             long tmpSafeWin = 0;
             long tmpWin = 0;
             if (dayWinStatic != null) {
                 tmpSafeWin = dayWinStatic.getSafeWin();
                 tmpWin = dayWinStatic.getWin();
             }
             safeWin += tmpSafeWin;
             win += tmpWin;
             if (i % 7 == 0) { // 整一周
                 Map<String, Object> map = Maps.newHashMap();
                 map.put("safeWin", safeWin);
                 map.put("win", win);
                 map.put("weekDayBegin", begin - 7 * 86400000);
                 map.put("weekDayEnd", begin);
                 resultList.add(map);
                 safeWin = 0;
                 win = 0;
             }
        }
        return new Result(resultList);
    }

    public Result getMonthWinStatic(String leaderId) { // 获取当前12个月
        long begin = DateUtils.yearBeginMill();
        int beginMonth = DateUtils.getYearOfMonth(begin);
        int nowMonth = DateUtils.getYearOfMonth(System.currentTimeMillis());
        long end = DateUtils.getYearMonthLastDayEndDate(Math.max(1, nowMonth - 1));
        List<Map<String, Object>> resultList = Lists.newArrayList();
        List<DayWinStatic> list = getWinStatics(leaderId, begin, end);
        Map<Long, DayWinStatic> dayWinStaticMap = Maps.newHashMap();
        if (list != null && list.size() > 0) {
            for (DayWinStatic dayWinStatic : list) {
                dayWinStaticMap.put(dayWinStatic.getDay(), dayWinStatic);
            }
        }

        for (int i = beginMonth; i < nowMonth; i++) {
             long safeWin = 0;
             long win = 0;
            // 计算一个月的
             long monthBeginDate = DateUtils.getYearMonthFirstDayBeginDate(beginMonth);
             long monthBeginEnd = DateUtils.getYearMonthLastDayBeginDate(beginMonth);
             for (long j = monthBeginDate; j <= monthBeginEnd; j+= 86400000) {
                  DayWinStatic dayWinStatic = dayWinStaticMap.get(j);
                  if (dayWinStatic != null) {
                      safeWin += dayWinStatic.getSafeWin();
                      win += dayWinStatic.getWin();
                 }
             }
            Map<String, Object> map = Maps.newHashMap();
            map.put("safeWin", safeWin);
            map.put("win", win);
            map.put("month", beginMonth);
            resultList.add(map);
        }
        return new Result(resultList);
    }

    public Result getDayBrief(String leaderId, long day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(day));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Query query = new Query();
        Criteria criteria = Criteria.where("userId").is(leaderId).and("statisDate").is(calendar.getTimeInMillis());
        query.addCriteria(criteria);
        DayBrief dayBrief = mongoTemplate.findOne(query,  DayBrief.class, LEADER_DAY_BRIEF);
        return new Result(dayBrief);
    }

    public Result exeTask(String day) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sf.parse(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LeaderWinJob leaderWinJob = new LeaderWinJob();
        leaderWinJob.exeTask(DateUtils.dayBeginMills(date), DateUtils.dayEndMills(date));
        LeaderDayBriefJob briefJob = new LeaderDayBriefJob();
        briefJob.executeTask(DateUtils.dayBeginMills(date), DateUtils.dayEndMills(date));
        return new Result("ok");
    }

    private class DayWinStatic {
        private long day;
        private long win;
        private long safeWin;

        public DayWinStatic(long day, long win, long safeWin) {
            this.day = day;
            this.win = win;
            this.safeWin = safeWin;
        }

        public long getDay() {
            return day;
        }

        public void setDay(long day) {
            this.day = day;
        }

        public long getWin() {
            return win;
        }

        public void setWin(long win) {
            this.win = win;
        }

        public long getSafeWin() {
            return safeWin;
        }

        public void setSafeWin(long safeWin) {
            this.safeWin = safeWin;
        }
    }

    private class LeaderStatics {
        private long _id;
        private long win;
        private long safeWin;

        public long get_id() {
            return _id;
        }

        public void set_id(long _id) {
            this._id = _id;
        }

        public long getWin() {
            return win;
        }

        public void setWin(long win) {
            this.win = win;
        }

        public long getSafeWin() {
            return safeWin;
        }

        public void setSafeWin(long safeWin) {
            this.safeWin = safeWin;
        }
    }

    private List<GroupUserInfo> getGroupUserInfos(String groupId) {
        Group group = getGroup(groupId);
        if (group == null) {
            return Lists.newArrayList();
        }
        List<GroupMember> groupMembers = group.getGroupMembers();
        if (groupMembers == null) {
            return Lists.newArrayList();
        }
        List<StrangerWinLoseLog> strangerWinLoseLogs = mongoTemplate.find(new Query(Criteria.where("groupId").is(groupId)),
                StrangerWinLoseLog.class, STRANGER_WIN_LOSE_LOG);
        Map<String, StrangerWinLoseLog> logMap = Maps.newHashMap();
        if (strangerWinLoseLogs != null) {
            for (StrangerWinLoseLog strangerWinLoseLog : strangerWinLoseLogs) {
                String userId = strangerWinLoseLog.getUserId();
                logMap.put(userId, strangerWinLoseLog);
            }
        }
        List<GroupUserInfo> userInfos = Lists.newArrayList();
       // Map<String, String> statusMap = StrangerEventUtils.getInstance().getGroupUserStatus(groupId);
        Map<String, String> statusMap = null;
        try {
            statusMap = StrangerEventIface.instance().iface().getGroupUserStatus(groupId);
        } catch (TException e) {
            e.printStackTrace();
        }
        for (GroupMember groupMember : groupMembers) {
            GroupUserInfo userInfo = new GroupUserInfo();
            String nickName = groupMember.getNickName();
            String avatarUrl = groupMember.getAvatarUrl();
            String userId = groupMember.getUserId();
            int isJoin = groupMember.getIsJoin();
            userInfo.setUserId(userId);
            userInfo.setNickName(nickName);
            userInfo.setAvatarUrl(avatarUrl);
            userInfo.setJoinDate(groupMember.getCreateDate());
            StrangerWinLoseLog strangerWinLoseLog = logMap.get(userId);
            String status = statusMap.get(userId);
            String userStatus;
            // join stand leave
            if (status == null) { // 玩家不在桌子上
                userStatus = "leave";
            } else {
                userStatus = status;
            }
            userInfo.setStatus(userStatus);
            if (strangerWinLoseLog != null) {
                userInfo.setWin(strangerWinLoseLog.getWin());
                userInfo.setLeftMoney(strangerWinLoseLog.getMoneyLeft());
            }
            userInfos.add(userInfo);
        }
        return userInfos;
    }


    private List<Group> getEndGroupsPage(String leaderId, long lastCreateDate, int count) {
        long currentMill = System.currentTimeMillis();
        long lastDay = currentMill - 24 * 3600000; // 不能超过24小时
        String pageLimit = "";
        if (lastCreateDate > 0) {
            pageLimit = " && this.createDate < " + lastCreateDate;
        }
        BasicDBObject dbObject = (BasicDBObject) JSON.parse("{$where : \"this.masterId == '" + leaderId + "' &&  " +
                " (this.ended == '1' || this.forceOver == 1 || this.startDate + this.hours * 3600000 < " + currentMill +
                " || this.createDate < " + lastDay + ")" + pageLimit + "\"}");
        DBCollection dbCollection = mongoTemplate.getCollection(GROUPS_TABLE);
        DBCursor cursor = dbCollection.find(dbObject)
                .sort(new BasicDBObject("createDate", -1)).limit(Math.min(count, 100));
        List<Group> list = new ArrayList<>();
        if (cursor != null) {
            while (cursor.hasNext()) {
                BasicDBObject object = (BasicDBObject) cursor.next();
                Group group = getGroupFromDB(object);
                list.add(group);
            }
        }
        return list;
    }

    private Group getGroupFromDB(BasicDBObject object) {
        Group group = new Group();
        group.setGroupId(object.get("_id").toString());
        group.setGroupCode(object.getString("groupCode"));
        group.setBigBlind(object.getLong("bigBlind"));
        group.setSmallBlind(object.getLong("smallBlind"));
        group.setBuyIn(object.getLong("buyIn"));
        group.setMasterId(object.getString("masterId"));
        group.setMasterAvatarUrl(object.getString("masterAvatarUrl"));
        group.setMasterName(object.getString("masterName"));
        group.setFee(object.getLong("fee"));
        group.setCreateDate(object.getLong("createDate"));
        group.setStartDate(object.getLong("startDate"));
        group.setForceOver(object.getInt("forceOver"));
        group.setTotalPlayCnt(object.getLong("totalPlayCnt"));
        group.setHours(object.getDouble("hours"));
        group.setSafe(object.getBoolean("safe"));
        group.setStarted(object.getString("started"));
        List<BasicDBObject> groupMembers = (List<BasicDBObject>) object.get("groupMembers");
        List<GroupMember> groupMembersList = new ArrayList<>();
        if (groupMembers != null) {
            for (BasicDBObject groupMemberDoc : groupMembers) {
                String memUserId = groupMemberDoc.getString("userId");
                String nickName = groupMemberDoc.getString("nickName");
                String avatarUrl = groupMemberDoc.getString("avatarUrl");
                Integer isJoin = groupMemberDoc.getInt("isJoin", 0);
                String mobile = groupMemberDoc.getString("mobile");
                GroupMember groupMember = new GroupMember();
                groupMember.setUserId(memUserId);
                groupMember.setNickName(nickName);
                groupMember.setIsJoin(isJoin);
                groupMember.setMobile(mobile);
                groupMember.setAvatarUrl(avatarUrl);
                groupMembersList.add(groupMember);
            }
        }
        group.setGroupMembers(groupMembersList);
        return group;
    }

    // 生成一个code
    private String generateCode(String masterId) {

        Query query = new Query();
        Criteria criteria = Criteria.where("masterId").is(masterId).and("createDate")
                .gt(DateUtils.todayBeginMills());
        query.addCriteria(criteria);
        query.with(new Sort(Sort.Direction.DESC, "createDate"));
        query.limit(1);
        List<Group> groupList = mongoTemplate.find(query, Group.class, GROUPS_TABLE);
        if (groupList != null && groupList.size() > 0) {
            String lastGroupCode = groupList.get(0).getGroupCode();
            return (Integer.parseInt(lastGroupCode) + 1) + "";
        }
        return "1";
    }


    private Group getGroup(String groupId) {
        return mongoTemplate.findOne(new Query(Criteria.where("_id").is(groupId)), Group.class, GROUPS_TABLE);
    }

    public Map<String, Long> getUserWinAndSafeWin(String groupId) { // 获取玩家盈利和团长的保险盈利
        Criteria matcher = Criteria.where("groupId").is(groupId);
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(matcher),
                Aggregation.group("groupId").sum("safeWin").as("safe_win").sum("safeCost").as("safe_cost")
        );
        AggregationResults<WinResult> result = mongoTemplate.aggregate(aggregation, STRANGER_WIN_LOSE_LOG, WinResult.class);
        List<WinResult> winResults = result.getMappedResults();
        Map<String, Long> resultMap = Maps.newHashMap();
        resultMap.put("userWin", 0l);
        resultMap.put("safeWin", 0l);
        resultMap.put("safeCost", 0l);

        if (winResults != null && winResults.size() > 0) {
            WinResult winResult = winResults.get(0);
            resultMap.put("safeWin", winResult.getSafe_win());
            resultMap.put("safeCost", winResult.getSafe_cost());
        }
        Criteria matcher2 = Criteria.where("groupId").is(groupId).and("win").gt(0);
        Aggregation aggregation2 = Aggregation.newAggregation(
                Aggregation.match(matcher2),
                Aggregation.group("groupId").sum("win").as("user_win")
        );
        AggregationResults<WinResult> result2 = mongoTemplate.aggregate(aggregation2, STRANGER_WIN_LOSE_LOG, WinResult.class);
        List<WinResult> winResults2 = result2.getMappedResults();
        if (winResults2 != null && winResults2.size() > 0) {
            WinResult winResult = winResults2.get(0);
            resultMap.put("userWin", winResult.getUser_win());
        }
        return resultMap;
    }

    private static class WinResult {
        private String _id;
        private long safe_win;
        private long safe_cost;
        private long user_win;
        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public long getSafe_win() {
            return safe_win;
        }

        public void setSafe_win(long safe_win) {
            this.safe_win = safe_win;
        }

        public long getSafe_cost() {
            return safe_cost;
        }

        public void setSafe_cost(long safe_cost) {
            this.safe_cost = safe_cost;
        }

        public long getUser_win() {
            return user_win;
        }

        public void setUser_win(long user_win) {
            this.user_win = user_win;
        }
    }
}
