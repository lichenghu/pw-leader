package com.tiantian.leader.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.tiantian.club.proxy_client.ClubIface;
import com.tiantian.clubmtt.proxy_client.MttIface;
import com.tiantian.clubmtt.thrift.*;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.db.JdbcTemplate;
import com.tiantian.leader.model.*;
import com.tiantian.leader.service.ClubService;
import com.tiantian.leader.utils.MD5;
import com.tiantian.mail.proxy_client.MailIface;
import com.tiantian.sng.event_proxy_client.SngEventIface;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.StringRedisTemplate;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.*;

/**
 *
 */
public class ClubServiceImpl implements ClubService {
    @Inject
    private JdbcTemplate jdbcTemplate;
    @Inject
    @Named("mongoCore")
    private MongoTemplate mongoTemplate;

    @Inject
    private StringRedisTemplate stringRedisTemplate;

    private static final long DEFAUTL_CLUB_SCORE = 50000l;
    private static final int MAX_USER_CNT = 6;
    private static final int SNG_BUY_IN = 10000;
    private static final String SNG_ROOM = "sng_rooms";
    private static final String MTT_ROOM = "mtt_rooms";
    private static final String MTT_TABLE_USER = "mtt_rooms_users";
    private static final String CLUB_AGENTS = "club_agents";
    private static final String MTT_REWARD_TEMPLATE = "mtt_reward_template";
    private final static String SNG_TABLES = "sng_tables";
    private static final String AWARD_KEY = "pokerwinner_award";
    private List<SngRule> DEFAULT_SNG_RULE;
    private List<MttRule> DEFAULT_MTT_RULE;
    private static final String MTT_TASK_KEY = "club_mtt_task:";
    private static final String USER_CLUB_SCORE_LOG = "user_club_score_log";

    private void init() {
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream("config/sng_default_rules.json");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        DEFAULT_SNG_RULE = JSON.parseArray(configString, SngRule.class);


        InputStream inputConfig2 = this.getClass().getClassLoader().getResourceAsStream("config/mtt_default_rules.json");
        StringWriter writer2 = new StringWriter();
        try {
            IOUtils.copy(inputConfig2, writer2, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString2 = writer2.toString();
        DEFAULT_MTT_RULE = JSON.parseArray(configString2, MttRule.class);

    }

    public ClubServiceImpl() {
        init();
    }

    public List<Club> getUserClubs(String leaderId) {
        String sql = "select * from clubs where leader_id = ?";
        try {
            return jdbcTemplate.queryBeanList(sql, Club.class, leaderId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public Result saveUserClubs(String leaderId, String clubName, String clubLogo, String clubMark) {
        if (StringUtils.isBlank(clubName)) {
            return new Result(-1, "俱乐部名不能为空");
        }
        if (StringUtils.isBlank(clubLogo)) {
            return new Result(-1, "请上传俱乐部logo");
        }
        List<Club> clubs = getClubByClubName(clubName);
        if (clubs!= null && clubs.size() > 0) {
            return new Result(-1, "该名称已经存在");
        }
        List<Club> clubsList = getUserClubs(leaderId);
        if (clubsList != null && clubsList.size() > 0) {
            return new Result(-1, "你已经创建过俱乐部");
        }
        String sql = "insert into clubs(club_id, club_name, club_logo, club_mark, leader_id, create_date, status, score)" +
                " values(?,?,?,?,?,?,?, ?)";
        try {
            Club club = jdbcTemplate.insertBean(sql, Club.class, UUID.randomUUID().toString().replace("-", ""),
                    clubName, clubLogo, clubMark, leaderId, System.currentTimeMillis(), "normal", DEFAUTL_CLUB_SCORE);
            return new Result(club);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Result(-1, "添加失败");
    }

    public Result createClubSNGRoom(String leaderId, String roomName, String roomDesc, long fee,
                                   String[] phRewards, long[] scores, int maxTableNums, int tableUsers, String[] awardIds) {
        if (StringUtils.isBlank(roomName)) {
            return new Result(-1, "请填写房间名称");
        }
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs.size() == 0) {
            return new Result(-1, "你还没有创建俱乐部无法创建房间");
        }
        ClubAward[] clubAwards = null;
        if (awardIds != null && awardIds.length > 0) {
            clubAwards = new ClubAward[awardIds.length];
            int i = 0;
            for (String awardId : awardIds) {
                 ClubAward clubAward;
                 if (StringUtils.isBlank(awardId)) {
                     clubAward = new ClubAward();
                 } else {
                     clubAward = getAvailableAwardById(awardId);
                     if (clubAward == null) {
                         return new Result(-1, "奖品信息不存在");
                     }
                 }
                 clubAwards[i] = clubAward;
                 i++;
            }
        }
        SngRoom sngRoom = new SngRoom();
        sngRoom.setRoomName(roomName);
        sngRoom.setClubId(clubs.get(0).getClubId());
        sngRoom.setRoomDesc(roomDesc);
        sngRoom.setFee(Math.abs(fee));
        sngRoom.setIsEnd(0);
        if (tableUsers != MAX_USER_CNT) {
            tableUsers = 9;
        }
        sngRoom.setUserCnt(tableUsers);
        sngRoom.setMaxTableNums(maxTableNums);
        sngRoom.setBuyIn(SNG_BUY_IN);
        List<SngReward> sngRewards = Lists.newArrayList();
        if (clubAwards == null) {
            for (int i = 0; i < scores.length; i++) {
                SngReward sngReward = new SngReward();
                sngReward.setRanking(i + 1);
                sngReward.setScore(scores[i]);
                sngRewards.add(sngReward);
            }
        } else {
            for (int i = 0; i < clubAwards.length; i++) {
                 SngReward sngReward = new SngReward();
                 sngReward.setRanking(i + 1);
                 sngReward.setPhysicalReward(clubAwards[i].getAwardName());
                 sngReward.setAwardId(clubAwards[i].getAwardId());
                 sngReward.setScore(scores[i]);
                 sngRewards.add(sngReward);
            }
        }
        sngRoom.setSngRewards(sngRewards);
        sngRoom.setRules(DEFAULT_SNG_RULE);
        sngRoom.setCreateDate(System.currentTimeMillis());
        mongoTemplate.save(sngRoom, SNG_ROOM);
        return new Result(sngRoom);
    }

    @Override
    public Result createClubMttRoom(String leaderId, String gameName, long taxFee,
                                    int minUsers, int maxUsers, long startTime,
                                    int startBuyIn, int buyInCnt, String rebuyDesc,
                                    String rewardMark, int signDelayMins, int maxRebuyBlindLvl,
                                    int upgradeBlindSecs,
                                    String[] phRewards, long[] scores,String[] awardIds) {
        upgradeBlindSecs = Math.max(60, upgradeBlindSecs);
        if (StringUtils.isBlank(gameName)) {
            return new Result(-1, "请填写房间名称");
        }
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs.size() == 0) {
            return new Result(-1, "你还没有创建俱乐部无法创建房间");
        }
        if (startTime - System.currentTimeMillis() < 3 * 60000) {
            return new Result(-1, "开始时间必须大于当前时间的3分钟之后");
        }
        ClubAward[] clubAwards = null;
        if (awardIds != null && awardIds.length > 0) {
            clubAwards = new ClubAward[awardIds.length];
            int i = 0;
            for (String awardId : awardIds) {
                ClubAward clubAward;
                if (StringUtils.isBlank(awardId)) {
                    clubAward = new ClubAward();
                } else {
                    clubAward = getAvailableAwardById(awardId);
                    if (clubAward == null) {
                        return new Result(-1, "奖品信息不存在");
                    }
                }
                clubAwards[i] = clubAward;
                i++;
            }
        }
        Club club = clubs.get(0);
        MttRoom mttRoom = new MttRoom();
        mttRoom.setClubId(club.getClubId());
        mttRoom.setTaxFee(taxFee);
        mttRoom.setPoolFee(0);
        mttRoom.setRebuyDesc(rebuyDesc);
        mttRoom.setRoomName(gameName);
        mttRoom.setStartMills(startTime);
        mttRoom.setStartBuyIn(startBuyIn);
        mttRoom.setSignDelayMins(signDelayMins);
        mttRoom.setCanRebuyBlindLvl(maxRebuyBlindLvl);
        mttRoom.setCreateDate(System.currentTimeMillis());
        mttRoom.setMaxUsers(maxUsers);
        mttRoom.setMinUsers(minUsers);
        mttRoom.setAvailable(0);
        mttRoom.setBuyInCnt(buyInCnt);
        mttRoom.setRewardMark(rewardMark);
        mttRoom.setStatus(0);
        mttRoom.setCurretBlindLvl(-1);
        mttRoom.setTableUsers(9);
        mttRoom.setIsPublish(false);
        List<MttReward> mttRewards = Lists.newArrayList();
        if (clubAwards == null) {
            for (int i = 0; i < scores.length; i++) {
                MttReward mttReward = new MttReward();
                mttReward.setRanking(i + 1);
                mttReward.setVirtualNums(scores[i]);
                mttReward.setVirtualType(2);
                mttRewards.add(mttReward);
            }
        } else {
            for (int i = 0; i < clubAwards.length; i++) {
                MttReward mttReward = new MttReward();
                mttReward.setRanking(i + 1);
                mttReward.setVirtualNums(scores[i]);
                mttReward.setVirtualType(2);
                mttReward.setPhysicalName(clubAwards[i].getAwardName());
                mttReward.setPhysicalId(clubAwards[i].getAwardId());
                mttRewards.add(mttReward);
            }
        }
        mttRoom.setRewards(mttRewards);
        List<MttRule> cloneRules = new ArrayList<>();
        for (MttRule mttRule : DEFAULT_MTT_RULE) {
            MttRule cloneRule = (MttRule) mttRule.clone();
            cloneRule.setUpgradeSecs(upgradeBlindSecs);
            cloneRules.add(cloneRule);
        }
        int index = Math.min(maxRebuyBlindLvl, cloneRules.size());
        for (int i = 0; i < index; i++) {
             cloneRules.get(i).setReBuyIn(startBuyIn);
        }
        mttRoom.setRules(cloneRules);
        mongoTemplate.save(mttRoom, MTT_ROOM);

        testSaveUsers(mttRoom.getRoomId());
        return new Result(mttRoom);
    }

    private void testSaveUsers(String gameId) {
        List<MttTableUser> list = testAddUsers();
        for(MttTableUser mttTableUser : list) {
            mttTableUser.setGameId(gameId);
            mongoTemplate.save(mttTableUser, "mtt_rooms_users");
        }
    }

    private static List<MttTableUser> testAddUsers() {
          String str = "[{" +
                  "            \"userId\" : \"e213538def914334bf6207a2963b4ffb\"," +
                  "            \"nickName\" : \"estMobile960\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"13a24adb054c4c9aaa19a5a1644de117\"," +
                  "            \"nickName\" : \"estMobile961\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"91d07825be0b426c9e3ba599fa1965b6\"," +
                  "            \"nickName\" : \"estMobile962\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"3b0a8bcf9cf64bb4b2c226cb1633b2e8\"," +
                  "            \"nickName\" : \"estMobile963\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"e4c37f34256b4416b590b5d7bba901ac\"," +
                  "            \"nickName\" : \"estMobile964\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"6cbb6625281e4bc6a7a7962fee240238\"," +
                  "            \"nickName\" : \"estMobile965\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"45ccfe77a0fd4354b211134b616158fc\"," +
                  "            \"nickName\" : \"estMobile966\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"ec0efba70515449e9cde393d99a936f2\"," +
                  "            \"nickName\" : \"estMobile967\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"46b51faaf4904dacafde8a91b27b7ad3\"," +
                  "            \"nickName\" : \"estMobile968\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"5f0574180ce94a3997511a92397b93ba\"," +
                  "            \"nickName\" : \"estMobile969\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"a2cc2d6bcf984c69bca9ccde645d7622\"," +
                  "            \"nickName\" : \"estMobile970\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"f4538ca0587c4418a2cdf44ce087e8a4\"," +
                  "            \"nickName\" : \"estMobile971\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"01b4c6a2fd40436ca34b9007fb8ff580\"," +
                  "            \"nickName\" : \"estMobile972\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"ffa4be5efd8d4022814391acad61b39a\"," +
                  "            \"nickName\" : \"estMobile973\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"58b754017fad4852b1feea70f6d2dd65\"," +
                  "            \"nickName\" : \"estMobile974\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"ef04086359b24b7db729a747f5064680\"," +
                  "            \"nickName\" : \"estMobile975\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"044027de5ee8478f8ff3f2399928170b\"," +
                  "            \"nickName\" : \"estMobile976\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"d00504734d1a43d7bd3b4a97085e35f5\"," +
                  "            \"nickName\" : \"estMobile977\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"efd31165c7164c4694e7df55e97fc4d6\"," +
                  "            \"nickName\" : \"estMobile978\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"0c55459be7b04e85bb28f834d9462182\"," +
                  "            \"nickName\" : \"estMobile979\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"79e7a7dc901c465788cc64ec82d32425\"," +
                  "            \"nickName\" : \"estMobile980\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"2f95931db7f34fc59e69d6b3b57dbb01\"," +
                  "            \"nickName\" : \"estMobile981\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"4a71fcceff3043bc8bd56980d5ce8883\"," +
                  "            \"nickName\" : \"estMobile982\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"b9c5d92ca07b46448eeeb3a2935ca328\"," +
                  "            \"nickName\" : \"estMobile983\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"c60220931a284c448a9eb2927a1eb95d\"," +
                  "            \"nickName\" : \"estMobile984\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"4718097c52f74f88b881576761cee974\"," +
                  "            \"nickName\" : \"estMobile985\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"1c34a7b8258f466190be7d4957d5225f\"," +
                  "            \"nickName\" : \"estMobile986\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"4cbe8942998c417cbcb967c28cf7369c\"," +
                  "            \"nickName\" : \"estMobile987\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"df8356fa94fe49bc93b0587691ac5f5a\"," +
                  "            \"nickName\" : \"estMobile988\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"cfbc2dacd3f34192b5f229ea721fe81f\"," +
                  "            \"nickName\" : \"estMobile989\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"2d0839cc1ada4f68b57e4e4bd682ed9c\"," +
                  "            \"nickName\" : \"estMobile990\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"0c7bde390ac34b4e89917542a74df00b\"," +
                  "            \"nickName\" : \"estMobile991\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"cd045b89272b44e898334559b3258baf\"," +
                  "            \"nickName\" : \"estMobile992\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"512e7ca2d01149c5a69db2e01ad3a5f8\"," +
                  "            \"nickName\" : \"estMobile993\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"90c0a28f4fc74579be831453547d2671\"," +
                  "            \"nickName\" : \"estMobile994\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"c55fc5515e3e4e95b74e803bd79890ec\"," +
                  "            \"nickName\" : \"estMobile995\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"30e58e919617429d935430dee1c12fff\"," +
                  "            \"nickName\" : \"estMobile996\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"60af6764040340388f6b9b672dfa7b30\"," +
                  "            \"nickName\" : \"estMobile997\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"1ab88fbc890b49059edaa59590b40e82\"," +
                  "            \"nickName\" : \"estMobile998\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"0dd7b1fcc11d40bd9b772749b00647dc\"," +
                  "            \"nickName\" : \"estMobile999\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"bcab1395572644fab2eace3b2d6a377d\"," +
                  "            \"nickName\" : \"estMobile889\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"753117ef08c341e0a7ea0c75cfbc4760\"," +
                  "            \"nickName\" : \"estMobile888\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"0ae5875a2e0e48c0882fba0bdb4dba3a\"," +
                  "            \"nickName\" : \"estMobile887\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"af42ef20769546bd9d8e99ae58690484\"," +
                  "            \"nickName\" : \"estMobile886\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"eebc07d3b28e43fa9595d6779fd0080a\"," +
                  "            \"nickName\" : \"estMobile885\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"5c7ea703485c4cae9da4e238155d11a0\"," +
                  "            \"nickName\" : \"estMobile884\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"d6075d482c3449e2b74e9ce9bd51676a\"," +
                  "            \"nickName\" : \"estMobile883\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"e73a5b4501714f228ff1f05ceb2c1923\"," +
                  "            \"nickName\" : \"estMobile882\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"5d48fabf8f5e45ff840fe044548d8112\"," +
                  "            \"nickName\" : \"estMobile881\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }, " +
                  "        {" +
                  "            \"userId\" : \"730eac1b534e480b95e44b0de3954f94\"," +
                  "            \"nickName\" : \"estMobile880\"," +
                  "            \"avatarUrl\" : \"http://obs.cn-north-1.myhwclouds.com/pw-public-test/user_avatar/20161104/581c4440e63751310eade017.png\"," +
                  "            \"joinTime\" :1480760948498," +
                  "            \"leftChips\" :5000," +
                  "            \"ranking\" : 0," +
                  "            \"rebuyCnt\" : 0," +
                  "            \"status\" : \"waiting\"" +
                  "        }]";
      return JSON.parseArray(str, MttTableUser.class);
    }



    public List<SngRoom> getSngRooms(String leaderId, long lastCreateDate, int count) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs.size() == 0) {
            return new ArrayList<>();
        }
        String clubId = clubs.get(0).getClubId();
        Query query = new Query();
        Criteria criteria = Criteria.where("clubId").is(clubId);
        if(lastCreateDate > 0) {
            criteria.and("createDate").lt(lastCreateDate);
        }
        query.addCriteria(criteria)
                .with(new Sort(new Sort.Order(Sort.Direction.DESC, "createDate"))).limit(count);
        List<SngRoom> sngRooms = mongoTemplate.find(query, SngRoom.class, SNG_ROOM);
        if (sngRooms != null && sngRooms.size() > 0) {
            for (SngRoom sngRoom : sngRooms) {
                 String rmId = sngRoom.getRoomId();
                 List<SngTable> sngTables = getSngTable(rmId);
                 int userJoin = 0;
                 if (sngTables != null && sngTables.size() > 0) {
                     for (SngTable sngTable : sngTables) {
                         if (sngTable.getTableUsers() != null) {
                             userJoin += sngTable.getTableUsers().size();
                         }

                     }
                 }
                sngRoom.setUserJoin(userJoin);
            }
        }
        return sngRooms;
    }

    public Result getMttRankings(String mttRoomId) { // 获取排名信息和桌子信息
        List<MttRanking> mttRankings = Lists.newArrayList();
        MttRoom mttRoom = getMttRoom(mttRoomId);
        if (mttRoom == null) {
            return new Result(mttRankings);
        }
        if (mttRoom.getStatus() == 0 || mttRoom.getStatus() == -2) {  // 0 未开始, 1已开始, -1 已结束 -2 已解散
            return new Result(mttRankings);
        }
        List<MttRanking> mttGameRankings = new ArrayList<>();
        List<MttTableUser> mttTableUsers = getMttTableUsers(mttRoomId);
        if (mttTableUsers != null && mttTableUsers.size() > 0) {
            Collections.sort(mttTableUsers, (o1, o2) -> {
                if (o1.getLeftChips() != 0 && o2.getLeftChips() != 0) {
                    return (int) (o2.getLeftChips() - o1.getLeftChips());
                }
                if (o1.getLeftChips() == 0 && o2.getLeftChips() == 0) {
                    return o1.getRanking() - o2.getRanking();
                }
                if (o1.getLeftChips() != 0 && o2.getLeftChips() == 0) {
                    return -1;
                }
                if (o1.getLeftChips() == 0 && o2.getLeftChips() != 0) {
                    return 1;
                }
                return 0;
            });
            int i = 0;
            long lastChips = 0;
            for (MttTableUser mttTableUser : mttTableUsers) {
                if (lastChips == 0 || lastChips != mttTableUser.getLeftChips()) {
                    i++;
                }
                lastChips = mttTableUser.getLeftChips();
                MttRanking mttGameRanking = new MttRanking();
                mttGameRanking.setRanking(i);
                mttGameRanking.setNickName(mttTableUser.getNickName());
                mttGameRanking.setLeftChips(mttTableUser.getLeftChips());
                mttGameRanking.setAvatarUrl(mttTableUser.getAvatarUrl());
                mttGameRankings.add(mttGameRanking);
            }
            Collections.sort(mttGameRankings, (o1, o2) -> o1.getRanking() - o2.getRanking());
        }
        return new Result(mttRankings);
    }

    public Result getMttTableInfos(String mttRoomId)   {
        try {
            Map<String,List<MttGameUserInfo>> map =
                    MttIface.instance().iface().mttTableInfo(mttRoomId);
            return new Result(map);
        } catch (TException e) {
            e.printStackTrace();
        }
        return new Result();
    }

    public Result publishMtt(String roomId, String leaderId) {
        MttRoom mttRoom = getMttRoom(roomId);
        if (mttRoom == null) {
            return new Result(-1, "发布失败,游戏信息不存在");
        }
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null) {
            return new Result(-1, "发布失败,游戏信息不存在");
        }
        if (!mttRoom.getClubId().equalsIgnoreCase(clubs.get(0).getClubId())) {
            return new Result(-2, "发布失败,不是当前用户创建的局");
        }
        if (mttRoom.isPublish()) {
            return new Result(-3, "该局已经发布");
        }
        if (mttRoom.getStartMills() - System.currentTimeMillis() < 3 * 60000 ) {
            return new Result(-4, "发布时间必须大于当前时间的3分钟后");
        }
        boolean ret = stringRedisTemplate.opsForZSet().add(MTT_TASK_KEY, roomId,  mttRoom.getStartMills());
        if (ret) {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(new ObjectId(roomId));
            query.addCriteria(criteria);

            Update update = new Update();
            update.set("isPublish", true).set("available", 1);
            mongoTemplate.updateMulti(query, update, MTT_ROOM);
            return new Result(0, "发布成功");
        } else {
            return new Result(-4, "发布失败");
        }
    }

    public List<MttRoomInfo> getMttRooms(String leaderId, long lastCreateDate, int count) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs.size() == 0) {
            return new ArrayList<>();
        }
        String clubId = clubs.get(0).getClubId();
        Query query = new Query();
        Criteria criteria = Criteria.where("clubId").is(clubId);
        if(lastCreateDate > 0) {
            criteria.and("createDate").lt(lastCreateDate);
        }
        query.addCriteria(criteria)
                .with(new Sort(new Sort.Order(Sort.Direction.DESC, "createDate"))).limit(count);
        List<MttRoom> mttRooms = mongoTemplate.find(query, MttRoom.class, MTT_ROOM);
        List<MttRoomInfo> roomInfos = Lists.newArrayList();
        if (mttRooms != null) {
            for(MttRoom mttRoom : mttRooms) {
                MttRoomInfo mttRoomInfo = new MttRoomInfo();
                mttRoomInfo.setRoomId(mttRoom.getRoomId());
                mttRoomInfo.setRoomName(mttRoom.getRoomName());
                mttRoomInfo.setCreateTimes(mttRoom.getCreateDate());
                mttRoomInfo.setFee(mttRoom.getTaxFee());
                mttRoomInfo.setStartTimes(mttRoom.getStartMills());
                int joinCnt = 0;
                int left = 0;
                List<MttTableUser> mttTableUsers = getMttTableUsers(mttRoom.getRoomId());
                joinCnt = mttTableUsers.size();
                for (MttTableUser mttTableUser : mttTableUsers) {
                    if (!"end".equals(mttTableUser.getStatus())) {
                        left++;
                    }
                }
                mttRoomInfo.setJoinUsersCnts(joinCnt);
                // 判断开始
                mttRoomInfo.setLeftUserCnts(left);
                mttRoomInfo.setStatus(mttRoom.getStatus());
                mttRoomInfo.setRewards(mttRoom.getRewards());
                mttRoomInfo.setPublish(mttRoom.isPublish());
                roomInfos.add(mttRoomInfo);
            }
        }
        return roomInfos;
    }

    public Map<String, Object> getRoomDetail(String roomId) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("room", null);
        result.put("tables", null);
        SngRoom sngRoom = getRoom(roomId);
        if (sngRoom != null) {
            result.put("sngRoom", sngRoom);
            List<SngTable> sngTables = getSngTable(roomId);
            result.put("tables", sngTables);
            int join = 0;
            if (sngTables != null && sngTables.size() > 0) {
                for (SngTable sngTable : sngTables) {
                    if (sngTable.getTableUsers() != null) {
                        join += sngTable.getTableUsers().size();
                        for (SngTableUser sngTableUser : sngTable.getTableUsers()) {
                            if(sngTable.getIsStarted() == 0) { // 未开始
                                sngTableUser.setUserChips(sngRoom.getBuyIn());
                            }
                            else if (sngTable.getIsEnd() == 1) { // 已经结束
                                sngTableUser.setUserChips(0);
                            }
                            else {
                                String chipStr  = (String) stringRedisTemplate.opsForHash()
                                        .get("user_sng_games_key:" + sngTableUser.getUserId(), sngTable.getTableId());
                                if (StringUtils.isNotBlank(chipStr)) {
                                    JSONObject jsonObject = JSON.parseObject(chipStr);
                                    Long userChips = jsonObject.getLong("chips");
                                    if (userChips == null) {
                                        userChips = 0L;
                                    }
                                    sngTableUser.setUserChips(userChips);
                                }
                            }
                        }
                    }
                }
            }
            sngRoom.setUserJoin(join);
        }
        return result;
    }

    public List<ClubUser> getClubUsers(String clubId) {
        List<ClubUser> clubUsers = Lists.newArrayList();
        String sql = "select t2.*, t1.score from user_clubs as t1 left join users as t2 on " +
                " t1.user_id = t2.id where t1.club_id = ? order by t1.create_date desc";
        try {
            List<ClubUser> result = jdbcTemplate.queryBeanList(sql, ClubUser.class, clubId);
            if (result != null) {
                clubUsers = result;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clubUsers;
    }

    public boolean closeRoom(String roomId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(new ObjectId(roomId));
        query.addCriteria(criteria);

        Update update = new Update();
        update.set("isEnd", 1);
        mongoTemplate.updateMulti(query, update, SNG_ROOM);

        List<SngTable> sngTables = getSngTable(roomId);
        if (sngTables != null && sngTables.size() > 0) {
            Map<String, List<String>> tableUserIds = Maps.newHashMap();
            for (SngTable sngTable : sngTables) {
                 if (sngTable.getIsStarted() == 0) {
                     List<SngTableUser> sngTableUsers = sngTable.getTableUsers();
                     if (sngTableUsers != null && sngTableUsers.size() > 0) {
                         List<String> userIds = Lists.newArrayList();
                         for (SngTableUser sngTableUser : sngTableUsers) {
                              userIds.add(sngTableUser.getUserId());
                         }
                         tableUserIds.put(sngTable.getTableId(), userIds);
                     }
                 }
            }
            if (tableUserIds.size() > 0) {
                try {
                    SngEventIface.instance().iface().closeSngRoom(roomId, tableUserIds);
                } catch (TException e) {
                    e.printStackTrace();
                }
            }

        }
        return true;
    }

    public boolean reduceUserScore(String leaderId, String userId, long score, String agentId){
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null || clubs.size() == 0) {
            return false;
        }
        Club club = clubs.get(0);
        try {
            boolean ret = ClubIface.instance().iface().reduceUserClubScore(userId, club.getClubId(), Math.abs(score));
            if (ret) {
                saveClubScoreLogs(club.getClubId(), agentId, score, userId, -1);
                //发送邮件
                MailIface.instance().iface().saveNotice(userId, club.getClubName() + "的管理员扣除了你" + Math.abs(score) + "俱乐部积分");
            }
            return ret;
        } catch (TException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean addUserScore(String leaderId, String userId, long score, String agentId) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null || clubs.size() == 0) {
            return false;
        }
        Club club = clubs.get(0);
        try {
            boolean ret = ClubIface.instance().iface().addUserClubScore(userId, club.getClubId(), Math.abs(score));
            if (ret) {
                saveClubScoreLogs(club.getClubId(), agentId, score, userId, 1);
                //发送邮件
                MailIface.instance().iface().saveNotice(userId, club.getClubName() + "的管理员给你增加了" + Math.abs(score) + "俱乐部积分");
            }
            return ret;
        } catch (TException e) {
            e.printStackTrace();
        }
        return false;
    }

    private SngRoom getRoom(String roomId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(new ObjectId(roomId));
        query.addCriteria(criteria);
        return mongoTemplate.findOne(query, SngRoom.class, SNG_ROOM);
    }

    private MttRoom getMttRoom(String roomId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(new ObjectId(roomId));
        query.addCriteria(criteria);
        return mongoTemplate.findOne(query, MttRoom.class, MTT_ROOM);
    }



    private List<SngTable> getSngTable(String roomId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("roomId").is(roomId);
        query.addCriteria(criteria);
        return mongoTemplate.find(query, SngTable.class, SNG_TABLES);
    }

    public List<Club> getClubByClubName(String clubName) {
        String sql = "select * from clubs where club_name = ?";
        try {
            return jdbcTemplate.queryBeanList(sql, Club.class, clubName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public Result createClubAward(String leaderId, String awardName) {
       List<Club> clubs = getUserClubs(leaderId);
       if (clubs == null || clubs.size() == 0) {
           return new Result(-1, "俱乐部不存在");
       }
       String clubId = clubs.get(0).getClubId();
       ClubAward clubAward = getAvailableAwardByName(clubId, awardName);
       if (clubAward == null) {
           clubAward = createNewClubAward(clubId, awardName);
           if (clubAward == null) {
               return new Result(-1, "添加奖品失败");
           }
       }
       return new Result(clubAward);
    }

    public Result getClubAllAwards(String leaderId) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null || clubs.size() == 0) {
            return new Result(-1, "俱乐部不存在");
        }
        String clubId = clubs.get(0).getClubId();
        String sql = "select * from club_award where club_id = ? and status = 1 order by create_time desc";
        try {
            List<ClubAward> clubAwards = jdbcTemplate.queryBeanList(sql, ClubAward.class, clubId);
            return new Result(clubAwards);
        } catch (SQLException e) {
            e.printStackTrace();
        }
       return new Result(Lists.newArrayList());
    }

    public Result getClubAwards(String leaderId, String awardId, long lastCreateDate, int count, int status) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs.size() == 0) {
            return new Result(-1, "俱乐部不存在");
        }
        String clubId = clubs.get(0).getClubId();
        String sql = "select cua.*, u.avatar_url from club_user_award cua left join users u on cua.user_id = u.id " +
                " where cua.club_id = ? and cua.award_id = ? and cua.status = ? ";
        if (lastCreateDate > 0) {
            sql += " and cua.create_time < ?";
        }
        sql += " order by cua.create_time desc limit ?";
        try {
            List<ClubUserAward> clubAwards;
            if (lastCreateDate > 0) {
                clubAwards = jdbcTemplate.queryBeanList(sql, ClubUserAward.class, clubId, awardId, status,
                        lastCreateDate, Math.min(100, Math.abs(count)));
            } else {
                clubAwards = jdbcTemplate.queryBeanList(sql, ClubUserAward.class, clubId, awardId, status,
                        Math.min(100, Math.abs(count)));
            }
            return new Result(clubAwards);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Result(Lists.newArrayList());

    }

    public Result validQRCode(String cuaId, String signature, long time, String leaderId) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs.size() == 0) {
            return new Result(-1, "俱乐部不存在");
        }
        String clubId = clubs.get(0).getClubId();
        if (System.currentTimeMillis() - time > 180000) {
            return new Result(-1, "二维码已过期");
        }
        String signature2 = MD5.encode(AWARD_KEY + cuaId + time);
        if (signature2 == null || !signature2.equals(signature)) {
            return new Result(-1, "二维码不正确");
        }
        ClubUserAward clubUserAward = getClubUserAwardById(cuaId);
        if (clubUserAward == null) {
            return new Result(-1, "奖品信息不存在");
        }
        ClubAward clubAward =  getAvailableAwardById(clubUserAward.getAwardId());
        if (clubAward == null) {
            return new Result(-1, "奖品信息不存在");
        }
        if (!clubAward.getClubId().equals(clubId)) {
            return new Result(-1, "奖品信息不存在");
        }
        if (clubUserAward.getStatus() == 1) {
            return new Result(-1, "奖品已领取过了");
        }
        if (updateClubUserAwardStatus(cuaId, 1)) {
            updateClubAwardReceiveNums(clubUserAward.getAwardId());
            return new Result("领取成功");
        }
        return new Result(-1, "领取失败");
    }

    public Result viewQRAward(String cuaId, String signature, long time, String leaderId) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs.size() == 0) {
            return new Result(-1, "俱乐部不存在");
        }
        String clubId = clubs.get(0).getClubId();
        String signature2 = MD5.encode(AWARD_KEY + cuaId + time);
        if (signature2 == null || !signature2.equals(signature)) {
            return new Result(-1, "二维码不正确");
        }
        ClubUserAward clubUserAward = getClubUserAwardById(cuaId);
        if (clubUserAward == null) {
            return new Result(-1, "奖品信息不存在");
        }
        ClubAward clubAward =  getAvailableAwardById(clubUserAward.getAwardId());
        if (clubAward == null) {
            return new Result(-1, "奖品信息不存在");
        }
        if (!clubAward.getClubId().equals(clubId)) {
            return new Result(-1, "奖品信息不存在");
        }
        if (clubUserAward.getStatus() == 1) {
            return new Result(-1, "奖品已领取过了");
        }
        return new Result(clubUserAward);
    }

    public Result gameMark(String roomId) {
        MttRoom mttGame = getMttRoom(roomId);
        if (mttGame == null) {
            return new Result();
        }
        MttMark mttGameMark = new MttMark();
        mttGameMark.setGameId(mttGame.getRoomId());
        mttGameMark.setName(mttGame.getRoomName());
        mttGameMark.setFee(mttGame.getTaxFee() + mttGame.getPoolFee());
        mttGameMark.setStartTime(mttGame.getStartMills());
        mttGameMark.setMinUsers(mttGame.getMinUsers());
        mttGameMark.setMaxUsers(mttGame.getMaxUsers());
        mttGameMark.setBeginBuyIn(mttGame.getStartBuyIn());
        mttGameMark.setRebuyDesc(mttGame.getRebuyDesc());
        mttGameMark.setFeeType("积分");
        mttGameMark.setRebuyDesc("第" + mttGame.getCanRebuyBlindLvl() + "级别可以rebuy" + mttGame.getBuyInCnt() + "次数");
        mttGameMark.setTemplateRewardList(mttGame.getTemplateRewards());
        return new Result(mttGameMark);
    }

    public Result mttRules(String roomId) {
        try {
            List<MttGameRule> rules = MttIface.instance().iface().mttGameRules(roomId);
            return new Result(rules);
        } catch (TException e) {
            e.printStackTrace();
        }
        return new Result(new ArrayList<>());
    }

    public Result mttRewards(String roomId) {
        try {
            List<MttGameReward> rewards = MttIface.instance().iface().mttGameRewards(roomId);
            return new Result(rewards);
        } catch (TException e) {
            e.printStackTrace();
        }
        return new Result(new ArrayList<>());
    }

    public Result mttRankings(String roomId) {
        MttRoom mttGame = getMttRoom(roomId);
        if (mttGame == null) {
            return new Result(new ArrayList<>());
        }
        List<MttRanking> mttGameRankings = new ArrayList<>();
        List<MttTableUser> mttTableUsers = getMttTableUsers(roomId);
        if (mttTableUsers  != null && mttTableUsers.size() > 0) {
            Collections.sort(mttTableUsers, (o1, o2) -> {
                if (o1.getLeftChips() != 0 && o2.getLeftChips() != 0) {
                    return (int) (o2.getLeftChips() - o1.getLeftChips());
                }
                if (o1.getLeftChips() == 0 && o2.getLeftChips() == 0) {
                    return o1.getRanking() - o2.getRanking();
                }
                if (o1.getLeftChips() != 0 && o2.getLeftChips() == 0) {
                    return -1;
                }
                if (o1.getLeftChips() == 0 && o2.getLeftChips() != 0) {
                    return 1;
                }
                return 0;
            });
            int i = 0;
            long lastChips = 0;
            for (MttTableUser mttTableUser : mttTableUsers) {
                 if (lastChips == 0 || lastChips != mttTableUser.getLeftChips()) {
                     i++;
                 }
                 lastChips = mttTableUser.getLeftChips();
                 MttRanking mttGameRanking = new MttRanking();
                 mttGameRanking.setRanking(i);
                 mttGameRanking.setNickName(mttTableUser.getNickName());
                 mttGameRanking.setLeftChips(mttTableUser.getLeftChips());
                 mttGameRanking.setAvatarUrl(mttTableUser.getAvatarUrl());
                 mttGameRankings.add(mttGameRanking);
            }
            Collections.sort(mttGameRankings, (o1, o2) -> o1.getRanking() - o2.getRanking());
        }
        return new Result(mttGameRankings);
    }

    public Result mttGameDetail(String roomId) {
        try {
             MttGameDetail detail = MttIface.instance().iface().mttGameDetail(roomId);
            return new Result(detail);
        } catch (TException e) {
            e.printStackTrace();
        }
        return new Result();
    }

    public Result mttTableUsersInfo(String roomId) {
        try {
            Map<String, List<MttGameUserInfo>> infos = MttIface.instance().iface().mttTableInfo(roomId);
            if(infos != null) {
               return new Result(infos.values());
            }
        } catch (TException e) {
            e.printStackTrace();
        }
        return new Result();
    }

    @Override
    public Result setRewardTemplate(String leaderId, String id, String name,
                                    String[] rankingNames, String[] rankingValues) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null || clubs.size() == 0) {
            return new Result("-1", "你还没有创建俱乐部");
        }
        Club club = clubs.get(0);
        String clubId = club.getClubId();
        if (rankingNames == null || rankingValues == null || rankingNames.length != rankingValues.length) {
            return new Result("-2", "奖品信息有误");
        }
        if(StringUtils.isBlank(id)) {
            //添加
            MttRewardTemplate mttRewardTemplate = new MttRewardTemplate();
            mttRewardTemplate.setClubId(clubId);
            mttRewardTemplate.setName(name);
            mttRewardTemplate.setCreateTime(System.currentTimeMillis());
            List<MttRewardTemplate.Reward> rewards = new ArrayList<>();
            for (int i = 0; i < rankingNames.length; i++) {
                String rname = rankingNames[i];
                String rvalue = rankingValues[i];
                MttRewardTemplate.Reward reward = new MttRewardTemplate.Reward();
                reward.setRankingName(rname);
                reward.setRankingValue(rvalue);
                rewards.add(reward);
            }
            mttRewardTemplate.setRewardList(rewards);
            mongoTemplate.insert(mttRewardTemplate, MTT_REWARD_TEMPLATE);
        }
        else  {
            Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
            Query query = new Query();
            query.addCriteria(criteria);
            List<MttRewardTemplate> mttRewardTemplates = mongoTemplate.find(query,
                    MttRewardTemplate.class, MTT_REWARD_TEMPLATE);
            if (mttRewardTemplates != null && mttRewardTemplates.size() > 0) {
                MttRewardTemplate mttRewardTemplate = mttRewardTemplates.get(0);
                mttRewardTemplate.setName(name);
                List<MttRewardTemplate.Reward> rewards = new ArrayList<>();
                for (int i = 0; i < rankingNames.length; i++) {
                    String rname = rankingNames[i];
                    String rvalue = rankingValues[i];
                    MttRewardTemplate.Reward reward = new MttRewardTemplate.Reward();
                    reward.setRankingName(rname);
                    reward.setRankingValue(rvalue);
                    rewards.add(reward);
                }
                mttRewardTemplate.setRewardList(rewards);

                Query query2 = new Query();
                Criteria criteria2 = Criteria.where("_id").is(new ObjectId(id));
                query2.addCriteria(criteria2);

                Update update2 = new Update();
                update2.set("name", name);
                update2.set("rewardList", mttRewardTemplate.getRewardList());
                mongoTemplate.updateMulti(query2, update2, MTT_REWARD_TEMPLATE);
            }
        }
        return new Result("0", "设置成功");
    }
    public Result rewardTemplates(String leaderId) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null || clubs.size() == 0) {
            return new Result("-1", "你还没有创建俱乐部");
        }
        Club club = clubs.get(0);
        String clubId = club.getClubId();
        Criteria criteria = Criteria.where("clubId").is(clubId);

        Query query = new Query();
        query.addCriteria(criteria);
        query.with(new Sort(Sort.Direction.DESC, "createTime"));

        List<MttRewardTemplate> mttRewardTemplates = mongoTemplate.find(query,
                MttRewardTemplate.class, MTT_REWARD_TEMPLATE);
        return new Result(mttRewardTemplates);
    }

    public Result setMttReward(String leaderId, String roomId, String[] rankingNames, String[] rankingValues) {
        if (rankingNames == null || rankingValues == null || rankingNames.length != rankingValues.length) {
            return new Result("-1", "奖品信息有误");
        }
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null || clubs.size() == 0) {
            return new Result("-2", "你还没有创建俱乐部");
        }
        Club club = clubs.get(0);
        String clubId = club.getClubId();
        MttRoom mttRoom = getMttRoom(roomId);
        if (mttRoom == null) {
            return new Result("-3", "比赛不存在");
        }
        if (!mttRoom.getClubId().equals(clubId)) {
            return new Result("-4", "你没有权限设置此项");
        }
        List<TemplateReward> templateRewards = new ArrayList<>();
        for (int i = 0; i < rankingValues.length; i++) {
            TemplateReward templateReward = new TemplateReward();
            templateReward.setRankingName(rankingNames[i]);
            templateReward.setRankingValue(rankingValues[i]);
            templateRewards.add(templateReward);
        }

        Query query2 = new Query();
        Criteria criteria2 = Criteria.where("_id").is(new ObjectId(mttRoom.getRoomId()));
        query2.addCriteria(criteria2);
        Update update2 = new Update();
        update2.set("templateRewards", templateRewards);
        StringBuilder stringBuilder = new StringBuilder();
        for(TemplateReward templateReward : templateRewards) {
            stringBuilder.append(templateReward.getRankingName())
                    .append("  ")
                    .append(templateReward.getRankingValue())
                    .append("\n");
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.substring(0, stringBuilder.length() - 1);
        }
        String remark = stringBuilder.toString();
        update2.set("rewardMark", remark);

        mongoTemplate.updateMulti(query2, update2, MTT_ROOM);
        return new Result("0", "设置成功");
    }

    public Result addClubAgent(String leaderId, String name) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null || clubs.size() == 0) {
            return new Result("-1", "你还没有创建俱乐部");
        }
        Club club = clubs.get(0);
        String clubId = club.getClubId();
        ClubAgent clubAgent = getClubAgentByName(clubId, name);
        if (clubAgent != null) {
            return new Result("-2", "该名称已经存在");
        }
        clubAgent = new ClubAgent();
        clubAgent.setClubId(clubId);
        clubAgent.setAgentName(name);
        clubAgent.setCreateTime(System.currentTimeMillis());
        clubAgent.setStatus(1);
        mongoTemplate.insert(clubAgent, CLUB_AGENTS);
        return new Result("0", "添加成功");
    }

    public Result getClubAgents(String leaderId) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs == null || clubs.size() == 0) {
            return new Result("-1", "你还没有创建俱乐部");
        }
        Club club = clubs.get(0);
        String clubId = club.getClubId();
        Criteria criteria = Criteria.where("clubId").is(clubId);
        criteria.and("status").is(1);
        Query query = new Query();
        query.addCriteria(criteria);
        List<ClubAgent> clubAgents = mongoTemplate.find(query, ClubAgent.class, CLUB_AGENTS);
        return new Result("0", clubAgents);
    }

    public Result delClubAgent(String agentId) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(new ObjectId(agentId));
        query.addCriteria(criteria);
        Update update = new Update();
        update.set("status", 0);
        mongoTemplate.updateFirst(query, update, CLUB_AGENTS);
        return new Result("0", "更新成功");
    }

    public Result getUserScoreLog(String leaderId, String agentId, long lastCreateDate, int count) {
        List<Club> clubs = getUserClubs(leaderId);
        if (clubs.size() == 0) {
            return new Result("0", new ArrayList<>());
        }
        String clubId = clubs.get(0).getClubId();
        Query query = new Query();
        Criteria criteria = Criteria.where("clubId").is(clubId);
        if(lastCreateDate > 0) {
            criteria.and("createTime").lt(lastCreateDate);
        }
        if (StringUtils.isNotBlank(agentId)) {
            criteria.and("agentId").is(agentId);
        }
        query.addCriteria(criteria)
             .with(new Sort(new Sort.Order(Sort.Direction.DESC, "createTime"))).limit(count);
        List<UserClubScoreLog> clubScoreLogs = mongoTemplate.find(query, UserClubScoreLog.class, USER_CLUB_SCORE_LOG);
        return new Result("0", clubScoreLogs);
    }

    private ClubAgent getClubAgentByName(String clubId, String name) {
        Criteria criteria = Criteria.where("clubId").is(clubId);
        criteria.and("agentName").is(name);
        criteria.and("status").is(1);
        Query query = new Query();
        query.addCriteria(criteria);
        List<ClubAgent> clubAgents = mongoTemplate.find(query, ClubAgent.class, CLUB_AGENTS);
        if (clubAgents == null || clubAgents.size() == 0) {
           return null;
        }
        return clubAgents.get(0);
    }
    private ClubAgent getClubAgentId(String agentId) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(agentId));
        Query query = new Query();
        query.addCriteria(criteria);
        List<ClubAgent> clubAgents = mongoTemplate.find(query, ClubAgent.class, CLUB_AGENTS);
        if (clubAgents == null || clubAgents.size() == 0) {
            return null;
        }
        return clubAgents.get(0);
    }

    private List<MttTableUser> getMttTableUsers(String gameId) {
        Criteria criteria = Criteria.where("gameId").is(gameId);
        Query query = new Query();
        query.addCriteria(criteria);
        List<MttTableUser> mttTableUsers = mongoTemplate.find(query, MttTableUser.class, MTT_TABLE_USER);
        if(mttTableUsers == null) {
           mttTableUsers = new ArrayList<>();
        }
        return mttTableUsers;
    }

    private ClubUserAward getClubUserAwardById(String cuaId) {
        String sql = "select cua.*, ca.award_name from club_user_award cua left join club_award ca on ca.award_id = cua.award_id where cua_id = ?";
        try {
           return jdbcTemplate.queryBean(sql, ClubUserAward.class, cuaId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean updateClubUserAwardStatus(String cuaId, int status) {
        String sql = "update club_user_award set status = ?,  receive_time = ? where cua_id = ?";
        try {
            int ret = jdbcTemplate.update(sql, status, System.currentTimeMillis(), cuaId);
            return ret > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean updateClubAwardReceiveNums(String cuaId) {
        String sql = "update club_award set receive_num = receive_num + 1, not_receive_num = not_receive_num - 1 where award_id = ?";
        try {
            int ret = jdbcTemplate.update(sql, cuaId);
            return ret > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private ClubAward createNewClubAward(String clubId, String awardName) {
        String sql = "insert into club_award(award_id, award_name, club_id, status, create_time)" +
                " values(?,?,?,?,?)";
        try {
            return jdbcTemplate.insertBean(sql, ClubAward.class, UUID.randomUUID().toString().replace("-", ""),
                    awardName, clubId, 1, System.currentTimeMillis());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ClubAward getAvailableAwardByName(String clubId, String awardName) {
        String sql = "select * from club_award where club_id = ? and award_name = ? and status = 1";
        try {
            return jdbcTemplate.queryBean(sql, ClubAward.class, clubId, awardName.trim());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ClubAward getAvailableAwardById(String awardId) {
        String sql = "select * from club_award where award_id = ? and status = 1";
        try {
            return jdbcTemplate.queryBean(sql, ClubAward.class, awardId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveClubScoreLogs(String clubId, String agentId, long score, String userId,
                                   int type) {
        String userName = userId;
        try {
            UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
            userName = userDetail.getNickName();
        } catch (TException e) {
            e.printStackTrace();
        }
        String agentName = "";
        if (StringUtils.isNoneBlank(agentId)) {
            ClubAgent clubAgent = getClubAgentId(agentId);
            if (clubAgent != null) {
                agentName = clubAgent.getAgentName();
            }
        }
        UserClubScoreLog userClubScoreLog = new UserClubScoreLog();
        userClubScoreLog.setAgentName(agentName);
        userClubScoreLog.setAgentId(agentId);
        userClubScoreLog.setClubId(clubId);
        userClubScoreLog.setUserName(userName);
        userClubScoreLog.setScore(Math.abs(score));
        userClubScoreLog.setType(type);
        userClubScoreLog.setCreateTime(System.currentTimeMillis());
        mongoTemplate.insert(userClubScoreLog, USER_CLUB_SCORE_LOG);
    }
}
