package com.tiantian.leader.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.tiantian.leader.db.JdbcTemplate;
import com.tiantian.leader.model.IosPurchase;
import com.tiantian.leader.model.Leader;
import com.tiantian.leader.model.Order;
import com.tiantian.leader.model.UserProps;
import com.tiantian.leader.service.PayService;
import org.apache.commons.io.IOUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class PayServiceImpl implements PayService {
    @Inject
    private JdbcTemplate jdbcTemplate;

    private static List<IosPurchase> ALL_PURCHASE;

    public PayServiceImpl() {
        init();
    }

    private void init() {
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream("config/ios_purchase.json");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        ALL_PURCHASE = JSON.parseArray(configString, IosPurchase.class);
    }

    public List<IosPurchase> iosPurchase() {
        return ALL_PURCHASE;
    }

    public IosPurchase getPurchase(String code) {
        for (IosPurchase iosPurchase : ALL_PURCHASE) {
             if (iosPurchase.getCode().equals(code)) {
                 return iosPurchase;
             }
        }
        return null;
    }

    @Override
    public Order saveOrder(Order order) {
        Order $order = gerOrder(order.getOrderId());
        if ($order != null) {
            return $order;
        }
        order.setId(UUID.randomUUID().toString().replace("-", ""));
        String sql = "insert into leader_orders(id, order_id, product_code, product_count, amount, pay_status, send_status," +
                     " platform, create_time, last_update_time, user_id) values(?,?,?,?,?,?,?,?,?,?,?);";
        try {
            return jdbcTemplate.insertBean(sql, Order.class, order.getId(), order.getOrderId(), order.getProductCode(), order.getProductCount(),
                    order.getAmount(), order.getPayStatus(), order.getSendStatus(), order.getPlatform(),
            new java.sql.Date(order.getCreateTime().getTime()), new java.sql.Date(order.getLastUpdateTime().getTime()),
                    order.getUserId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Order gerOrder(String orderId) {
        String sql = "select * from leader_orders where order_id = ?";
        try {
            return jdbcTemplate.queryBean(sql, Order.class, orderId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean updateOrderPaySucc(String orderId) {
        String sql = "update leader_orders set pay_status = ?, last_update_time = ? where order_id = ?";
        try {
            return jdbcTemplate.update(sql, "success",  new java.sql.Date(System.currentTimeMillis()), orderId) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean updateOrderSendSucc(String orderId) {
        String sql = "update leader_orders set send_status = ?, last_update_time = ? where order_id = ?";
        try {
            return jdbcTemplate.update(sql, "yes",  new java.sql.Date(System.currentTimeMillis()), orderId) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String orderSend(Order order) {
        IosPurchase iosPurchase = getPurchase(order.getProductCode());
        UserProps userProps = getUserProps(order.getUserId(), iosPurchase.getGoodsCode());
        if (userProps != null) {
            addUserProps(order.getUserId(), iosPurchase.getGoodsCode(), iosPurchase.getGoodsNum());
        } else {
            savePurchase(order.getUserId(), iosPurchase.getGoodsCode(), iosPurchase.getGoodsNum());
        }
        boolean ret = updateOrderSendSucc(order.getOrderId());
        if (ret) {
            return "success";
        }
        return "fail";
    }

    private UserProps getUserProps(String userId, String propId) {
        String sql = "select * from leader_user_props where prop_code = ? and user_id = ?";
        try {
            return jdbcTemplate.queryBean(sql, UserProps.class, propId, userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private UserProps savePurchase(String userId, String propCode, long nums) {
        String sql = "insert into leader_user_props(id, user_id, prop_code, prop_nums) values(?,?,?,?);";
        try {
            return jdbcTemplate.insertBean(sql, UserProps.class, UUID.randomUUID().toString().replace("-", ""),
                    userId, propCode, nums);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean addUserProps(String userId, String propCode, long nums) {
        String sql = "update leader_user_props set prop_nums = prop_nums + ? where user_id = ? and prop_code = ?";
        try {
            return jdbcTemplate.update(sql, nums,  userId, propCode) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
