package com.tiantian.leader.service.impl;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.model.UserAdvice;
import com.tiantian.leader.service.AdviceService;
import com.tiantian.leader.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;
import java.util.List;

/**
 *
 */
public class AdviceServiceImpl implements AdviceService {
    @Inject
    @Named("mongoCore")
    private MongoTemplate mongoTemplate;

    private static final String USER_ADVICE = "user_advice";

    public Result userAdvice(String userId, String content) {
        if (StringUtils.isBlank(content)) {
            return new Result(-1, "建议内容不能为空");
        }
        if (content.trim().length() > 200) {
            return new Result(-2, "建议内容长度不能超过200字");
        }
        Query query = new Query();
        Criteria criteria = Criteria.where("userId").is(userId).and("createDate")
                .gte(DateUtils.dayBeginMills(new Date())).andOperator(Criteria.where("createDate")
                        .lte(DateUtils.dayEndMills(new Date())));
        query.addCriteria(criteria);
        List<UserAdvice> userAdviceList = mongoTemplate.find(query, UserAdvice.class, USER_ADVICE);
        if (userAdviceList != null && userAdviceList.size() >= 10) {
            return new Result(-3, "每天建议数量不能超过10条");
        }
        UserAdvice userAdvice = new UserAdvice();
        userAdvice.setUserId(userId);
        userAdvice.setContent(content);
        userAdvice.setCreateDate(System.currentTimeMillis());
        mongoTemplate.insert(userAdvice, USER_ADVICE);
        return new Result("提交成功");
    }
}
