package com.tiantian.leader.service.impl;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.model.LeaderOpLogs;
import com.tiantian.leader.service.LogService;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 *
 */
public class LogServiceImpl implements LogService {
    @Inject
    @Named("mongoCore")
    private MongoTemplate mongoTemplate;
    private static final String OP_LOGS = "leader_op_logs";

    public void saveOpLogs(String leaderId, String leaderName, String groupId, String type, String content) {
        LeaderOpLogs leaderOpLogs = new LeaderOpLogs();
        leaderOpLogs.setLeaderId(leaderId);
        leaderOpLogs.setLeaderName(leaderName);
        leaderOpLogs.setGroupId(groupId);
        leaderOpLogs.setType(type);
        leaderOpLogs.setContent(content);
        leaderOpLogs.setCreateDate(System.currentTimeMillis());
        mongoTemplate.save(leaderOpLogs, OP_LOGS);
    }

    public Result getOpLogList(String groupId, long lastCreateTime, int count) {
        Query query = new Query();
        Criteria criteria = Criteria.where("groupId").is(groupId);
        if (lastCreateTime > 0) {
            criteria.andOperator(Criteria.where("createDate").lt(lastCreateTime));
        }
        query.addCriteria(criteria).with(new Sort(new Sort.Order(Sort.Direction.DESC, "createDate")))
                .limit(Math.min(count, 100));
        List<LeaderOpLogs> result = mongoTemplate.find(query, LeaderOpLogs.class, OP_LOGS);
        return new Result(result);
    }
}
