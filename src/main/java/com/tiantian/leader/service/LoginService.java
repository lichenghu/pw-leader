package com.tiantian.leader.service;

import com.tiantian.leader.application.Result;

/**
 *
 */
public interface LoginService {
      Result login(String mobile, String pwd, String deviceToken, String platform);
      Result login(String token, String deviceToken, String platform);
      Result exit(String token);
}
