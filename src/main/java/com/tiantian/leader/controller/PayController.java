package com.tiantian.leader.controller;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.tiantian.leader.model.IosPurchase;
import com.tiantian.leader.model.JsonResult;
import com.tiantian.leader.model.Order;
import com.tiantian.leader.service.PayService;
import com.tiantian.leader.utils.IOSVerify;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;
import java.util.Date;

/**
 *
 */
public class PayController  extends Controller {
    @Inject
    public PayService payService;

    private static final String IOS_ORDER_PRE = "IOS";

    public void iosList() {
        getResponse().json().send(payService.iosPurchase());
    }

    public void iosPostOrder(@Param("receipt")String receipt,
                             @Param("userId")String userId) throws Exception {
        //苹果客户端传上来的收据,是最原据的收据
        System.out.println(receipt);
        String purchaseInf = IOSVerify.getPurchaseInfo(receipt);
        String iosProductId = IOSVerify.getProductIdFromPurchaseInfo(purchaseInf);
        IosPurchase iosPurchase = payService.getPurchase(iosProductId);
        if (iosPurchase == null) {
            getResponse().json().send(new JsonResult(1, "fail", null));
            return;
        }
        String orderId = getOrderId(receipt);
        Order order = new Order();
        // 生成订单ID
        order.setOrderId(orderId);
        order.setProductCount(iosPurchase.getGoodsNum());
        order.setAmount(iosPurchase.getMoney() * 1.0f);
        // 默认支付失败（未完成）
        order.setPayStatus("unfinished");
        order.setUserId(userId);
        order.setProductCode(iosProductId);
        order.setPlatform("ios");
        order.setSendStatus("no");
        order.setCreateTime(new Date());
        order.setLastUpdateTime(new Date());
        try {
            // 保存订单,订单信息如果在数据库中已存在则从数据库加载订单
            order = payService.saveOrder(order);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("========save ios order fail=======");
            getResponse().json().send(new JsonResult(1, "fail", null));
            return;
        }
        // 校验收据信息是否正确
        String status = IOSVerify.validReceipt(receipt);
        // 校验正确
        if (status != null && "0".equalsIgnoreCase(status)) {
            if (!order.getPayStatus().equals("success")) {
                // 更新订单支付状态为成功
                payService.updateOrderPaySucc(order.getOrderId());
                // 设置支付状态成功
                order.setPayStatus("success");
            }
           if (!order.getSendStatus().equals("yes")) {
               // 发放道具
               String result = payService.orderSend(order);
               // 发放道具失败
               if ("fail".equalsIgnoreCase(result)) {
                   // 发放失败
                   getResponse().json().send(new JsonResult(1, "sendFail", null));
                   return;
               }
           }
        }
        else {
            System.out.print("==============ios pay valid fail, status : " + status +
                    ",orderId:" + orderId + ",receipt:" + receipt);
            getResponse().json().send(new JsonResult(1, "fail", null));
            return;
        }
        getResponse().json().send(new JsonResult(1, "ok", null));
    }

    /**
     * 订单ID
     * @param receipt
     * @return
     */
    private String getOrderId(String receipt) {
        String md5Str = IOSVerify.md5(receipt);
        return IOS_ORDER_PRE + md5Str;
    }
}
