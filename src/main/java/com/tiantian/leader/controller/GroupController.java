package com.tiantian.leader.controller;

import com.google.inject.Inject;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.model.LoginSession;
import com.tiantian.leader.service.GroupService;
import com.tiantian.leader.service.LogService;
import com.tiantian.leader.service.LoginService;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class GroupController extends Controller {
    @Inject
    GroupService groupService;
    @Inject
    LogService logService;

    public void getBuyInList() {
        Result result = groupService.getBuyInList();
        getResponse().json().send(result);
    }

    public void createGroup(@Param("token") String token,
                            @Param("buyIndex") int buyIndex,
                            @Param("hours") float hours,
                            @Param("safe") boolean safe) {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.createGroup(buyIndex, hours, safe,
                loginSession.getLeaderId());
        getResponse().json().send(result);
    }

    public void getGroups(@Param("$leaderId") String leaderId) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = groupService.getNotEndGroups($leaderId);
        getResponse().json().send(result);
    }

    public void getEndGroups(@Param("$leaderId") String leaderId,
                             @Param("lastCreateDate") long lastCreateDate,
                             @Param("count") int count) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = groupService.getEndGroups($leaderId, lastCreateDate, count);
        getResponse().json().send(result);
    }

    public void startGroupGame(@Param("groupId") String groupId) {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.startGroupGame(groupId, loginSession.getLeaderId(),
                loginSession.getNickName());
        getResponse().json().send(result);
    }

    public void groupUsers(@Param("groupId") String groupId) {
        Result result = groupService.groupUsers(groupId);
        getResponse().json().send(result);
    }

    public void getApplications(@Param("groupId") String groupId) {
        Result result = groupService.getApplications(groupId);
        getResponse().json().send(result);
    }

    public void agreeBuyIn(@Param("groupId")String groupId,
                           @Param("userId")String userId) {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.agreeBuyIn(groupId, userId, loginSession.getLeaderId(),
                loginSession.getNickName());
        getResponse().json().send(result);
    }

    public void getAllOdds() {
        Result result = groupService.getAllOdds();
        getResponse().json().send(result);
    }

    public void getScoreboard(@Param("groupId")String groupId) {
        Result result = groupService.getScoreboard(groupId);
        getResponse().json().send(result);
    }

    public void getOpLogList(@Param("groupId")String groupId,
                             @Param("lastCreateTime") long lastTime,
                             @Param("count") int count) {
        Result result = logService.getOpLogList(groupId, lastTime, count);
        getResponse().json().send(result);
    }

    public void forceStandUp(@Param("groupId") String groupId,
                             @Param("userId") String userId) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = groupService.forceStandUp(groupId, userId, $leaderId);
        getResponse().json().send(result);
    }

    public void forceCloseGame(@Param("groupId") String groupId) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = groupService.forceCloseGame(groupId, $leaderId);
        getResponse().json().send(result);
    }

    public void getAddMoneyInfo(@Param("groupId") String groupId,
                                @Param("userId") String userId) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = groupService.getAddMoneyInfo(groupId, userId, $leaderId);
        getResponse().json().send(result);
    }

    public void addUserMoney(@Param("groupId") String groupId,
                             @Param("userId") String userId,
                             @Param("buyCnt") int buyCnt) {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.addUserMoney(groupId, userId, loginSession.getLeaderId(), buyCnt,
                loginSession.getNickName());
        getResponse().json().send(result);
    }

    public void ignoreUserApply(@Param("groupId") String groupId,
                                @Param("userId") String userId) {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);

        Result result = groupService.ignoreUserApply(groupId, userId, loginSession.getLeaderId(),
                loginSession.getNickName());
        getResponse().json().send(result);
    }

    public void getAllApplications() {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.getAllApplications(loginSession.getLeaderId());
        getResponse().json().send(result);
    }

    public void getAppCounts() {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.getAppCounts(loginSession.getLeaderId());
        getResponse().json().send(result);
    }

    public void getDayWinStatic() {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.getDayWinStatic(loginSession.getLeaderId());
        getResponse().json().send(result);
    }

    public void getWeekWinStatic() {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.getWeekWinStatic(loginSession.getLeaderId());
        getResponse().json().send(result);
    }

    public void getMonthWinStatic() {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.getMonthWinStatic(loginSession.getLeaderId());
        getResponse().json().send(result);
    }

    public void getDayBrief(@Param("day")long day) {
        String $token = getRequest().getHeader("token");
        LoginSession loginSession = LocalCache.get($token);
        Result result = groupService.getDayBrief(loginSession.getLeaderId(), day);
        getResponse().json().send(result);
    }

    public void exeTask(@Param("day")String day) {
        Result result = groupService.exeTask(day);
        getResponse().json().send(result);
    }
}
