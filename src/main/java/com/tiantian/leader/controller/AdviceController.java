package com.tiantian.leader.controller;

import com.google.inject.Inject;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.service.AdviceService;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class AdviceController extends Controller {
    @Inject
    AdviceService adviceService;

    public void userAdvice(@Param("content") String content) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = adviceService.userAdvice($leaderId, content);
        getResponse().json().send(result);
    }
}
