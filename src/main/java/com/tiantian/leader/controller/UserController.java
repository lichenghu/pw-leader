package com.tiantian.leader.controller;

import com.google.inject.Inject;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.model.LoginSession;
import com.tiantian.leader.model.UserProps;
import com.tiantian.leader.obs.ObsService;
import com.tiantian.leader.service.UserService;
import ro.pippo.controller.Controller;
import ro.pippo.core.FileItem;
import ro.pippo.core.Param;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 *
 */
public class UserController extends Controller {
    @Inject
    private UserService userService;
    @Inject
    private ObsService obsService;

    public void register(@Param("mobile") String mobile,
                         @Param("pwd") String pwd,
                         @Param("code") String code,
                         @Param("deviceToken") String deviceToken,
                         @Param("platform") String platform) {
        Result result = userService.createUser(mobile, pwd, code, deviceToken, platform);
        getResponse().json().send(result);
    }

    public void sendRegisterValidCode(@Param("mobile")String mobile) {
        Result result = userService.sendRegisterCode(mobile);
        getResponse().json().send(result);
    }

    public void sendUpdatePwdValidCode(@Param("mobile")String mobile) {
        Result result = userService.sendUpdatePwdCode(mobile);
        getResponse().json().send(result);
    }

    public void updatePwd(@Param("mobile")String mobile, @Param("pwd")String pwd, @Param("code")String code) {
        Result result = userService.updatePwd(mobile, pwd, code);
        getResponse().json().send(result);
    }

    public void updateNickName(@Param("nickName") String nickName) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = userService.updateNickName($leaderId, nickName);
        String token = getRouteContext().getHeader("token");
        LoginSession session = LocalCache.get(token);
        if (session != null && result.getCode() == 0) {
            session.setNickName(nickName);
        }
        getResponse().json().send(result);
    }

    public void updateEmail(@Param("email") String email) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = userService.updateEmail($leaderId, email);
        String token = getRouteContext().getHeader("token");
        LoginSession session = LocalCache.get(token);
        if (session != null && result.getCode() == 0) {
            session.setEmail(email);
        }
        getResponse().json().send(result);
    }

    public void updateAvatarUrl() {
        FileItem file = getRouteContext().getRequest().getFile("file");
        try {
            File uploadedFile = new File(file.getSubmittedFileName());
            try {
                file.write(uploadedFile);
                String url = obsService.upload(uploadedFile);
                String $leaderId = getRequest().getSession().get("$leaderId");
                Result result = userService.updateAvatarUrl($leaderId, url);
                getResponse().json().send(result);
                uploadedFile.delete();
                String token = getRouteContext().getHeader("token");
                LoginSession session = LocalCache.get(token);
                if (session != null) {
                    session.setAvatarUrl(url);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch (Exception e) {
            getResponse().json().send(new Result(-1, "上传失败"));
        }
    }

    public void systemTime() {
        getResponse().json().send(new Result(System.currentTimeMillis()));
    }

    public void version(@Param("version") String version,
                        @Param("platform") String platform,
                        @Param("status")String status) {
        Result result = userService.getVersion(version, platform, status);
        getResponse().json().send(result);
    }

    public void vipInfo() {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = userService.vipInfo($leaderId);
        getResponse().json().send(result);
    }


    public void userProps() {
        String $leaderId = getRequest().getSession().get("$leaderId");
        List<UserProps> result = userService.getUserProps($leaderId);
        getResponse().json().send(result);
    }

    public void giveProps(@Param("userId") String otherUserId,
                          @Param("propCode") String propCode,
                          @Param("num") int num) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = userService.giveProps($leaderId, otherUserId, propCode, num);
        getResponse().json().send(result);
    }
}
