package com.tiantian.leader.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.inject.Inject;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.model.Club;
import com.tiantian.leader.model.ClubUser;
import com.tiantian.leader.model.MttRoomInfo;
import com.tiantian.leader.model.SngRoom;
import com.tiantian.leader.obs.ObsService;
import com.tiantian.leader.service.ClubService;
import ro.pippo.controller.Controller;
import ro.pippo.core.FileItem;
import ro.pippo.core.Param;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class ClubController extends Controller {
    @Inject
    private ClubService clubService;
    @Inject
    private ObsService obsService;

    public void getUserClubs() {
        String $leaderId = getRequest().getSession().get("$leaderId");
        List<Club> result = clubService.getUserClubs($leaderId);
        getResponse().json().send(new Result(result));
    }


    public void saveUserClubs(@Param("clubName") String clubName,
                              @Param("clubLogo") String clubLogo,
                              @Param("clubMark") String clubMark) {
        String $leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.saveUserClubs($leaderId, clubName, clubLogo, clubMark);
        getResponse().json().send(result);
    }

    public void createClubSNGRoom(@Param("roomName")String roomName,
                                  @Param("roomDesc")String roomDesc,
                                  @Param("fee")long fee,
                                  @Param("phRewards")String[] phRewards,
                                  @Param("scores") long[] scores,
                                  @Param("maxTableNums") int maxTableNums,
                                  @Param("tableUsers") int tableUsers,
                                  @Param("awardIds") String[] awardIds) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.createClubSNGRoom(leaderId, roomName,roomDesc, fee, phRewards,
                scores, maxTableNums, tableUsers, awardIds);
        getResponse().json().send(result);
    }

    public void createClubMttRoom( @Param("gameName")String gameName,
                                   @Param("taxFee")long taxFee,
                                   @Param("minUsers")int minUsers,
                                   @Param("maxUsers")int maxUsers,
                                   @Param("startTime")long startTime,
                                   @Param("startBuyIn")int startBuyIn,
                                   @Param("buyInCnt")int buyInCnt,
                                   @Param("rebuyDesc")String rebuyDesc,
                                   @Param("rewardMark")String rewardMark,
                                   @Param("signDelayMins")int signDelayMins,
                                   @Param("maxRebuyBlindLvl")int maxRebuyBlindLvl,
                                   @Param("upgradeBlindSecs") int upgradeBlindSecs,
                                   @Param("phRewards") String[] phRewards,
                                   @Param("scores")long[] scores,
                                   @Param("awardIds")String[] awardIds) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.createClubMttRoom(leaderId, gameName, taxFee,
                minUsers, maxUsers, startTime, startBuyIn, buyInCnt, rebuyDesc, rewardMark,
                signDelayMins,  maxRebuyBlindLvl, upgradeBlindSecs, phRewards, scores, awardIds);
        getResponse().json().send(result);
    }

    public void getMttRooms(@Param("lastCreateDate")long lastCreateDate,
                            @Param("count")int count) {
        String leaderId = getRequest().getSession().get("$leaderId");
        List<MttRoomInfo> result = clubService.getMttRooms(leaderId, lastCreateDate, count);
        getResponse().json().send(new Result(result));
    }

    public void publishMtt(@Param("roomId") String roomId) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.publishMtt(roomId, leaderId);
        getResponse().json().send(result);
    }

    public void getSngRooms(@Param("lastCreateDate")long lastCreateDate,
                            @Param("count")int count) {
        String leaderId = getRequest().getSession().get("$leaderId");
        List<SngRoom> result = clubService.getSngRooms(leaderId,  lastCreateDate, count);
        getResponse().json().send(new Result(result));
    }


    public void roomDetail(@Param("roomId")String roomId) {
        Map<String, Object> result = clubService.getRoomDetail(roomId);
        getResponse().json().send(new Result(result));
    }

    public void uploadClubLogo() {
        FileItem file = getRouteContext().getRequest().getFile("file");
        try {
            File uploadedFile = new File(file.getSubmittedFileName());
            try {
                file.write(uploadedFile);
                String url = obsService.upload(uploadedFile);
                getResponse().json().send(new Result(0, url));

            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                uploadedFile.delete();
            }
        }
        catch (Exception e) {
            getResponse().json().send(new Result(-1, "上传失败"));
        }
    }

    public void getClubUsers(@Param("clubId")String clubId) {
         List<ClubUser> result = clubService.getClubUsers(clubId);
         getResponse().json().send(new Result(result));
    }

    public void closeRoom(@Param("roomId")String roomId) {
        boolean result = clubService.closeRoom(roomId);
        getResponse().json().send(new Result(result));
    }

    public void reduceUserScore(@Param("userId")String userId,
                                @Param("score")long score,
                                @Param("agentId") String agentId) {
        String leaderId = getRequest().getSession().get("$leaderId");
        boolean ret = clubService.reduceUserScore(leaderId, userId, score, agentId);
        Result result = null;
        if (ret) {
            result = new Result(0, "扣除成功");
        } else {
            result = new Result(-1, "扣除失败");
        }
        getResponse().json().send(result);
    }

    public void addUserScore(@Param("userId")String userId,
                             @Param("score")long score,
                             @Param("agentId") String agentId) {
        String leaderId = getRequest().getSession().get("$leaderId");
        boolean ret = clubService.addUserScore(leaderId, userId, score, agentId);
        Result result = null;
        if (ret) {
            result = new Result(0, "增加成功");
        } else {
            result = new Result(-1, "增加失败,请确认你的俱乐部总积分是否足够!");
        }
        getResponse().json().send(result);
    }

    public void createClubAward(@Param("awardName")String awardName) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.createClubAward(leaderId, awardName);
        getResponse().json().send(result);
    }

    public void getClubAllAwards() {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.getClubAllAwards(leaderId);
        getResponse().json().send(result);
    }

    public void getClubUserNotReceiveAwards(@Param("awardId")String awardId,
                                            @Param("lastCreateDate")long lastCreateDate,
                                            @Param("count")int count) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.getClubAwards(leaderId, awardId, lastCreateDate, count, 0);
        getResponse().json().send(result);
    }

    public void getClubUserReceiveAwards(@Param("awardId")String awardId,
                                         @Param("lastCreateDate")long lastCreateDate,
                                         @Param("count")int count) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.getClubAwards(leaderId, awardId, lastCreateDate, count, 1);
        getResponse().json().send(result);
    }

    public void validQRCode(@Param("jsonData") String jsonData) {
        JSONObject object = JSON.parseObject(jsonData);
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.validQRCode(object.getString("id"), object.getString("signature"), object.getLong("time"),
                leaderId);
        getResponse().json().send(result);
    }

    public void viewQRAward(@Param("jsonData") String jsonData) {
        String leaderId = getRequest().getSession().get("$leaderId");
        JSONObject object = JSON.parseObject(jsonData);
        Result result = clubService.viewQRAward(object.getString("id"), object.getString("signature"), object.getLong("time"),
                leaderId);
        getResponse().json().send(result);
    }


    public void gameMark(@Param("roomId")String roomId) {
        Result result = clubService.gameMark(roomId);
        getResponse().json().send(result);
    }

    public void mttRules(@Param("roomId")String roomId) {
        Result result = clubService.mttRules(roomId);
        getResponse().json().send(result);
    }

    public void mttRewards(@Param("roomId")String roomId) {
        Result result = clubService.mttRewards(roomId);
        getResponse().json().send(result);
    }

    public void mttRankings(@Param("roomId")String roomId) {
        Result result = clubService.mttRankings(roomId);
        getResponse().json().send(result);
    }

    public void mttGameDetail(@Param("roomId")String roomId) {
        Result result = clubService.mttGameDetail(roomId);
        getResponse().json().send(result);
    }

    public void mttTableUsersInfo(@Param("roomId")String roomId) {
        Result result = clubService.mttTableUsersInfo(roomId);
        getResponse().json().send(result);
    }

    public void mttSignFee() {
        int[] fee = new int[]{0, 1, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500};
        getResponse().json().send(new Result(fee));
    }

    public void setRewardTemplate(@Param("id") String id,
                                  @Param("name") String name,
                                  @Param("rankingNames") String[] rankingNames,
                                  @Param("rankingValues") String[] rankingValues) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.setRewardTemplate(leaderId, id , name, rankingNames, rankingValues);
        getResponse().json().send(result);
    }

    public void rewardTemplates() {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.rewardTemplates(leaderId);
        getResponse().json().send(result);
    }


    public void setMttReward(@Param("roomId")String roomId,
                             @Param("rankingNames") String[] rankingNames,
                             @Param("rankingValues") String[] rankingValues) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.setMttReward(leaderId, roomId, rankingNames, rankingValues);
        getResponse().json().send(result);
    }

    public void addClubAgent(@Param("name")String name) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.addClubAgent(leaderId, name);
        getResponse().json().send(result);
    }

    public void getClubAgents() {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.getClubAgents(leaderId);
        getResponse().json().send(result);
    }

    public void delClubAgent(@Param("agentId")String agentId) {
        Result result = clubService.delClubAgent(agentId);
        getResponse().json().send(result);
    }

    public void getUserScoreLog(@Param("agentId")String agentId,
                                         @Param("lastCreateDate")long lastCreateDate,
                                         @Param("count")int count) {
        String leaderId = getRequest().getSession().get("$leaderId");
        Result result = clubService.getUserScoreLog(leaderId, agentId, lastCreateDate, count);
        getResponse().json().send(result);
    }
}
