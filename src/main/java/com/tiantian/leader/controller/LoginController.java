package com.tiantian.leader.controller;

import com.google.inject.Inject;
import com.tiantian.leader.application.Result;
import com.tiantian.leader.service.LoginService;
import ro.pippo.controller.Controller;
import ro.pippo.core.Param;

/**
 *
 */
public class LoginController extends Controller {
    @Inject
    private LoginService loginService;

    public void login(@Param("mobile")String mobile,
                      @Param("pwd")String pwd,
                      @Param("deviceToken") String deviceToken,
                      @Param("platform") String platform) {
       Result result = loginService.login(mobile, pwd, deviceToken, platform);
       getResponse().json().send(result);
    }

    public void loginWithToken(@Param("token")String token,
                               @Param("deviceToken") String deviceToken,
                               @Param("platform") String platform) {
        Result result = loginService.login(token, deviceToken, platform);
        getResponse().json().send(result);
    }

    public void exit() {
        String $token = getRequest().getHeader("token");
        Result result = loginService.exit($token);
        getResponse().json().send(result);
    }

    public void test(@Param("$leaderId") String leaderId, @Param("contacts")String contacts) {

    }
}
