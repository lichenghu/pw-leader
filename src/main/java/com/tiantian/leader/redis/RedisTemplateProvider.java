package com.tiantian.leader.redis;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPoolConfig;

import javax.inject.Provider;

/**
 *
 */
public class RedisTemplateProvider implements Provider<StringRedisTemplate> {

    private StringRedisTemplate redisTemplate;

    @Inject
    public RedisTemplateProvider(@Named("redis.name") String hostName,
                                 @Named("redis.port") int port) {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory(jedisPoolConfig);
        jedisConnectionFactory.setUsePool(true);
        jedisConnectionFactory.setHostName(hostName);
        jedisConnectionFactory.setPort(port);
        jedisConnectionFactory.afterPropertiesSet(); //需要手动调用一下

        redisTemplate = new StringRedisTemplate(jedisConnectionFactory);
    }

    @Override
    public StringRedisTemplate get() {
        return redisTemplate;
    }
}
