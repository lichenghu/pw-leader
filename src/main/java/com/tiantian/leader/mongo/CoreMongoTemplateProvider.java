package com.tiantian.leader.mongo;

import com.google.inject.Inject;
import com.mongodb.Mongo;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.inject.Provider;


/**
 *
 */
public class CoreMongoTemplateProvider implements Provider<MongoTemplate> {

    private MongoTemplate mongoTemplate;

    @Inject
    public CoreMongoTemplateProvider(Mongo mongo) {
        mongoTemplate = new MongoTemplate(mongo, "pw_core");
    }

    @Override
    public MongoTemplate get() {
        return mongoTemplate;
    }
}
