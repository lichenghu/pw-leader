package com.tiantian.leader.db;

import com.alibaba.druid.pool.DruidDataSource;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import javax.inject.Provider;
import javax.sql.DataSource;

/**
 *
 */
public class DataSourceProvider implements Provider<DataSource> {

    private DruidDataSource dataSource;
    @Inject
    public DataSourceProvider( @Named("db.name") String userName,
                               @Named("db.pwd") String pwd,
                               @Named("db.url") String url) {
        dataSource = new DruidDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(pwd);

        dataSource.setMaxActive(20);
        dataSource.setPoolPreparedStatements(true);
        dataSource.setMaxOpenPreparedStatements(50);

        dataSource.setTestWhileIdle(false);
    }

    @Override
    public DataSource get() {
        return dataSource;
    }
}
