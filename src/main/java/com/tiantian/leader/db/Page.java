package com.tiantian.leader.db;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Page<T> {
    /**每页显示几条*/
    private int pageSize = 10;

    /**总条数*/
    private int total = 0;

    /**当前页*/
    private int pageNo = 0;

    /**当前记录起始索引*/
    private int currentResult = 0;
    /**排序名称**/
    private String sortName = "";
    /**排序策略**/
    private String sortOrder = "asc";

    /**存放结果集*/
    private List<T> rows = new ArrayList<>();
    /**
     *
     * <p>获取结果集</p>
     *
     * @return
     */
    public List<T> getRows() {
        if (rows == null) {
            return new ArrayList<T>();
        }
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    /**
     *
     * <p>
     * 获取总页数
     * </p>
     *
     * @return
     */
    public int getTotalPage() {
        if (total % pageSize == 0) {
            return total / pageSize;
        }
        return total / pageSize + 1;
    }

    /**
     *
     * <p>
     * 获取总条数
     * </p>
     *
     * @return
     */
    public int getTotal() {
        return total;
    }

    /**
     *
     * <p>
     * 设置总条数
     * </p>
     *
     * @param total
     */
    public void setTotal(int total) {
        this.total = total;
    }

    public int getPageNo() {
        if (pageNo <= 0) {
            pageNo = 1;
        }
        /*if (pageNo > getTotalPage()) {
            pageNo = getTotalPage();
        }*/
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if (pageSize == 0) {
            pageSize = 10;
        }
        this.pageSize = pageSize;
    }

    public int getCurrentResult() {
        currentResult = (getPageNo() - 1) * getPageSize();
        if (currentResult < 0) {
            currentResult = 0;
        }
        return currentResult;
    }

    public void setCurrentResult(int currentResult) {
        this.currentResult = currentResult;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
}
