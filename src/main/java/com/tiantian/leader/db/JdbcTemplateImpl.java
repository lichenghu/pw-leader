package com.tiantian.leader.db;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import javax.sql.DataSource;

/**
 *
 */
public class JdbcTemplateImpl extends AbstractJdbcTemplate {

    @Inject
    public JdbcTemplateImpl(@Named("core") DataSource dataSource) {
        super(dataSource);
    }
}
