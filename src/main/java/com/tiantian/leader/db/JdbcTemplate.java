package com.tiantian.leader.db;

import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 */
public interface JdbcTemplate {
    <T> T query(Connection conn, String sql, ResultSetHandler<T> rsh, Object... params) throws SQLException;

    <T> T query(Connection conn, String sql, ResultSetHandler<T> rsh) throws SQLException;

    <T> T query(String sql, ResultSetHandler<T> rsh, Object... params) throws SQLException;

    <T> T queryBean(String sql, Class<T> clazz, Object... params) throws SQLException;

    <T> List<T> queryBeanList(String sql, Class<T> clazz, Object... params) throws SQLException;

    <T> T query(String sql, ResultSetHandler<T> rsh) throws SQLException;

    int update(Connection conn, String sql) throws SQLException;

    int update(Connection conn, String sql, Object param) throws SQLException;

    int update(Connection conn, String sql, Object... params) throws SQLException;

    int update(String sql) throws SQLException;

    int update(String sql, Object param) throws SQLException;

    int update(String sql, Object... params) throws SQLException;

    <T> T insert(String sql, ResultSetHandler<T> rsh) throws SQLException;

    <T> T insert(String sql, ResultSetHandler<T> rsh, Object... params) throws SQLException;

    <T> T insert(Connection conn, String sql, ResultSetHandler<T> rsh) throws SQLException;

    <T> T insert(Connection conn, String sql, ResultSetHandler<T> rsh, Object... params) throws SQLException;

    <T> T insertBatch(String sql, ResultSetHandler<T> rsh, Object[][] params) throws SQLException;

    <T> T insertBatch(Connection conn, String sql, ResultSetHandler<T> rsh, Object[][] params) throws SQLException;

    <T> T insertBean(String sql, Class<T> tClass, Object... params) throws SQLException;

    <T> Page<T> getPage(String sql, Page page, Class<T> tClass, Object... params) throws SQLException;
}
