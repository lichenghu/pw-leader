package com.tiantian.leader.db;

import org.apache.commons.dbutils.*;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 */
public abstract class AbstractJdbcTemplate implements JdbcTemplate {

    private QueryRunner queryRunner;

    private static final RowProcessor DEFAULT_PROCESSOR = new BasicRowProcessor(new GenerousBeanProcessor());

    public AbstractJdbcTemplate(DataSource dataSource)
    {
        this.queryRunner = new QueryRunner(dataSource);
    }

    public <T> T query(Connection conn, String sql, ResultSetHandler<T> rsh, Object... params) throws SQLException {
        return queryRunner.query(conn, sql, rsh, params);
    }

    public <T> T query(Connection conn, String sql, ResultSetHandler<T> rsh) throws SQLException {
        return queryRunner.query(conn, sql, rsh);
    }

    public <T> T query(String sql, ResultSetHandler<T> rsh, Object... params) throws SQLException {
        return queryRunner.query(sql, rsh, params);
    }

    public <T> T queryBean(String sql, Class<T> clazz, Object... params) throws SQLException {
        return query(sql, new BeanHandler<>(clazz, DEFAULT_PROCESSOR), params);
    }

    public <T> List<T> queryBeanList(String sql, Class<T> clazz, Object... params) throws SQLException{
        return query(sql, new BeanListHandler<>(clazz, DEFAULT_PROCESSOR), params);
    }

    public <T> T query(String sql, ResultSetHandler<T> rsh) throws SQLException {
        return queryRunner.query(sql, rsh);
    }

    public int update(Connection conn, String sql) throws SQLException {
        return  queryRunner.update(conn, sql);
    }

    public int update(Connection conn, String sql, Object param) throws SQLException {
        return queryRunner.update(conn, sql, param);
    }

    public int update(Connection conn, String sql, Object... params) throws SQLException {
        return queryRunner.update(conn, sql, params);
    }

    public int update(String sql) throws SQLException {
        return queryRunner.update(sql);
    }

    public int update(String sql, Object param) throws SQLException {
        return queryRunner.update(sql, param);
    }

    public int update(String sql, Object... params) throws SQLException {
        return queryRunner.update(sql, params);
    }

    public <T> T insert(String sql, ResultSetHandler<T> rsh) throws SQLException {
        return queryRunner.insert(sql, rsh);
    }

    public <T> T insert(String sql, ResultSetHandler<T> rsh, Object... params) throws SQLException {
        return queryRunner.insert(sql, rsh, params);
    }

    public <T> T insert(Connection conn, String sql, ResultSetHandler<T> rsh) throws SQLException {
        return queryRunner.insert(conn, sql, rsh);
    }

    public <T> T insert(Connection conn, String sql, ResultSetHandler<T> rsh, Object... params) throws SQLException {
        return queryRunner.insert(conn, sql, rsh, params);
    }

    public <T> T insertBatch(String sql, ResultSetHandler<T> rsh, Object[][] params) throws SQLException {
        return queryRunner.insertBatch(sql, rsh, params);
    }

    public <T> T insertBatch(Connection conn, String sql, ResultSetHandler<T> rsh, Object[][] params) throws SQLException {
        return queryRunner.insertBatch(conn, sql, rsh, params);
    }

    public <T> T insertBean(String sql, Class<T> tClass, Object... params) throws SQLException {
       return insert(sql,  new BeanHandler<>(tClass, DEFAULT_PROCESSOR), params);
    }

    public <T> Page<T> getPage(String sql, Page page, Class<T> tClass, Object... params) throws SQLException {
        if (page == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as cnt from (")
          .append(sql)
          .append(") tmp_count;");
        String totalSql = sb.toString();
        Long total = query(totalSql, new ResultSetHandler<Long>() {
            @Override
            public Long handle(ResultSet rs) throws SQLException {
                return rs != null && rs.next() ? rs.getLong("cnt") : 0;
            }
        }, params);
        page.setTotal(total.intValue());

        sb.setLength(0);
        sb.append("select * from ( ");
        sb.append(sql);
        if (StringUtils.isNotBlank(page.getSortName())) {
            sb.append(" order by ")
              .append(StringEscapeUtils.escapeSql(page.getSortName()))
              .append(StringEscapeUtils.escapeSql(page.getSortOrder()));
        }
        sb.append(" ) as page_temp limit ");
        sb.append(page.getPageSize());
        sb.append(" offset ");
        sb.append(page.getCurrentResult());
        sb.append(";");
        String selectSql = sb.toString();
        List<T> result = queryBeanList(selectSql, tClass, params);
        page.setRows(result);
        return page;
    }
}
