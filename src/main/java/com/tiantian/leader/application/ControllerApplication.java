package com.tiantian.leader.application;

import com.google.inject.Injector;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.controller.*;
import com.tiantian.leader.guice.GuiceManger;
import com.tiantian.leader.model.LoginSession;
import com.tiantian.leader.model.Schedule;
import com.tiantian.leader.schedule.ScheduleManager;
import ro.pippo.core.PippoRuntimeException;
import ro.pippo.guice.GuiceControllerFactory;


/**
 *
 */
public class ControllerApplication extends ro.pippo.controller.ControllerApplication {
    private final Injector injector;
    public ControllerApplication(Injector injector) {
        this.injector = injector;
        GuiceManger.INSTANCE.init(injector);
    }

    @Override
    protected void onInit() {
        addPublicResourceRoute();
        addWebjarsResourceRoute();
        getContentTypeEngines().setContentTypeEngine(new CustomJacksonEngine());
        setControllerFactory(new GuiceControllerFactory(injector));
        ALL("/v2.*", routeContext -> {
            String token = routeContext.getHeader("token");
            if (token == null) {
                routeContext.getResponse().json().send(new Result(300, "登陆已实效"));
                return;
            }
            LoginSession loginSession = LocalCache.get(token);
            if (loginSession == null) {
                routeContext.getResponse().json().send(new Result(300, "登陆已实效"));
                return;
            }
            if (!token.equalsIgnoreCase(loginSession.getToken())) {
                routeContext.getResponse().json().send(new Result(300, "已在其他地方登陆"));
                return;
            }
            //TODO 设置登陆后的leaderId
//            Map<String, ParameterValue> parameterValueMap = routeContext.getRequest().getPathParameters();
//            if(parameterValueMap == null) {
//               parameterValueMap = new HashMap<>();
//            }
//            Set<String> keys = parameterValueMap.keySet();
//            Map<String, String> pathMap = new HashMap<>();
//            if (keys.size() > 0) {
//                for (String key : keys) {
//                     pathMap.put(key, parameterValueMap.get(key).toString());
//                }
//            }
//            pathMap.put("$leaderId", loginSession.getLeaderId());
//            pathMap.put("token", token);
//            routeContext.getRequest().setPathParameters(pathMap);
            routeContext.getRequest().getSession().put("$leaderId", loginSession.getLeaderId());
            routeContext.next();
        });


        /** 手机号和密码登陆 **/
        POST("/v1/login/index", LoginController.class, "login");
        POST("/v1/login/test", LoginController.class, "test");
        /** token登陆 **/
        POST("/v1/login/login_token", LoginController.class, "loginWithToken");
        POST("/v2/login/exit", LoginController.class, "exit");
        /** 注册 **/
        POST("/v1/user/register", UserController.class, "register");
        /** 发送注册验证码 **/
        POST("/v1/user/send_register_code", UserController.class, "sendRegisterValidCode");
        /** 发送更新密码证码 **/
        POST("/v1/user/send_update_pwd_code", UserController.class, "sendUpdatePwdValidCode");
        /** 更新密码**/
        POST("/v1/user/update_pwd", UserController.class, "updatePwd");
        /**版本号**/
        POST("/v1/user/version", UserController.class, "version");

        /** 修改名称 **/
        POST("/v2/user/update_nick_name", UserController.class, "updateNickName");
        /** 修改邮箱 **/
        POST("/v2/user/update_email", UserController.class, "updateEmail");

        POST("/v2/user/update_avatar_url", UserController.class, "updateAvatarUrl");

        POST("/v2/user/system_time", UserController.class, "systemTime");
        POST("/v2/user/vip_info", UserController.class, "vipInfo");
        POST("/v2/user/props", UserController.class, "userProps");
        POST("/v2/user/give_props", UserController.class, "giveProps");

        /**获取创建局的可买入list**/
        POST("/v2/group/buy_in_list", GroupController.class, "getBuyInList");
        /**创建局**/
        POST("/v2/group/create", GroupController.class, "createGroup");
        /**获取所有进行中的局**/
        POST("/v2/group/list", GroupController.class, "getGroups");
        /**获取所有结束的局**/
        POST("/v2/group/end_list", GroupController.class, "getEndGroups");

        /**开始游戏**/
        POST("/v2/group/start", GroupController.class, "startGroupGame");
        POST("/v2/group/users", GroupController.class, "groupUsers");
        POST("/v2/group/applications", GroupController.class, "getApplications");
        POST("/v2/group/applications_all", GroupController.class, "getAllApplications");
        POST("/v2/group/agree_buy_in", GroupController.class, "agreeBuyIn");
        POST("/v2/group/odds", GroupController.class, "getAllOdds");
        POST("/v2/group/score_board", GroupController.class, "getScoreboard");
        POST("/v2/group/op_log_list", GroupController.class, "getOpLogList");
        POST("/v2/group/force_stand_up", GroupController.class, "forceStandUp");
        POST("/v2/group/force_close", GroupController.class, "forceCloseGame");
        POST("/v2/group/add_money_info", GroupController.class, "getAddMoneyInfo");
        POST("/v2/group/add_money", GroupController.class, "addUserMoney");
        POST("/v2/group/apply_ignore", GroupController.class, "ignoreUserApply");
        POST("/v2/group/app_counts", GroupController.class, "getAppCounts");
        POST("/v2/group/day_statistics", GroupController.class, "getDayWinStatic");
        POST("/v2/group/week_statistics", GroupController.class, "getWeekWinStatic");
        POST("/v2/group/month_statistics", GroupController.class, "getMonthWinStatic");
        POST("/v2/group/day_brief", GroupController.class, "getDayBrief");
        POST("/v2/group/exe_task", GroupController.class, "exeTask");

        POST("/v2/advice/add", AdviceController.class, "userAdvice");

        POST("/v1/pay/ios_list", PayController.class, "iosList");
        POST("/v1/pay/ios_post", PayController.class, "iosPostOrder");

        POST("/v2/club/add", ClubController.class, "saveUserClubs");
        POST("/v2/club/list", ClubController.class, "getUserClubs");
        POST("/v2/club/upload_logo", ClubController.class, "uploadClubLogo");
        POST("/v2/club/create_room", ClubController.class, "createClubSNGRoom");
        POST("/v2/club/sng_rooms", ClubController.class, "getSngRooms");
        POST("/v2/club/room_detail", ClubController.class, "roomDetail");
        POST("/v2/club/club_users", ClubController.class, "getClubUsers");
        POST("/v2/club/close_room", ClubController.class, "closeRoom");
        POST("/v2/club/reduce_user_score", ClubController.class, "reduceUserScore");
        POST("/v2/club/add_user_score", ClubController.class, "addUserScore");
        POST("/v2/club/add_award", ClubController.class, "createClubAward");
        POST("/v2/club/all_award", ClubController.class, "getClubAllAwards");
        POST("/v2/club/view_qr_award", ClubController.class, "viewQRAward");
        POST("/v2/club/valid_qr_code", ClubController.class, "validQRCode");
        POST("/v2/club/not_receive_awards", ClubController.class, "getClubUserNotReceiveAwards");
        POST("/v2/club/receive_awards", ClubController.class, "getClubUserReceiveAwards");
        POST("/v2/club/create_mtt_room", ClubController.class, "createClubMttRoom");
        POST("/v2/club/mtt_rooms", ClubController.class, "getMttRooms");
        POST("/v2/club/mtt_publish", ClubController.class, "publishMtt");
        POST("/v2/club/mtt_game_mark", ClubController.class, "gameMark");

        POST("/v2/club/mtt_game_rules", ClubController.class, "mttRules");
        POST("/v2/club/mtt_game_rewards", ClubController.class, "mttRewards");
        POST("/v2/club/mtt_game_rankings", ClubController.class, "mttRankings");
        POST("/v2/club/mtt_game_detail", ClubController.class, "mttGameDetail");
        POST("/v2/club/mtt_game_table_users", ClubController.class, "mttTableUsersInfo");
        POST("/v2/club/mtt_game_sign_fee", ClubController.class, "mttSignFee");
        POST("/v2/club/mtt_set_reward_template", ClubController.class, "setRewardTemplate");
        POST("/v2/club/mtt_reward_templates", ClubController.class, "rewardTemplates");
        POST("/v2/club/mtt_set_mtt_rewards", ClubController.class, "setMttReward");

        POST("/v2/club/add_club_agent", ClubController.class, "addClubAgent");
        POST("/v2/club/club_agents", ClubController.class, "getClubAgents");
        POST("/v2/club/del_club_agent", ClubController.class, "delClubAgent");
        POST("/v2/club/user_score_logs", ClubController.class, "getUserScoreLog");

        getErrorHandler().setExceptionHandler(Exception.class, (e, routeContext) -> {
            e.printStackTrace();
            if (e instanceof PippoRuntimeException) {
                if (e.getCause() instanceof IllegalArgumentException) {
                    // 参数异常
                    routeContext.getResponse().json().send(new Result(-100, e.getCause().getMessage()));
                    return;
                }
            }
            routeContext.getResponse().json().send(new Result(500, "服务错误"));
        });

        ScheduleManager.init();
    }
}
