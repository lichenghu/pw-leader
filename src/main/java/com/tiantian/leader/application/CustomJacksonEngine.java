package com.tiantian.leader.application;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import ro.pippo.fastjson.FastjsonEngine;

/**
 *
 */
public class CustomJacksonEngine extends FastjsonEngine {
    @Override
    public String toString(Object object) {
        return JSON.toJSONString(object, SerializerFeature.UseISO8601DateFormat,
                SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullListAsEmpty,
                SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteNullNumberAsZero,
                SerializerFeature.WriteNullBooleanAsFalse);
    }
}
