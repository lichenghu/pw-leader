package com.tiantian.leader.guice;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.lmax.disruptor.EventHandler;
import com.mongodb.Mongo;
import com.tiantian.leader.db.DataSourceProvider;
import com.tiantian.leader.db.JdbcTemplate;
import com.tiantian.leader.db.JdbcTemplateImpl;
import com.tiantian.leader.mongo.CoreMongoTemplateProvider;
import com.tiantian.leader.mongo.MongoProvider;
import com.tiantian.leader.obs.ObsService;
import com.tiantian.leader.redis.RedisTemplateProvider;
import com.tiantian.leader.service.*;
import com.tiantian.leader.service.impl.*;
import com.tiantian.leader.socketio.SocketServer;
import com.tiantian.leader.socketio.disruptor.MessageDisruptor;
import com.tiantian.leader.socketio.disruptor.MessageEventHandler;
import com.tiantian.leader.socketio.handler.UserHandler;
import com.tiantian.leader.socketio.manager.IOClientsManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Properties;

/**
 *
 */
public class GuiceModule extends AbstractModule {
    private Logger LOG = LoggerFactory.getLogger(GuiceModule.class);

    @Override
    protected void configure() {
        bind(DataSource.class).annotatedWith(Names.named("core")).toProvider(DataSourceProvider.class).asEagerSingleton();
        bind(JdbcTemplate.class).to(JdbcTemplateImpl.class).asEagerSingleton();
        bind(LoginService.class).to(LoginServiceImpl.class).asEagerSingleton();
        bind(UserService.class).to(UserServiceImpl.class).asEagerSingleton();
        bind(GroupService.class).to(GroupServiceImpl.class).asEagerSingleton();
        bind(LogService.class).to(LogServiceImpl.class).asEagerSingleton();
        bind(ObsService.class).asEagerSingleton();
        bind(AdviceService.class).to(AdviceServiceImpl.class).asEagerSingleton();
        bind(EventHandler.class).to(MessageEventHandler.class).asEagerSingleton();
        bind(MessageDisruptor.class).asEagerSingleton();
        bind(UserHandler.class).asEagerSingleton();
        bind(IOClientsManager.class).asEagerSingleton();

        bind(Mongo.class).toProvider(MongoProvider.class).asEagerSingleton();
        bind(MongoTemplate.class).annotatedWith(Names.named("mongoCore")).toProvider(CoreMongoTemplateProvider.class)
                .asEagerSingleton();
//        bind(MongoTemplate.class).annotatedWith(Names.named("mongoLogs")).toProvider(LogMongoTemplateProvider.class)
//                .asEagerSingleton();
        bind(StringRedisTemplate.class).toProvider(RedisTemplateProvider.class).asEagerSingleton();
        bind(PayService.class).to(PayServiceImpl.class).asEagerSingleton();
        bind(ClubService.class).to(ClubServiceImpl.class).asEagerSingleton();

        configureProperties("/conf/db.properties", "db");
        configureProperties("/conf/redis.properties", "redis");
        configureProperties("/conf/mongo.properties", "mongo");
        configureProperties("/conf/system.properties", "system");
    }

    private void configureProperties(String fileName, String namePre) {
        Properties p = new Properties();
        try {
            p.load(new InputStreamReader(this.getClass()
                   .getResourceAsStream(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
        }
        String env = System.getenv("NOMAD_ENV");
        if(null == env || "" == env){
            env = "development";
        }
        env += ".";
        Enumeration e = p.keys();
        while(e.hasMoreElements()) {
              String key = (String)e.nextElement();
              if (!key.startsWith(env)) {
                   continue;
              }
              String value = (String)p.get(key);
              key = key.replace(env, "");
              LOG.info("env is {}, {}, {} : {}",  env, fileName, key, value);
              bindConstant().annotatedWith(Names.named(
                    StringUtils.isNotBlank(namePre) ? namePre + "." + key : key)).to(value);
        }
    }
}
