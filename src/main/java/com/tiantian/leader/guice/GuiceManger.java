package com.tiantian.leader.guice;

import com.google.inject.Injector;
import com.google.inject.Key;

/**
 *
 */
public class GuiceManger {
    private Injector injector;

    public static GuiceManger INSTANCE = new GuiceManger();

    private GuiceManger() {
    }

    public void init(Injector injector) {
        this.injector = injector;
    }

    public <T> T get(Class<T> clazz) {
         return injector.getInstance(clazz);
    }

    public <T> T getByKey(Key<T> key) {
        return injector.getInstance(key);
    }
}
