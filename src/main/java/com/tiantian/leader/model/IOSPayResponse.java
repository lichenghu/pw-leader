package com.tiantian.leader.model;

/**
 * IOS收据校验结果
 */
public class IOSPayResponse {
    /**
     * 0表示有效
     * 21000 无法解析JSON
     * 21002，21003，21004，21005，21006，21007，21008
     */
   private String status;
   private String receipt;
   private String latest_receipt;
   private String latest_receipt_info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getLatest_receipt() {
        return latest_receipt;
    }

    public void setLatest_receipt(String latest_receipt) {
        this.latest_receipt = latest_receipt;
    }

    public String getLatest_receipt_info() {
        return latest_receipt_info;
    }

    public void setLatest_receipt_info(String latest_receipt_info) {
        this.latest_receipt_info = latest_receipt_info;
    }
}
