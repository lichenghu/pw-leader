package com.tiantian.leader.model;

/**
 *
 */
public class TemplateReward {
    private String rankingName;
    private String rankingValue;

    public String getRankingName() {
        return rankingName;
    }

    public void setRankingName(String rankingName) {
        this.rankingName = rankingName;
    }

    public String getRankingValue() {
        return rankingValue;
    }

    public void setRankingValue(String rankingValue) {
        this.rankingValue = rankingValue;
    }
}
