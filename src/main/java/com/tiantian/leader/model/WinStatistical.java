package com.tiantian.leader.model;
import org.springframework.data.annotation.Id;
/**
 *
 */
public class WinStatistical {
    @Id
    private String id;
    private String userId;
    private String groupId;
    private long win;
    private long safeWin;
    private long statistDate; // 统计日期
    private long createDate; //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public long getWin() {
        return win;
    }

    public void setWin(long win) {
        this.win = win;
    }

    public long getStatistDate() {
        return statistDate;
    }

    public void setStatistDate(long statistDate) {
        this.statistDate = statistDate;
    }

    public long getSafeWin() {
        return safeWin;
    }

    public void setSafeWin(long safeWin) {
        this.safeWin = safeWin;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }
}
