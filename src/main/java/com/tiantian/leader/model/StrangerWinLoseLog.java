package com.tiantian.leader.model;

import org.springframework.data.annotation.Id;

/**
 *
 */
public class StrangerWinLoseLog {
    @Id
    private String id;
    private String groupId;
    private String masterNickName;
    private String userId;
    private String nickName;
    private String mobile;
    private long win;
    private int playCnt;
    private long leftChips;
    private long chipsBuyTotal;
    private long moneyBuyTotal;
    private long moneyLeft;
    private int appMoneyStatus;
    private long appMoneyBuyIn;
    private long appMoneyBuyInDate;
    private long safeWin;
    private long safeCost;
    private long createDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMasterNickName() {
        return masterNickName;
    }

    public void setMasterNickName(String masterNickName) {
        this.masterNickName = masterNickName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public long getWin() {
        return win;
    }

    public void setWin(long win) {
        this.win = win;
    }

    public int getPlayCnt() {
        return playCnt;
    }

    public void setPlayCnt(int playCnt) {
        this.playCnt = playCnt;
    }

    public long getLeftChips() {
        return leftChips;
    }

    public void setLeftChips(long leftChips) {
        this.leftChips = leftChips;
    }

    public long getChipsBuyTotal() {
        return chipsBuyTotal;
    }

    public void setChipsBuyTotal(long chipsBuyTotal) {
        this.chipsBuyTotal = chipsBuyTotal;
    }

    public long getMoneyBuyTotal() {
        return moneyBuyTotal;
    }

    public void setMoneyBuyTotal(long moneyBuyTotal) {
        this.moneyBuyTotal = moneyBuyTotal;
    }

    public int getAppMoneyStatus() {
        return appMoneyStatus;
    }

    public void setAppMoneyStatus(int appMoneyStatus) {
        this.appMoneyStatus = appMoneyStatus;
    }

    public long getMoneyLeft() {
        return moneyLeft;
    }

    public void setMoneyLeft(long moneyLeft) {
        this.moneyLeft = moneyLeft;
    }

    public long getAppMoneyBuyIn() {
        return appMoneyBuyIn;
    }

    public void setAppMoneyBuyIn(long appMoneyBuyIn) {
        this.appMoneyBuyIn = appMoneyBuyIn;
    }

    public long getAppMoneyBuyInDate() {
        return appMoneyBuyInDate;
    }

    public void setAppMoneyBuyInDate(long appMoneyBuyInDate) {
        this.appMoneyBuyInDate = appMoneyBuyInDate;
    }

    public long getSafeWin() {
        return safeWin;
    }

    public void setSafeWin(long safeWin) {
        this.safeWin = safeWin;
    }

    public long getSafeCost() {
        return safeCost;
    }

    public void setSafeCost(long safeCost) {
        this.safeCost = safeCost;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }
}
