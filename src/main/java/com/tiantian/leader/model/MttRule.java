package com.tiantian.leader.model;

/**
 *
 */
public class MttRule implements Cloneable {
    private int level; // 盲注等级
    private long smallBlind;
    private long bigBlind;
    private long ante; //底注
    private int upgradeSecs; // 升盲时间
    private int storeSecs; // 下注储备时间
    private int reBuyIn; // 重新买入值
    private long costMoney; //花费

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(long smallBlind) {
        this.smallBlind = smallBlind;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(long bigBlind) {
        this.bigBlind = bigBlind;
    }

    public long getAnte() {
        return ante;
    }

    public void setAnte(long ante) {
        this.ante = ante;
    }

    public int getUpgradeSecs() {
        return upgradeSecs;
    }

    public void setUpgradeSecs(int upgradeSecs) {
        this.upgradeSecs = upgradeSecs;
    }

    public int getStoreSecs() {
        return storeSecs;
    }

    public void setStoreSecs(int storeSecs) {
        this.storeSecs = storeSecs;
    }

    public int getReBuyIn() {
        return reBuyIn;
    }

    public void setReBuyIn(int reBuyIn) {
        this.reBuyIn = reBuyIn;
    }

    public long getCostMoney() {
        return costMoney;
    }

    public void setCostMoney(long costMoney) {
        this.costMoney = costMoney;
    }

    public Object clone() {
        Object o = null;
        try {
            o = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return o;
    }
}
