package com.tiantian.leader.model;

/**
 *
 */
public class SngRule {
    private int level;
    private long smallBlind;
    private long bigBlind;
    private long upBlindSecs;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(long smallBlind) {
        this.smallBlind = smallBlind;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(long bigBlind) {
        this.bigBlind = bigBlind;
    }

    public long getUpBlindSecs() {
        return upBlindSecs;
    }

    public void setUpBlindSecs(long upBlindSecs) {
        this.upBlindSecs = upBlindSecs;
    }
}
