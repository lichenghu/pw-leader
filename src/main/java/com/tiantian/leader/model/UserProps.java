package com.tiantian.leader.model;

/**
 *
 */
public class UserProps {
    private String id;
    private String userId;
    private String propCode; // 道具信息
    private long propNums; // 道具数量

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPropCode() {
        return propCode;
    }

    public void setPropCode(String propCode) {
        this.propCode = propCode;
    }

    public long getPropNums() {
        return propNums;
    }

    public void setPropNums(long propNums) {
        this.propNums = propNums;
    }
}
