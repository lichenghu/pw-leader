package com.tiantian.leader.model;

import java.util.List;

/**
 *
 */
public class MttRoomInfo {
    private String roomId;
    private String roomName;
    private long fee;
    private int status;
    private long startTimes;
    private long createTimes;
    private int joinUsersCnts;
    private int leftUserCnts;
    private boolean publish;
    private List<MttReward> rewards;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public long getFee() {
        return fee;
    }

    public void setFee(long fee) {
        this.fee = fee;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getStartTimes() {
        return startTimes;
    }

    public void setStartTimes(long startTimes) {
        this.startTimes = startTimes;
    }

    public long getCreateTimes() {
        return createTimes;
    }

    public void setCreateTimes(long createTimes) {
        this.createTimes = createTimes;
    }

    public int getJoinUsersCnts() {
        return joinUsersCnts;
    }

    public void setJoinUsersCnts(int joinUsersCnts) {
        this.joinUsersCnts = joinUsersCnts;
    }

    public int getLeftUserCnts() {
        return leftUserCnts;
    }

    public void setLeftUserCnts(int leftUserCnts) {
        this.leftUserCnts = leftUserCnts;
    }

    public List<MttReward> getRewards() {
        return rewards;
    }

    public void setRewards(List<MttReward> rewards) {
        this.rewards = rewards;
    }

    public boolean isPublish() {
        return publish;
    }

    public void setPublish(boolean publish) {
        this.publish = publish;
    }
}
