package com.tiantian.leader.model;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 *
 */
public class SngRoom {
    @Id
    private String roomId;
    private String roomName;
    private String roomDesc;
    private String clubId;
    private int isEnd;
    private int userCnt;
    private long fee;
    private int maxTableNums;
    private long buyIn;
    private List<SngReward> sngRewards;
    private List<SngRule> rules;
    private long createDate;
    private int userJoin;
    private String type = "SNG";

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public String getClubId() {
        return clubId;
    }

    public void setClubId(String clubId) {
        this.clubId = clubId;
    }

    public int getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(int isEnd) {
        this.isEnd = isEnd;
    }

    public int getUserCnt() {
        return userCnt;
    }

    public void setUserCnt(int userCnt) {
        this.userCnt = userCnt;
    }

    public long getFee() {
        return fee;
    }

    public void setFee(long fee) {
        this.fee = fee;
    }

    public int getMaxTableNums() {
        return maxTableNums;
    }

    public void setMaxTableNums(int maxTableNums) {
        this.maxTableNums = maxTableNums;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public void setBuyIn(long buyIn) {
        this.buyIn = buyIn;
    }

    public List<SngReward> getSngRewards() {
        return sngRewards;
    }

    public void setSngRewards(List<SngReward> sngRewards) {
        this.sngRewards = sngRewards;
    }

    public List<SngRule> getRules() {
        return rules;
    }

    public void setRules(List<SngRule> rules) {
        this.rules = rules;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getUserJoin() {
        return userJoin;
    }

    public void setUserJoin(int userJoin) {
        this.userJoin = userJoin;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
