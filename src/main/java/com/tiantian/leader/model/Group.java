package com.tiantian.leader.model;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 *
 */
public class Group {
    @Id
    private String groupId;
    private String groupCode;
    private String masterId;
    private String masterName;
    private String masterAvatarUrl;
    private long buyIn;
    private double hours;
    private long fee;
    private long startDate;
    private long createDate;
    private int forceOver;
    private long bigBlind;
    private long smallBlind;
    private long totalPlayCnt;
    private boolean safe; // 开启保险 1是 0否
    private List<GroupMember> groupMembers;
    private String started; // "0" , "1"
    private String ended; // "0",否 "1"是

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getMasterAvatarUrl() {
        return masterAvatarUrl;
    }

    public void setMasterAvatarUrl(String masterAvatarUrl) {
        this.masterAvatarUrl = masterAvatarUrl;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public void setBuyIn(long buyIn) {
        this.buyIn = buyIn;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public long getFee() {
        return fee;
    }

    public void setFee(long fee) {
        this.fee = fee;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getForceOver() {
        return forceOver;
    }

    public void setForceOver(int forceOver) {
        this.forceOver = forceOver;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(long bigBlind) {
        this.bigBlind = bigBlind;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(long smallBlind) {
        this.smallBlind = smallBlind;
    }

    public long getTotalPlayCnt() {
        return totalPlayCnt;
    }

    public void setTotalPlayCnt(long totalPlayCnt) {
        this.totalPlayCnt = totalPlayCnt;
    }

    public boolean isSafe() {
        return safe;
    }

    public void setSafe(boolean safe) {
        this.safe = safe;
    }

    public List<GroupMember> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(List<GroupMember> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getStarted() {
        return started;
    }

    public void setStarted(String started) {
        this.started = started;
    }

    public String getEnded() {
        return ended;
    }

    public void setEnded(String ended) {
        this.ended = ended;
    }
}
