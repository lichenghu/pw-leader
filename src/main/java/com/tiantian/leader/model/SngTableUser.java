package com.tiantian.leader.model;

import org.springframework.data.annotation.Transient;

/**
 *
 */
public class SngTableUser {
    private String userId;
    private String avatarUrl;
    private String nickName;
    private int sitNums;
    private int ranking; //排名
    private String status;//normal exit
    @Transient
    private long userChips;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getSitNums() {
        return sitNums;
    }

    public void setSitNums(int sitNums) {
        this.sitNums = sitNums;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public long getUserChips() {
        return userChips;
    }

    public void setUserChips(long userChips) {
        this.userChips = userChips;
    }
}
