package com.tiantian.leader.model;

/**
 *
 */
public class ClubAward {
    private String awardId;
    private String awardName;
    private String clubId;
    private int status; // 1可用  0不可用
    private long createTime;
    private int receiveNum; // 已经领取数量
    private int notReceiveNum; // 未领取数量

    public String getAwardId() {
        return awardId;
    }

    public void setAwardId(String awardId) {
        this.awardId = awardId;
    }

    public String getAwardName() {
        return awardName;
    }

    public void setAwardName(String awardName) {
        this.awardName = awardName;
    }

    public String getClubId() {
        return clubId;
    }

    public void setClubId(String clubId) {
        this.clubId = clubId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getReceiveNum() {
        return receiveNum;
    }

    public void setReceiveNum(int receiveNum) {
        this.receiveNum = receiveNum;
    }

    public int getNotReceiveNum() {
        return notReceiveNum;
    }

    public void setNotReceiveNum(int notReceiveNum) {
        this.notReceiveNum = notReceiveNum;
    }
}
