package com.tiantian.leader.model;

/**
 *
 */
public class JsonResult {
    private int resultCode;

    private String failCode;

    private Object result;

    public JsonResult(){

    }
    public JsonResult(int resultCode, String failCode, Object result) {
        this.resultCode = resultCode;
        this.failCode = failCode;
        this.result = result;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getFailCode() {
        return failCode;
    }

    public void setFailCode(String failCode) {
        this.failCode = failCode;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
