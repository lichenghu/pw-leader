package com.tiantian.leader.model;

/**
 *
 */
public class GroupUserInfo {
    private String userId;
    private String nickName;
    private String avatarUrl;
    private long win;
    private long leftMoney;
    private String status;
    private long joinDate;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public long getWin() {
        return win;
    }

    public void setWin(long win) {
        this.win = win;
    }

    public long getLeftMoney() {
        return leftMoney;
    }

    public void setLeftMoney(long leftMoney) {
        this.leftMoney = leftMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(long joinDate) {
        this.joinDate = joinDate;
    }
}
