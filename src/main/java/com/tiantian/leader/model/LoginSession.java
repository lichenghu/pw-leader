package com.tiantian.leader.model;

import java.util.Date;

/**
 * 玩家登陆信息
 */
public class LoginSession {
    private String leaderId;
    private String nickName;
    private String avatarUrl;
    private String token;
    private String email;
    private Date createDate;
    private int vipLvl;
    private Date vipEndDate;
    private String deviceToke;
    private String platform;
    private boolean iosTest;

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getVipLvl() {
        return vipLvl;
    }

    public void setVipLvl(int vipLvl) {
        this.vipLvl = vipLvl;
    }

    public Date getVipEndDate() {
        return vipEndDate;
    }

    public void setVipEndDate(Date vipEndDate) {
        this.vipEndDate = vipEndDate;
    }

    public String getDeviceToke() {
        return deviceToke;
    }

    public void setDeviceToke(String deviceToke) {
        this.deviceToke = deviceToke;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public boolean isIosTest() {
        return iosTest;
    }

    public void setIosTest(boolean iosTest) {
        this.iosTest = iosTest;
    }
}
