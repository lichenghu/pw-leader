package com.tiantian.leader.model;

/**
 *
 */
public class ScoreBoard {
    private String nickName;
    private String avatarUrl;
    private long win;
    private long leftChips;
    private long totalBuyIn;
    private long safeWin;
    private long safeCost;
    private long moneyLeft;
    private long moneyBuyTotal;
    private long joinDate;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public long getWin() {
        return win;
    }

    public void setWin(long win) {
        this.win = win;
    }

    public long getLeftChips() {
        return leftChips;
    }

    public void setLeftChips(long leftChips) {
        this.leftChips = leftChips;
    }

    public long getSafeWin() {
        return safeWin;
    }

    public void setSafeWin(long safeWin) {
        this.safeWin = safeWin;
    }

    public long getSafeCost() {
        return safeCost;
    }

    public void setSafeCost(long safeCost) {
        this.safeCost = safeCost;
    }

    public long getMoneyBuyTotal() {
        return moneyBuyTotal;
    }

    public void setMoneyBuyTotal(long moneyBuyTotal) {
        this.moneyBuyTotal = moneyBuyTotal;
    }

    public long getMoneyLeft() {
        return moneyLeft;
    }

    public void setMoneyLeft(long moneyLeft) {
        this.moneyLeft = moneyLeft;
    }

    public long getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(long joinDate) {
        this.joinDate = joinDate;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public long getTotalBuyIn() {
        return totalBuyIn;
    }

    public void setTotalBuyIn(long totalBuyIn) {
        this.totalBuyIn = totalBuyIn;
    }
}
