package com.tiantian.leader.model;

import java.util.Date;
/**
 *
 */
public class Order {
    /**
     * UUID主键
     */

    private String id;
    /**
     * 订单号
     */
    private String orderId;
    /**
     * 商品CODE
     */
    private String productCode;

    /**
     * 购买商品数量（暂不提供具体数量，从private_data中获取）
     */
    private Integer productCount;
    /**
     * 支付金额，单位元 值根据不同渠道的要求可能为浮点类型
     */
    private Float amount;
    /**
     * 支付状态，1为成功， 0 失败（未校验）
     */
    private String payStatus;
    /**
     * 发放状态：NO 未发放, YES 已发放, FAIL发放失败
     */
    private String sendStatus;
    /**
     * 平台类型
     */
    private String platform;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后更新时间
     */
    private Date lastUpdateTime;

    /**
     *
     * 用户id，用户系统的用户id
     *
     */
    private String userId;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
