package com.tiantian.leader.model;

/**
 *
 */
public class MttRanking {
    private int ranking;
    private String nickName;
    private long leftChips;
    private String avatarUrl;

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public long getLeftChips() {
        return leftChips;
    }

    public void setLeftChips(long leftChips) {
        this.leftChips = leftChips;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
