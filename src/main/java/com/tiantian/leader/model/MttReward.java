package com.tiantian.leader.model;

/**
 *
 */
public class MttReward {
    private int ranking;
    private int virtualType; // 虚拟奖励类型 1是金币 2 积分
    private long virtualNums; // 虚拟奖励数据量
    private String physicalId; // 实物ID
    private String physicalName; //实物名称

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public int getVirtualType() {
        return virtualType;
    }

    public void setVirtualType(int virtualType) {
        this.virtualType = virtualType;
    }

    public long getVirtualNums() {
        return virtualNums;
    }

    public void setVirtualNums(long virtualNums) {
        this.virtualNums = virtualNums;
    }

    public String getPhysicalId() {
        return physicalId;
    }

    public void setPhysicalId(String physicalId) {
        this.physicalId = physicalId;
    }

    public String getPhysicalName() {
        return physicalName;
    }

    public void setPhysicalName(String physicalName) {
        this.physicalName = physicalName;
    }
}
