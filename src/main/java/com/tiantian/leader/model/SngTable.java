package com.tiantian.leader.model;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 *
 */
public class SngTable {
    @Id
    private String tableId;
    private String clubId;
    private String roomId;
    private int maxNumber; // 桌子最大人数
    private int isStarted; // 桌子是否开始 0 否 1是
    private int isEnd; // 桌子是否结束 0 否 1是
    private long createDate;
    private int tableIndex; //桌子号码 1 开始
    private List<SngTableUser> tableUsers;

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public int getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public List<SngTableUser> getTableUsers() {
        return tableUsers;
    }

    public void setTableUsers(List<SngTableUser> tableUsers) {
        this.tableUsers = tableUsers;
    }

    public int getIsStarted() {
        return isStarted;
    }

    public void setIsStarted(int isStarted) {
        this.isStarted = isStarted;
    }

    public int getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(int isEnd) {
        this.isEnd = isEnd;
    }

    public int getTableIndex() {
        return tableIndex;
    }

    public void setTableIndex(int tableIndex) {
        this.tableIndex = tableIndex;
    }

    public String getClubId() {
        return clubId;
    }

    public void setClubId(String clubId) {
        this.clubId = clubId;
    }
}
