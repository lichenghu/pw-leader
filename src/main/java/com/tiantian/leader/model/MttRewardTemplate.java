package com.tiantian.leader.model;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 *
 */
public class MttRewardTemplate {
    @Id
    private String id;
    private String clubId;
    private String name;
    private long createTime;
    private List<Reward> rewardList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClubId() {
        return clubId;
    }

    public void setClubId(String clubId) {
        this.clubId = clubId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public List<Reward> getRewardList() {
        return rewardList;
    }

    public void setRewardList(List<Reward> rewardList) {
        this.rewardList = rewardList;
    }

    public static class Reward {
        private String rankingName;
        private String rankingValue;

        public String getRankingName() {
            return rankingName;
        }

        public void setRankingName(String rankingName) {
            this.rankingName = rankingName;
        }

        public String getRankingValue() {
            return rankingValue;
        }

        public void setRankingValue(String rankingValue) {
            this.rankingValue = rankingValue;
        }
    }

}
