package com.tiantian.leader.model;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 *
 */
public class UserAdvice {
    @Id
    private String id;
    private String userId;
    private String content;
    private long createDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }
}
