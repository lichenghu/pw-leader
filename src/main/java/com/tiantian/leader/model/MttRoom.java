package com.tiantian.leader.model;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 *
 */
public class MttRoom {
    @Id
    private String roomId;
    private String clubId;
    private String roomName;
    private long taxFee; //抽水费
    private long poolFee; //入池费
    private int tableUsers; // 每桌多少人
    private int minUsers; // 报名最少人数
    private int maxUsers; // 报名最多人数
    private long startMills; //游戏开始时间戳 显示给玩家的开始时间
    private int curretBlindLvl; // 当前盲注级别, 游戏为开始之前为 -1 从 1开始
    private long updateBlindTimes; // 下次涨忙时间
    private int status; // 0 未开始, 1已开始, -1 已结束 -2 已解散
    private int perRestMins; // 每多少分钟休息
    private int restMins; // 休息时长
    private int startBuyIn; //起始买入额
    private int buyInCnt; // 可以买入次数
    private String rebuyDesc; // 买入描述信息
    private int canRebuyBlindLvl; // 买入最大的盲注级别 包括该级别
    private int signDelayMins;// 报名延时时间分钟
    private long createDate;
    private long startDate; // 游戏真正开始时间
    private long endDate;
    private String rewardMark; // 奖励描述
    private String type; // 免费赛 free, 实物赛 object 等
    private int available; //0否 1是
    private boolean isPublish; // 是否发布 false未  true已发布

    private List<MttReward> rewards;
    private List<MttRule> rules;
    private List<TemplateReward> templateRewards;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public long getTaxFee() {
        return taxFee;
    }

    public void setTaxFee(long taxFee) {
        this.taxFee = taxFee;
    }

    public long getPoolFee() {
        return poolFee;
    }

    public void setPoolFee(long poolFee) {
        this.poolFee = poolFee;
    }

    public int getTableUsers() {
        return tableUsers;
    }

    public void setTableUsers(int tableUsers) {
        this.tableUsers = tableUsers;
    }

    public int getMinUsers() {
        return minUsers;
    }

    public void setMinUsers(int minUsers) {
        this.minUsers = minUsers;
    }

    public int getMaxUsers() {
        return maxUsers;
    }

    public void setMaxUsers(int maxUsers) {
        this.maxUsers = maxUsers;
    }

    public long getStartMills() {
        return startMills;
    }

    public void setStartMills(long startMills) {
        this.startMills = startMills;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPerRestMins() {
        return perRestMins;
    }

    public void setPerRestMins(int perRestMins) {
        this.perRestMins = perRestMins;
    }

    public int getRestMins() {
        return restMins;
    }

    public void setRestMins(int restMins) {
        this.restMins = restMins;
    }

    public int getStartBuyIn() {
        return startBuyIn;
    }

    public void setStartBuyIn(int startBuyIn) {
        this.startBuyIn = startBuyIn;
    }

    public int getBuyInCnt() {
        return buyInCnt;
    }

    public void setBuyInCnt(int buyInCnt) {
        this.buyInCnt = buyInCnt;
    }

    public int getSignDelayMins() {
        return signDelayMins;
    }

    public void setSignDelayMins(int signDelayMins) {
        this.signDelayMins = signDelayMins;
    }

    public List<MttReward> getRewards() {
        return rewards;
    }

    public void setRewards(List<MttReward> rewards) {
        this.rewards = rewards;
    }

    public List<MttRule> getRules() {
        return rules;
    }

    public void setRules(List<MttRule> rules) {
        this.rules = rules;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getRewardMark() {
        return rewardMark;
    }

    public void setRewardMark(String rewardMark) {
        this.rewardMark = rewardMark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCurretBlindLvl() {
        return curretBlindLvl;
    }

    public void setCurretBlindLvl(int curretBlindLvl) {
        this.curretBlindLvl = curretBlindLvl;
    }

    public long getUpdateBlindTimes() {
        return updateBlindTimes;
    }

    public void setUpdateBlindTimes(long updateBlindTimes) {
        this.updateBlindTimes = updateBlindTimes;
    }

    public String getRebuyDesc() {
        return rebuyDesc;
    }

    public void setRebuyDesc(String rebuyDesc) {
        this.rebuyDesc = rebuyDesc;
    }

    public int getCanRebuyBlindLvl() {
        return canRebuyBlindLvl;
    }

    public void setCanRebuyBlindLvl(int canRebuyBlindLvl) {
        this.canRebuyBlindLvl = canRebuyBlindLvl;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public String getClubId() {
        return clubId;
    }

    public void setClubId(String clubId) {
        this.clubId = clubId;
    }

    public boolean isPublish() {
        return isPublish;
    }

    public void setIsPublish(boolean isPublish) {
        this.isPublish = isPublish;
    }

    public List<TemplateReward> getTemplateRewards() {
        return templateRewards;
    }

    public void setTemplateRewards(List<TemplateReward> templateRewards) {
        this.templateRewards = templateRewards;
    }
}
