package com.tiantian.leader.model;

import java.util.List;

/**
 *
 */
public class MttMark {
    public String gameId; // required
    public String name; // required
    public long fee; // required
    public long startTime; // required
    public int minUsers; // required
    public int maxUsers; // required
    public long beginBuyIn; // required
    public String rebuyDesc; // required
    public String status; // required
    public String feeType; // required
    public List<TemplateReward> templateRewardList;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getFee() {
        return fee;
    }

    public void setFee(long fee) {
        this.fee = fee;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getMinUsers() {
        return minUsers;
    }

    public void setMinUsers(int minUsers) {
        this.minUsers = minUsers;
    }

    public int getMaxUsers() {
        return maxUsers;
    }

    public void setMaxUsers(int maxUsers) {
        this.maxUsers = maxUsers;
    }

    public long getBeginBuyIn() {
        return beginBuyIn;
    }

    public void setBeginBuyIn(long beginBuyIn) {
        this.beginBuyIn = beginBuyIn;
    }

    public String getRebuyDesc() {
        return rebuyDesc;
    }

    public void setRebuyDesc(String rebuyDesc) {
        this.rebuyDesc = rebuyDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public List<TemplateReward> getTemplateRewardList() {
        return templateRewardList;
    }

    public void setTemplateRewardList(List<TemplateReward> templateRewardList) {
        this.templateRewardList = templateRewardList;
    }
}
