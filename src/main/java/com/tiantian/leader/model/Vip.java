package com.tiantian.leader.model;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;

import java.util.List;

/**
 *
 */
public class Vip {
    private int vipLvl;
    private String name;
    private String mark;
    private int limitTableNum;//每天限制开房数, -1 不限制
    private int maxTableNum; //同时最多房间数
    private int price; // 每月价格
    private long maxSmallBlind; // 房间最大的小盲注

    public int getVipLvl() {
        return vipLvl;
    }

    public void setVipLvl(int vipLvl) {
        this.vipLvl = vipLvl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public int getMaxTableNum() {
        return maxTableNum;
    }

    public void setMaxTableNum(int maxTableNum) {
        this.maxTableNum = maxTableNum;
    }

    public int getLimitTableNum() {
        return limitTableNum;
    }

    public void setLimitTableNum(int limitTableNum) {
        this.limitTableNum = limitTableNum;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getMaxSmallBlind() {
        return maxSmallBlind;
    }

    public void setMaxSmallBlind(long maxSmallBlind) {
        this.maxSmallBlind = maxSmallBlind;
    }
}
