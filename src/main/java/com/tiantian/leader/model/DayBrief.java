package com.tiantian.leader.model;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 *
 */
public class DayBrief {
    @Id
    private String id;
    private String userId;
    private List<Round> roundList;
    private long totalBuyChips;
    private long totalWin;
    private long totalUserCostSafe;
    private long totalUserSafeWin;
    private long totalLeaderSafeWin;
    private long statisDate;
    private long createDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Round> getRoundList() {
        return roundList;
    }

    public void setRoundList(List<Round> roundList) {
        this.roundList = roundList;
    }

    public long getTotalBuyChips() {
        return totalBuyChips;
    }

    public void setTotalBuyChips(long totalBuyChips) {
        this.totalBuyChips = totalBuyChips;
    }

    public long getTotalWin() {
        return totalWin;
    }

    public void setTotalWin(long totalWin) {
        this.totalWin = totalWin;
    }

    public long getTotalUserCostSafe() {
        return totalUserCostSafe;
    }

    public void setTotalUserCostSafe(long totalUserCostSafe) {
        this.totalUserCostSafe = totalUserCostSafe;
    }

    public long getTotalUserSafeWin() {
        return totalUserSafeWin;
    }

    public void setTotalUserSafeWin(long totalUserSafeWin) {
        this.totalUserSafeWin = totalUserSafeWin;
    }

    public long getTotalLeaderSafeWin() {
        return totalLeaderSafeWin;
    }

    public void setTotalLeaderSafeWin(long totalLeaderSafeWin) {
        this.totalLeaderSafeWin = totalLeaderSafeWin;
    }

    public long getStatisDate() {
        return statisDate;
    }

    public void setStatisDate(long statisDate) {
        this.statisDate = statisDate;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public static class Round {
        private long smallBlind;
        private long bigBlind;
        private double totalHours;
        private int playCnt;

        public long getSmallBlind() {
            return smallBlind;
        }

        public void setSmallBlind(long smallBlind) {
            this.smallBlind = smallBlind;
        }

        public long getBigBlind() {
            return bigBlind;
        }

        public void setBigBlind(long bigBlind) {
            this.bigBlind = bigBlind;
        }

        public double getTotalHours() {
            return totalHours;
        }

        public void setTotalHours(double totalHours) {
            this.totalHours = totalHours;
        }

        public int getPlayCnt() {
            return playCnt;
        }

        public void setPlayCnt(int playCnt) {
            this.playCnt = playCnt;
        }
    }
}


