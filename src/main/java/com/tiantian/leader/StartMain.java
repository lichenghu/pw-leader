package com.tiantian.leader;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.tiantian.leader.application.ControllerApplication;
import com.tiantian.leader.guice.GuiceModule;
import com.tiantian.leader.socketio.SocketServer;
import com.tiantian.leader.utils.PushUtils;
import ro.pippo.core.Pippo;

/**
 *
 */
public class StartMain {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new GuiceModule());
        new Thread(() -> {
            Pippo pippo = new Pippo(new ControllerApplication(injector));
            pippo.start();
        }).start();
        startSocketIo(args, injector);

    }

    private static void startSocketIo(String[] args, Injector injector) {
        SocketServer socketServer = injector.getInstance(SocketServer.class);
        socketServer.start(args);
    }
}
