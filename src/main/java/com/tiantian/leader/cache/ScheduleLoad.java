package com.tiantian.leader.cache;

import com.alibaba.fastjson.JSON;
import com.tiantian.leader.model.Schedule;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

/**
 *
 */
public class ScheduleLoad {

    public List<Schedule> get() {
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream("config/schedule.json");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        return JSON.parseArray(configString, Schedule.class);
    }
}
