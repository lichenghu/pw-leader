package com.tiantian.leader.cache;

import com.alibaba.fastjson.JSON;
import com.tiantian.leader.db.JdbcTemplate;
import com.tiantian.leader.guice.GuiceManger;
import com.tiantian.leader.model.LeaderAppVersion;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class CacheUtils {
    private static final String MAINTAIN_INFO_KEY = "maintain_info:";

    public static List<MaintainInfo> getAllMaintainInfo() {
        StringRedisTemplate stringRedisTemplate = GuiceManger.INSTANCE.get(StringRedisTemplate.class);
        String jsonStr = stringRedisTemplate.opsForValue().get(MAINTAIN_INFO_KEY);
        if (jsonStr == null) {
            return new ArrayList<>();
        }
        List<MaintainInfo> maintainInfos = JSON.parseArray(jsonStr, MaintainInfo.class);
        if (maintainInfos == null) {
            return new ArrayList<>();
        }
        return maintainInfos;
    }

    public static List<LeaderAppVersion> getAllAppVersions() {
        JdbcTemplate jdbcTemplate = GuiceManger.INSTANCE.get(JdbcTemplate.class);
        String sql = "select * from leader_app_version where available = 1 order by version desc";
        List<LeaderAppVersion> leaderAppVersions;
        try {
            leaderAppVersions = jdbcTemplate.queryBeanList(sql, LeaderAppVersion.class);
            return leaderAppVersions;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
