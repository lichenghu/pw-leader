package com.tiantian.leader.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.tiantian.leader.model.LeaderAppVersion;
import com.tiantian.leader.model.LoginSession;
import com.google.common.base.Optional;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class LocalCache {
    // 10天的有效时间   leaderId - LoginSession
    private static final Cache<String, Optional<LoginSession>> SESSION_CACHE = CacheBuilder
            .newBuilder()
            .expireAfterAccess(10, TimeUnit.DAYS)
            .build();

    // 10分钟的有效时间
    private static final Cache<String, Optional<MaintainInfo>> STRANGER_STATUS_CACHE = CacheBuilder
            .newBuilder()
            .expireAfterWrite(600, TimeUnit.SECONDS)
            .build();

    // 10分钟的有效时间
    private static final Cache<String, Optional<List<LeaderAppVersion>>> ALL_VERSION_CACHE = CacheBuilder
            .newBuilder()
            .expireAfterWrite(600, TimeUnit.SECONDS)
            .build();

    private static final String MAINTAIN = "maintain";
    private static final String APP_NAME = "stranger";
    private static final String APP_VERSION = "version";

    public static MaintainInfo getAppStatus() {
        try {
            Optional<MaintainInfo> optional = STRANGER_STATUS_CACHE.get(MAINTAIN, () -> {
                MaintainInfo maintainInfo = null;
                List<MaintainInfo> allMaintainInfos = CacheUtils.getAllMaintainInfo();
                if (allMaintainInfos != null && allMaintainInfos.size() > 0) {
                    for (MaintainInfo $maintainInfo : allMaintainInfos) {
                        if (APP_NAME.equalsIgnoreCase($maintainInfo.getAppName())) {
                            maintainInfo = $maintainInfo;
                            break;
                        }
                    }
                }
                return Optional.fromNullable(maintainInfo);
            });
            if (optional.isPresent()) {
                return optional.get();
            }
            else {
                STRANGER_STATUS_CACHE.invalidate(MAINTAIN);
                return null;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }


    // 10天的有效时间  token - userId
    private static final Cache<String, Optional<String>> USER_ID_CACHE = CacheBuilder
            .newBuilder()
            .expireAfterAccess(10, TimeUnit.DAYS)
            .build();


    public static void set(String token, LoginSession loginSession) {
        USER_ID_CACHE.put(token, Optional.of(loginSession.getLeaderId()));
        SESSION_CACHE.put(loginSession.getLeaderId(), Optional.of(loginSession));
    }

    public static void exit(String leaderId) {
        Optional<LoginSession> optional = null;
        try {
            optional = SESSION_CACHE.get(leaderId, () -> Optional.fromNullable(null));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (optional != null && optional.isPresent()) {
            LoginSession loginSession = optional.get();
            SESSION_CACHE.invalidate(loginSession.getLeaderId());
            USER_ID_CACHE.invalidate(loginSession.getToken());
        }
    }

    public static void expire(String token)  {
        Optional<String> optional = null;
        try {
            optional = USER_ID_CACHE.get(token, () -> Optional.fromNullable(null));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (optional != null && optional.isPresent()) {
            String userId = optional.get();
            SESSION_CACHE.invalidate(userId);
            USER_ID_CACHE.invalidate(token);
        }
    }

    public static LoginSession get(String token) {
        Optional<String> optional = null;
        try {
            optional = USER_ID_CACHE.get(token, () -> Optional.fromNullable(null));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (optional != null && optional.isPresent()) {
            String userId = optional.get();
            Optional<LoginSession> sessionOptional = null;
            try {
                sessionOptional = SESSION_CACHE.get(userId, () -> Optional.fromNullable(null));
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            if (sessionOptional != null && sessionOptional.isPresent()) {
                return sessionOptional.get();
            }
        }
        return null;
    }

    public static LoginSession getSessionById(String leaderId) {
        Optional<LoginSession> sessionOptional = null;
        try {
            sessionOptional = SESSION_CACHE.get(leaderId, () -> Optional.fromNullable(null));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (sessionOptional != null && sessionOptional.isPresent()) {
            return sessionOptional.get();
        }
        return null;
    }

    public static List<LeaderAppVersion> getAllVersions() {
        try {
            Optional<List<LeaderAppVersion>> optional = ALL_VERSION_CACHE.get(APP_VERSION, () -> {
                List<LeaderAppVersion> allVersions = CacheUtils.getAllAppVersions();
                return Optional.fromNullable(allVersions);
            });
            if (optional.isPresent()) {
                return optional.get();
            }
            else {
                ALL_VERSION_CACHE.invalidate(APP_VERSION);
                return null;
            }
        }
        catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

}
