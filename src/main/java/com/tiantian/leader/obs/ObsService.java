package com.tiantian.leader.obs;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.huawei.obs.services.ObsClient;
import com.huawei.obs.services.ObsConfiguration;
import com.huawei.obs.services.exception.ObsException;
import com.huawei.obs.services.model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 *
 */
public class ObsService {

    private ObsClient obsClient = null;
    private static final String endPoint = "obs.cn-north-1.myhwclouds.com";
    private String publicBucket;

    @Inject
    public ObsService(@Named("system.obs.ak") String ak, @Named("system.obs.sk") String sk,
                      @Named("system.obs.public_bucket") String publicBucket) {
        // 创建客户端实例
        ObsClient obsClient = null;
        final int httpPort = 80; // HTTP请求对应的端口
//        final String ak = "PRGHLIITXBMQZA9WAO7A"; // 存储服务器用户的接入证书
//        final String sk = "KEKTCOE7QDQ0KFLFJCNURNNNEMEU1XSOGLB4HDOH"; // 存储服务器用户的安全证书
        ObsConfiguration config = new ObsConfiguration();
        config.setSocketTimeout(20000);//设置socket超时时间
        config.setEndPoint(endPoint);
        config.setHttpsOnly(false);
        config.setEndpointHttpPort(httpPort);
        config.setEndpointHttpsPort(443);
        try {
            obsClient = new ObsClient(ak, sk, config);
        } catch (ObsException e) {
            e.printStackTrace();
        }
        this.obsClient = obsClient;
        this.publicBucket = publicBucket;
    }

    public String upload(File file) {
        String fileName = file.getName();
        String prefix = fileName.substring(fileName.lastIndexOf(".")+1);
        return putObject(file, prefix);
    }


    private String putObject(File file, String fileType)
    {
        try
        {
            Owner owner = new Owner();
            owner.setId("38d309de14674c09a097c7307b9f1ad2");
            owner.setDisplayName("pokerwinner");
            AccessControlList accessControlList = new AccessControlList();
            accessControlList.setOwner(owner);
            GroupGrantee groupGrantee = new GroupGrantee("http://acs.amazonaws.com/groups/global/AllUsers");
            accessControlList.grantPermission(groupGrantee, Permission.PERMISSION_READ);
//            String bucketName = "pw-public-test";
            String objectKey = "user_avatar/" + System.currentTimeMillis() + "/" +  ((int)(Math.random() * 9000) + 1000);
            objectKey += "." + fileType;

            FileInputStream fis = new FileInputStream(file);
            ObjectMetadata metadata = new ObjectMetadata();// 设置上传对象的元数据
            metadata.setContentLength(file.length());// 设置头信息中的文件长度

//            // 封装上传对象的请求
            PutObjectRequest request = new PutObjectRequest();
            request.setBucketName(publicBucket);
            request.setInput(fis);
            metadata.setContentLength(file.length());
            request.setMetadata(metadata);
            request.setObjectKey(objectKey);
            // 调用putObject接口创建对象
            PutObjectResult result = obsClient.putObject(request);
            obsClient.setObjectAcl(publicBucket, objectKey, accessControlList, null);
            System.out.println(result.getEtag());
            return "http://" + endPoint + "/" + publicBucket + "/" + objectKey;
        }
        catch (ObsException e)
        {
            System.out.println("Failed to reate object. Error message: " + e.getErrorMessage() + " ResponseCode: "
                    + e.getResponseCode());
        }
        catch (FileNotFoundException e)
        {
            System.out.println("FileNotFoundException: " + e.getMessage());
        }
        return null;
    }
}
