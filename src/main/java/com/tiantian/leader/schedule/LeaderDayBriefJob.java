package com.tiantian.leader.schedule;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tiantian.leader.guice.GuiceManger;
import com.tiantian.leader.model.DayBrief;
import com.tiantian.leader.model.Group;
import com.tiantian.leader.mongo.CoreMongoTemplateProvider;
import com.tiantian.leader.utils.DateUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import java.util.List;
import java.util.Map;

/**
 * 每日简报
 */
public class LeaderDayBriefJob implements Job {
    private final String GROUPS_TABLE = "stranger_groups";
    private static final String STRANGER_WIN_LOSE_LOG = "stranger_win_lose_log";
    private static final String LEADER_DAY_BRIEF = "leader_day_brief";
    private static long[] BIG_BLINDS = new long[]{2, 4, 10, 20, 50, 100, 200, 500, 1000, 10000};
    private static long[] SMALL_BLINDS = new long[]{1, 2, 5, 10, 25, 50, 100, 250, 500, 5000};

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        executeTask(DateUtils.todayBeginMills(), DateUtils.todayEndMills());
    }

    public void executeTask(long beginMills, long endMills) {
        CoreMongoTemplateProvider coreMongoTemplateProvider = GuiceManger.INSTANCE.get(CoreMongoTemplateProvider.class);
        MongoTemplate mongoTemplate = coreMongoTemplateProvider.get();
        Criteria criteria = Criteria.where("createDate").gt(beginMills - 86400000)
                .andOperator(Criteria.where("createDate").lt(endMills - 86400000));
        Query query = new Query();
        query.addCriteria(criteria);
        List<Group> groups = mongoTemplate.find(query, Group.class, GROUPS_TABLE);
        Map<String, List<Group>> userGroups = Maps.newHashMap();
        if (groups != null && groups.size() > 0) {
            for (Group group : groups) {
                String userId = group.getMasterId();
                List<Group> userGroupList = userGroups.get(userId);
                if (userGroupList == null) {
                    userGroupList = Lists.newArrayList();
                }
                userGroupList.add(group);
                userGroups.put(userId, userGroupList);
            }
        }
        Map<String, DayBrief> userDayBrief = Maps.newHashMap();
        for (Map.Entry<String, List<Group>> entry : userGroups.entrySet()) {
            DayBrief dayBrief = new DayBrief();
            String userId = entry.getKey();
            dayBrief.setUserId(userId);
            dayBrief.setCreateDate(System.currentTimeMillis());
            dayBrief.setStatisDate(beginMills - 86400000);

            Map<Long, DayBrief.Round> roundMap = Maps.newHashMap();
            List<Group> groupList = entry.getValue();
            Map<Long, Group> userGroupMap = Maps.newHashMap();
            for (Group group : groupList) {
                 userGroupMap.put(group.getSmallBlind(), group);
            }
            for (int i = 0 ; i < SMALL_BLINDS.length; i ++) {
                 long sb = SMALL_BLINDS[i];
                 Group group = userGroupMap.get(sb);
                 long big = BIG_BLINDS[i];
                 double hours = 0.0d;
                 int playCnt = 0;
                 if(group != null) {
                    hours = group.getHours();
                    playCnt = 1;
                 }
                DayBrief.Round round = roundMap.get(sb);
                if (round == null) {
                    round = new DayBrief.Round();
                    round.setSmallBlind(sb);
                    round.setBigBlind(big);
                }
                round.setPlayCnt(round.getPlayCnt() + playCnt);
                round.setTotalHours(round.getTotalHours() + hours);
                roundMap.put(sb, round);
            }
            dayBrief.setRoundList(Lists.newArrayList(roundMap.values()));
            userDayBrief.put(userId, dayBrief);
        }
        for (Map.Entry<String, DayBrief> entry : userDayBrief.entrySet()) {
             String userId = entry.getKey();
             DayBrief dayBrief = entry.getValue();
             List<String> groupIds = Lists.newArrayList();
             for (Group group : userGroups.get(userId)) {
                  groupIds.add(group.getGroupId());
             }
             //统计总量
             Criteria matcher = Criteria.where("createDate").gt(beginMills - 86400000)
                    .andOperator(Criteria.where("createDate").lt(endMills - 86400000))
                    .and("groupId").in(groupIds);
             Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(matcher),
                Aggregation.group()
                        .sum("chipsBuyTotal").as("chipsBuyTotal")
                        .sum("safeCost").as("safeCost")
                        .sum("safeWin").as("safeWin"));
             AggregationResults<DayBriefResult> result = mongoTemplate.aggregate(aggregation, STRANGER_WIN_LOSE_LOG,
                     DayBriefResult.class);
             if (result != null) {
                for (DayBriefResult dayBriefResult : result) {
                     dayBrief.setTotalBuyChips(dayBriefResult.chipsBuyTotal);
                     dayBrief.setTotalUserSafeWin(dayBriefResult.safeWin);
                     dayBrief.setTotalUserCostSafe(dayBriefResult.safeCost);
                }
             }
            Criteria matcher2 = Criteria.where("createDate").gt(beginMills - 86400000)
                    .andOperator(Criteria.where("createDate").lt(endMills - 86400000))
                    .and("win").gt(0).and("groupId").in(groupIds);
            Aggregation aggregation2 = Aggregation.newAggregation(
                    Aggregation.match(matcher2),
                    Aggregation.group()
                            .sum("win").as("win"));
            AggregationResults<DayBriefResult> result2 = mongoTemplate.aggregate(aggregation2, STRANGER_WIN_LOSE_LOG,
                    DayBriefResult.class);

            if (result2 != null) {
                for (DayBriefResult dayBriefResult : result2) {
                     dayBrief.setTotalWin(dayBriefResult.win);
                }
            }
        }
        mongoTemplate.insert(userDayBrief.values(), LEADER_DAY_BRIEF);
    }

    private static class DayBriefResult {
        long chipsBuyTotal;
        long safeCost;
        long safeWin;
        long win;
    }
}
