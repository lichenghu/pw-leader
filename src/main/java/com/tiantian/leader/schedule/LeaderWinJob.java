package com.tiantian.leader.schedule;

import com.google.common.collect.Maps;
import com.tiantian.leader.guice.GuiceManger;
import com.tiantian.leader.model.Group;
import com.tiantian.leader.model.WinStatistical;
import com.tiantian.leader.mongo.CoreMongoTemplateProvider;
import com.tiantian.leader.utils.DateUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class LeaderWinJob implements Job {
    private final String GROUPS_TABLE = "stranger_groups";
    private static final String STRANGER_WIN_LOSE_LOG = "stranger_win_lose_log";
    private static final String STRANGER_LEADER_WIN_STATISTICAL = "stranger_leader_win_statistical";
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        exeTask(DateUtils.todayBeginMills(), DateUtils.todayEndMills());
    }

    public void exeTask(long beginMills, long endMills) {
        CoreMongoTemplateProvider coreMongoTemplateProvider =  GuiceManger.INSTANCE.get(CoreMongoTemplateProvider.class);
        MongoTemplate mongoTemplate = coreMongoTemplateProvider.get();
        Criteria criteria = Criteria.where("createDate").gt(beginMills - 86400000)
                .andOperator(Criteria.where("createDate").lt(endMills - 86400000));

        Query query = new Query();
        query.addCriteria(criteria);
        List<Group> groups = mongoTemplate.find(query, Group.class, GROUPS_TABLE);
        if (groups == null || groups.size() == 0) {
            System.out.println("------groups is empty------");
            return;
        }
        System.out.println("------groups size------" + groups.size());
        Map<String, WinStatistical> winStatisticalMap = Maps.newHashMap();
        for (Group group : groups) {
            WinStatistical winStatistical = new WinStatistical();
            winStatistical.setGroupId(group.getGroupId());
            winStatistical.setUserId(group.getMasterId());
            winStatistical.setCreateDate(System.currentTimeMillis());
            winStatistical.setWin(0);
            winStatistical.setSafeWin(0);
            winStatistical.setStatistDate(beginMills - 86400000);
            winStatisticalMap.put(group.getGroupId(), winStatistical);
        }

        // 查询前一天
        Criteria matcher = Criteria.where("createDate").gt(beginMills - 86400000)
                .andOperator(Criteria.where("createDate").lt(endMills - 86400000));
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(matcher),
                Aggregation.group("groupId").sum("safeWin").as("safeWin"));
        AggregationResults<LeaderWinResult> result = mongoTemplate.aggregate(aggregation, STRANGER_WIN_LOSE_LOG, LeaderWinResult.class);

        if(result != null) {
            for (LeaderWinResult safeWinResult : result) {
                WinStatistical winStatistical = winStatisticalMap.get(safeWinResult.get_id());
                if (winStatistical != null) {
                    winStatistical.setSafeWin(safeWinResult.getSafeWin());
                }
            }
        }

//        // 查询前一天
        Criteria matcher2 = Criteria.where("createDate").gt(beginMills - 86400000)
                .andOperator(Criteria.where("createDate").lt(endMills - 86400000)).and("win").gt(0);

        Aggregation aggregation2 = Aggregation.newAggregation(
                Aggregation.match(matcher2),
                Aggregation.group("groupId").sum("win").as("win"));
        AggregationResults<LeaderWinResult> result2 = mongoTemplate.aggregate(aggregation2, STRANGER_WIN_LOSE_LOG, LeaderWinResult.class);
        if(result != null) {
            for (LeaderWinResult winResult : result2) {
                WinStatistical winStatistical = winStatisticalMap.get(winResult.get_id());
                if (winStatistical != null) {
                    winStatistical.setWin(winResult.getWin());
                }
            }
        }

        for (WinStatistical statistical : winStatisticalMap.values()) {
             mongoTemplate.insert(statistical, STRANGER_LEADER_WIN_STATISTICAL);
        }
    }




    private static class LeaderWinResult {
        private String _id;
        private long safeWin;
        private long win;
        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public long getSafeWin() {
            return safeWin;
        }

        public void setSafeWin(long safeWin) {
            this.safeWin = safeWin;
        }

        public long getWin() {
            return win;
        }

        public void setWin(long win) {
            this.win = win;
        }
    }
}
