package com.tiantian.leader.schedule;

import com.tiantian.leader.cache.ScheduleLoad;
import com.tiantian.leader.model.Schedule;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import java.util.List;
/**
 *
 */
public class ScheduleManager {
    private static SchedulerFactory schedulerFactory = new StdSchedulerFactory();

    public static void init()  {
        ScheduleLoad scheduleLoad = new ScheduleLoad();
        List<Schedule> schedules = scheduleLoad.get();
        if (schedules != null) {
            for (Schedule schedule : schedules) {
                String clazz = schedule.getClazz();
                Class tClass = null;
                try {
                    tClass = Class.forName(clazz);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    continue;
                }
                String coreEx = schedule.getCron();
                String jobName = schedule.getJobName();
                String jobGroupName = schedule.getJobGroupName();
                String triggerName = schedule.getTriggerName();
                String triggerGroupName = schedule.getTriggerGroupName();
                try {
                    addJob(jobName, jobGroupName, triggerName, triggerGroupName, tClass, coreEx);
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    private static  <T extends Job> void addJob(String jobName, String jobGroupName,
                      String triggerName, String triggerGroupName, Class<T> jobClazz,
                      String cronExpression) throws SchedulerException
    {
        Scheduler scheduler = schedulerFactory.getScheduler();
        JobDetail jobDetail = new JobDetail(jobName, jobGroupName, jobClazz);
        CronTrigger cronTrigger = new CronTrigger(triggerName, triggerGroupName);
        try {
            CronExpression cexp = new CronExpression(cronExpression);
            cronTrigger.setCronExpression(cexp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        scheduler.scheduleJob(jobDetail, cronTrigger);
    }

    private static void start() throws SchedulerException {
        Scheduler scheduler = schedulerFactory.getScheduler();
        scheduler.start();
    }
}
