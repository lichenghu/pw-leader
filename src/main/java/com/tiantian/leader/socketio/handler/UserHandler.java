package com.tiantian.leader.socketio.handler;

import com.alibaba.fastjson.JSON;
import com.google.inject.Inject;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.model.LoginSession;
import com.tiantian.leader.socketio.constants.Constants;
import com.tiantian.leader.socketio.constants.EventType;
import com.tiantian.leader.socketio.domain.PacketData;
import com.tiantian.leader.socketio.manager.IOClientsManager;
import com.tiantian.leader.socketio.settings.LeaderSocketConfig;
import com.tiantian.system.thrift.notifier.NoticeType;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserHandler {
    static Logger LOG = LoggerFactory.getLogger(UserHandler.class);
    @Inject
    private StringRedisTemplate stringRedisTemplate;
    @Inject
    IOClientsManager ioClientsManager;

    public void register(String token,final UUID sessionId) {
        String userId = null;
        try {
            LoginSession loginSession = LocalCache.get(token);
            if (loginSession != null) {
                userId = loginSession.getLeaderId();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        if (StringUtils.isBlank(userId)) {
            ioClientsManager.send(sessionId,
                    new PacketData(EventType.REGISTER_FAIL.type(), null , generate("403")));
        }
        else {
            UUID oldSessionID = ioClientsManager.getSessionID(userId);
            if (oldSessionID != null) {
                String oldToken = ioClientsManager.getToken(oldSessionID);
                if (!token.equalsIgnoreCase(oldToken)) {
                    // 推送在其他地方登陆
                    Map<String, String> map = new HashMap<>();
                    map.put("reason", "账号已在其它地方登陆");
                    ioClientsManager.send(oldSessionID,
                            new PacketData("kick", null, generate(JSON.toJSONString(map))));
                    // 立即退出
                    ioClientsManager.exit(oldSessionID);
                }
            }
            LOG.info("join ok ; userId is " + userId + "; sessionId:" + sessionId.toString());
            ioClientsManager.join(userId, sessionId, token);
            // 调用协议 在redis中注册
            registerRouter(Constants.ROUTER_KEY + userId,
                    LeaderSocketConfig.getInstance().getServerId());

            ioClientsManager.send(sessionId,
                    new PacketData(EventType.REGISTER_OK.type(), null , generate("ok")));
        }
    }


    public static String generate(String data) {
        String id = UUID.randomUUID().toString().replace("-", "");
        String type = NoticeType.GAME.toString();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("id: ").append(id).append("\n");
        stringBuffer.append("type: ").append(type).append("\n");
        try {
            String b64Str = new String(Base64.getEncoder().encode(data.getBytes("utf-8")));
            // b64里面有换行
            stringBuffer.append("data: ").append(b64Str.replace("\n", ""));
            stringBuffer.append("\n\n");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return stringBuffer.toString();
    }

    public void unregister(UUID sessionId) {
        String userId = ioClientsManager.getUserId(sessionId);
        LOG.info("user unregister sessionId :" + sessionId.toString());
        LOG.info("user unregister userId :" + userId);
        ioClientsManager.delayExit(sessionId);
    }

    public boolean registerRouter(String userIdKey, String router) {
        stringRedisTemplate.boundValueOps(userIdKey).set(router);
        stringRedisTemplate.boundValueOps("leader_server_load:" + router).increment(1);
        return true;
    }
}
