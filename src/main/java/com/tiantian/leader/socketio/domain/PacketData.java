package com.tiantian.leader.socketio.domain;

import sun.misc.BASE64Decoder;

import java.io.IOException;

/**
 *
 */
public class PacketData {
    private String event;
    private String to;
    private String dataStr;

    public PacketData() {
    }

    public PacketData(String event, String to, String dataStr) {
        this.event = event;
        this.to = to;
        this.dataStr = dataStr;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDataStr() {
        return dataStr;
    }

    public void setDataStr(String dataStr) {
        this.dataStr = dataStr;
    }

    @Override
    public String toString() {
        String[] strs = dataStr.split("\n");
        String str = "";
        int i = 0;
        for (String s :strs) {
            i ++;
            if (i == strs.length) {
                BASE64Decoder decoder = new BASE64Decoder();
                try {
                    s = s.replace("data: ", "");
                    str += "\n" + new String(decoder.decodeBuffer(s));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                str += s + "   ";
            }
        }
        return event + "\n" + to + "\n" +  str;
    }
}
