package com.tiantian.leader.socketio.listener;

import com.corundumstudio.socketio.AuthorizationListener;
import com.corundumstudio.socketio.HandshakeData;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.model.LoginSession;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * token 验证
 */
public class TokenAuthorizationListener implements AuthorizationListener {
    static Logger LOG = LoggerFactory.getLogger(TokenAuthorizationListener.class);
    @Override
    public boolean isAuthorized(HandshakeData data) {
        HandshakeData handshakeData = data;
        Map<String, List<String>> params = handshakeData.getUrlParams();
        List<String> tokens = params.get("auth_token");
        // 注册
        if (tokens != null && tokens.size() > 0) {
            String userId = null;
            try {
                 LoginSession loginSession = LocalCache.get(tokens.get(0));
                if (loginSession == null) {
                    LOG.info("token is err, token :" + tokens.get(0));
                    return false;
                }
                userId = loginSession.getLeaderId();
                if (StringUtils.isBlank(userId)) {
                    LOG.info("token is err, token :" + tokens.get(0));
                    return false;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }
}
