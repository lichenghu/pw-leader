package com.tiantian.leader.socketio;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.*;
import com.google.inject.Inject;
import com.tiantian.leader.socketio.constants.EventType;
import com.tiantian.leader.socketio.dispatcher.RabbitDispatcher;
import com.tiantian.leader.socketio.disruptor.Message;
import com.tiantian.leader.socketio.disruptor.MessageDisruptor;
import com.tiantian.leader.socketio.domain.GameObject;
import com.tiantian.leader.socketio.listener.TokenAuthorizationListener;
import com.tiantian.leader.socketio.manager.IOClientsManager;
import com.tiantian.leader.socketio.settings.LeaderSocketConfig;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class SocketServer {
    static Logger LOG = LoggerFactory.getLogger(SocketServer.class);
    @Inject
    private StringRedisTemplate stringRedisTemplate;
    @Inject
    private MessageDisruptor messageDisruptor;
    @Inject
    private IOClientsManager ioClientsManager;

    public void start(String[] args) {
        if (args != null) {
            if (args.length >= 1) {
                String serverId = args[0];
                if (StringUtils.isNotBlank(serverId)) {
                    LeaderSocketConfig.getInstance().setServerId(serverId);
                }
            }
            if (args.length >= 2) {
                String port = args[1];
                if (StringUtils.isNotBlank(port)) {
                    LeaderSocketConfig.getInstance().setPort(Integer.parseInt(port));
                }
            }
        }
        Configuration config = new Configuration();
        //config.setHostname(com.tiantian.socket.settings.LeaderSocketConfig.getInstance().getBindIp());
        config.setPort(LeaderSocketConfig.getInstance().getPort());
        config.setPingInterval(10000);
        config.setPingTimeout(30000);
        SocketConfig socketConfig = new SocketConfig();
        socketConfig.setTcpNoDelay(true);
        socketConfig.setTcpKeepAlive(false);
        socketConfig.setAcceptBackLog(2048);
        socketConfig.setSoLinger(0);
        // 当接收方通过Socket 的close() 方法关闭Socket 时, 如果网络上还有发送到这个Socket 的数据, 那么底层的Socket 不会立即释放本地端口, 而是会等待一段时间, 确保接收到了网络上发送过来的延迟数据, 然后再释放端口.  Socket接收到延迟数据后, 不会对这些数据作任何处理. Socket 接收延迟数据的目的是, 确保这些数据不会被其他碰巧绑定到同样端口的新进程接收到.
        socketConfig.setReuseAddress(true);
        config.setSocketConfig(socketConfig);
        AuthorizationListener listener = new TokenAuthorizationListener();
        config.setAuthorizationListener(listener);

        final SocketIOServer server = new SocketIOServer(config);


        // 游戏事件
        server.addEventListener(EventType.GAME_EVENT.type(), String.class, (client, data, ackRequest) -> {
            GameObject gameObject = null;
            try {
                gameObject = JSON.parseObject(data, GameObject.class);
                UUID sessionId = client.getSessionId();
                gameObject.setSessionId(sessionId);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if (gameObject != null) {
                messageDisruptor.in(new Message(EventType.GAME_EVENT, gameObject));
            }
        });

        // 断开连接
        server.addDisconnectListener(client -> {
            LOG.info(String.format("client disconnect, sessionID is %s", client.getSessionId().toString()));
            messageDisruptor.in(new Message(EventType.S_USER_EXIT, client.getSessionId()));
        });

        // 建立了连接
        server.addConnectListener(client -> {
            LOG.info("client connect");
            HandshakeData handshakeData = client.getHandshakeData();
            Map<String, List<String>> params = handshakeData.getUrlParams();
            List<String> tokens = params.get("auth_token");

            if (tokens != null && tokens.size() > 0) {
                LOG.info("token is :" + tokens.get(0));
                LOG.info("sessionId is :" + client.getSessionId());
                // 注册
                Map<String, Object> map = new HashMap<>();
                map.put("token", tokens.get(0));
                map.put("sessionId", client.getSessionId());
                messageDisruptor.in(new Message(EventType.C_REGISTER, map));
            }
        });

        // 初始化client管理器
        ioClientsManager.init(server);
        messageDisruptor.start();
        // 启动rabbit 监听
        RabbitDispatcher dispatcher = new RabbitDispatcher();
        dispatcher.init();
        dispatcher.start();
        server.start();

        // 清除负载
        clearRouterLoad();

    }

    public boolean unregisterRouter(String userIdKey) {
        String router = stringRedisTemplate.opsForValue().get(userIdKey);
        if (StringUtils.isNotBlank(router)) {
            stringRedisTemplate.delete(userIdKey);
            stringRedisTemplate.boundValueOps("leader_server_load:" + router).increment(-1);
        }
        return true;
    }

    public void clearRouterLoad() {
        stringRedisTemplate.delete("leader_server_load:" + LeaderSocketConfig.getInstance().getServerId());
    }

}
