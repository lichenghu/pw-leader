package com.tiantian.leader.socketio.disruptor;

import com.google.inject.Inject;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.tiantian.leader.socketio.settings.LeaderSocketConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 */
public class MessageDisruptor {
    private Logger LOG = LoggerFactory.getLogger(MessageDisruptor.class);
    private static Disruptor<MessageEvent> disruptor;
    private EventFactory<MessageEvent> eventFactory = new MessageEventFactory();
    // EventHandler多少个最多就在线程池中开启多少个线程
    private ExecutorService executor = Executors.newFixedThreadPool(300);
    private volatile boolean isStarted = false;
    @Inject
    private EventHandler eventHandler;

    public MessageDisruptor() {

    }

    public void start() {
        if (isStarted) {
            return;
        }
        LOG.info("disruptor started");
        int ringBufferSize = Math.max(LeaderSocketConfig.getInstance().getRingBufferSize(), 1024 * 32) ; // RingBuffer 大小，必须是 2 的 N 次方
        // ProducerType 根据系事件来源判断:多个来源选择MULTI， 单个来源选择SINGLE (单个来源比多个快)
        disruptor = new Disruptor<>(eventFactory,
                ringBufferSize, executor, ProducerType.MULTI,
                new BlockingWaitStrategy());
        disruptor.handleEventsWith(eventHandler);

        isStarted = true;

        // 启动
        disruptor.start();
    }

    public void stop() {
        if (!isStarted) {
            return;
        }
        isStarted = false;
        disruptor.shutdown();
    }

    public boolean in(Message message) {
        if (!isStarted) {
            return false;
        }
        // 发布事件；
        RingBuffer<MessageEvent> ringBuffer = disruptor.getRingBuffer();
        long sequence = ringBuffer.next();//请求下一个事件序号；
        try {
            MessageEvent event = ringBuffer.get(sequence);//获取该序号对应的事件对象；
            event.setMessage(message);
        } finally {
            ringBuffer.publish(sequence);//发布事件；
        }
        return true;
    }
}
