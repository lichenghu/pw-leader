package com.tiantian.leader.socketio.disruptor;


import com.google.inject.Inject;
import com.lmax.disruptor.EventHandler;
import com.tiantian.leader.socketio.handler.UserHandler;

import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class MessageEventHandler implements EventHandler<MessageEvent> {
    @Inject
    private UserHandler userHandler;

    public MessageEventHandler() {
    }

    @Override
    public void onEvent(MessageEvent messageEvent, long l, boolean b) throws Exception {
         try {
             Message message = messageEvent.getMessage();
             if (message != null) {
                 switch (message.getEventType()) {
                     case C_REGISTER:
                         Map<String, Object> map = (Map<String, Object>) message.getData();
                         userHandler.register((String) map.get("token"),
                                 (UUID) map.get("sessionId"));
                         break;
                     case REGISTER_FAIL:
                         break;
                     case CHAT_EVENT:
                         break;
                     case NOTICE_EVENT:
                         break;
                     case S_USER_EXIT:
                         userHandler.unregister((UUID) message.getData());
                         break;
                     default:
                         return;
                 }
             }
         } catch (Throwable e){
             e.printStackTrace();
         }
    }
}
