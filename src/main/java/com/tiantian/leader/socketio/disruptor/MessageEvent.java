package com.tiantian.leader.socketio.disruptor;

import java.io.Serializable;

/**
 *
 */
public class MessageEvent implements Serializable {
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
