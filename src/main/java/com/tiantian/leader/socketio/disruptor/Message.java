package com.tiantian.leader.socketio.disruptor;

import com.tiantian.leader.socketio.constants.EventType;

import java.io.Serializable;

/**
 *
 */
public class Message implements Serializable {

    private EventType eventType;

    private Object data;

    public Message() {
    }

    public Message(EventType eventType,  Object data) {
        this.eventType = eventType;
        this.data = data;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}