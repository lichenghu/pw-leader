package com.tiantian.leader.socketio.settings;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.parser.Parseable;
import us.bpsm.edn.parser.Parser;
import us.bpsm.edn.parser.Parsers;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

import static us.bpsm.edn.Keyword.newKeyword;
import static us.bpsm.edn.parser.Parsers.defaultConfiguration;

/**
 *
 */
public class LeaderSocketConfig {
    private Logger LOG= LoggerFactory.getLogger(LeaderSocketConfig.class);
    private String configFile = "config/socket.edn";
    private Map<?, ?> appConf;
    private String bindIp;
    private int port;
    private String env;
    private String serverId;
    private int ringBufferSize;

    public String getBindIp() {
        return bindIp;
    }

    public void setBindIp(String bindIp) {
        this.bindIp = bindIp;
    }

    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
        LOG.info("set port is {}", this.port);
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
        LOG.info("set serverId is {}", this.serverId);
    }

    public int getRingBufferSize() {
        return ringBufferSize;
    }

    public void setRingBufferSize(int ringBufferSize) {
        this.ringBufferSize = ringBufferSize;
    }

    private LeaderSocketConfig(){
        env = System.getenv("NOMAD_ENV");
        if(null == env || "" == env){
            env = "development";
        }
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream(configFile);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        LOG.info("==> config {}",configString);
        LOG.info("env is {}",env);
        Parseable pbr = Parsers.newParseable(configString);
        Parser p = Parsers.newParser(defaultConfiguration());
        appConf = (Map<?, ?>) p.nextValue(pbr);
        Map<?, ?> envs = (Map<?, ?>)appConf.get(newKeyword("nomad","environments"));
        Map<?, ?> appEnv = (Map<?, ?>)envs.get(env);
        this.bindIp = String.valueOf(appEnv.get(newKeyword("bind_ip")));
        this.port = Integer.parseInt(String.valueOf(appEnv.get(newKeyword("port"))));
        LOG.info("default port is {}", this.port);
        this.serverId = String.valueOf(appEnv.get(newKeyword("serverId")));
        LOG.info("default serverId is {}", this.serverId);
        this.ringBufferSize = Integer.parseInt(String.valueOf(appEnv.get(newKeyword("ring_buffer_size"))));
    }
    private static class SocketConfigHolder{
        private final static LeaderSocketConfig instance = new LeaderSocketConfig();
    }
    public static LeaderSocketConfig getInstance(){
        return SocketConfigHolder.instance;
    }
}
