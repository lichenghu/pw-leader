package com.tiantian.leader.socketio.settings;

/**
 * Created by jeffma on 15/6/10.
 */

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.parser.Parseable;
import us.bpsm.edn.parser.Parser;
import us.bpsm.edn.parser.Parsers;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

import static us.bpsm.edn.Keyword.newKeyword;
import static us.bpsm.edn.parser.Parsers.defaultConfiguration;

public class RabbitmqConfig {
    private Logger LOG= LoggerFactory.getLogger(RabbitmqConfig.class);
    private String configFile = "config/rabbitmq.edn";
    private String host;
    private String username;
    private String password;
    private int port;
    private String vhost;
    private String env;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getVhost() {
        return vhost;
    }

    public void setVhost(String vhost) {
        this.vhost = vhost;
    }
    private RabbitmqConfig(){
        env = System.getenv("NOMAD_ENV");
        if(null == env || "" == env){
            env = "development";
        }
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream(configFile);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        LOG.info("==>rabbitmq config {}",configString);
        Parseable pbr = Parsers.newParseable(configString);
        Parser p = Parsers.newParser(defaultConfiguration());
        Map<?, ?> rc = (Map<?, ?>) p.nextValue(pbr);
        Map<?, ?> rabbitEnv = (Map<?, ?>)rc.get(env);
        Map<?, ?> rabbitSpec = (Map<?, ?>)rabbitEnv.get(newKeyword("rmq-spec"));
        this.host = (String) rabbitSpec.get(newKeyword("host"));
        this.username = (String) rabbitSpec.get(newKeyword("username"));
        this.password = (String) rabbitSpec.get(newKeyword("password"));
        this.port = Integer.parseInt(String.valueOf(rabbitSpec.get(newKeyword("port"))));
        this.vhost = (String) rabbitSpec.get(newKeyword("vhost"));
        LOG.info("--->{}:{}:{}:{}:{}",this.username,this.password,this.host,this.port,this.vhost);
    }
    private static class RabbitmqConfigHolder{
        private final static RabbitmqConfig instance = new RabbitmqConfig();
    }
    public static RabbitmqConfig getInstance(){
        return RabbitmqConfigHolder.instance;
    }
}

