package com.tiantian.leader.socketio.constants;

/**
 *
 */
public enum EventType {
    C_REGISTER("register"),

    REGISTER_FAIL ("register_fail"),

    REGISTER_OK ("register_ok"),

    CHAT_EVENT ("chat_event"),

    GAME_EVENT ("game_event"),

    FRIEND_GAME_EVENT ("friend_game_event"),

    SPINGO_GAME_EVENT("spingo_game_event"),

    STRANGER_GAME_EVENT("stranger_game_event"),

    EVENT_FAIL ("event_fail"),

    NOTICE_EVENT ("noticeEvent"),

    S_USER_EXIT("user_exit");

    private final String type;

    private EventType(String type) {
        this.type = type;
    }

    public String type() {
        return this.type;
    }
}
