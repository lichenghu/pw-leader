package com.tiantian.leader.socketio.manager;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.google.inject.Inject;
import com.tiantian.leader.socketio.constants.Constants;
import com.tiantian.leader.socketio.domain.PacketData;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class IOClientsManager {
    static Logger LOG = LoggerFactory.getLogger(IOClientsManager.class);
    private static final Map<String, UUID> userIdSessionMap = new ConcurrentHashMap<>();
    private static final Map<String, String> sessionUserIdMap = new ConcurrentHashMap<>();
    private static final Map<String, String> sessionTokenMap = new ConcurrentHashMap<>();
    // 需要检验移除的session
    private static final Map<UUID, Long> checkSessionRemoveMap = new ConcurrentHashMap<>();

    private SocketIOServer server;

    @Inject
    private StringRedisTemplate stringRedisTemplate;

    public void init(SocketIOServer server) {
        this.server = server;
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(70);
                    try {
                        checkSessionRemove();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        });
        // 守护线程
        thread.setDaemon(true);
        thread.start();
    }

    public void join(String userId, UUID sessionId, String token) {
        if (userId == null || sessionId == null || token == null) {
            return;
        }
        userIdSessionMap.put(userId, sessionId);
        sessionUserIdMap.put(sessionId.toString(), userId);
        sessionTokenMap.put(sessionId.toString(), token);
    }

    public void exit(String userId) {
       UUID sessionId = userIdSessionMap.remove(userId);
       if (sessionId != null) {
           sessionUserIdMap.remove(sessionId.toString());
           sessionTokenMap.remove(sessionId.toString());
       }
    }

    public boolean isOnline(String userId) {
        return userIdSessionMap.containsKey(userId);
    }

    public UUID getSessionID(String userId) {
        return userIdSessionMap.get(userId);
    }

    public String getToken(UUID sessionId) {
        return sessionTokenMap.remove(sessionId.toString());
    }

    public void exit(UUID sessionId) {
       if (sessionId == null) {
           return;
       }
       String userId = sessionUserIdMap.remove(sessionId.toString());
       if (userId != null) {
           LOG.info("user exit userId :" + userId + "; sessionId : "  + sessionId.toString());
           UUID session = userIdSessionMap.get(userId);
           if (session != null && session.toString().equals(sessionId.toString())) {
               userIdSessionMap.remove(userId);
               try {
                   // 清除数据
                   unregisterRouter(Constants.ROUTER_KEY + userId);
               } catch (Exception e) {
                   e.printStackTrace();
               }
           }
           sessionTokenMap.remove(sessionId.toString());
       }
    }

    public void send(PacketData packetData) {
        if (packetData == null) {
            return;
        }
        String to = packetData.getTo();
        // 发送给所有人
        if (StringUtils.isBlank(to)) {
            if (StringUtils.isNotBlank(packetData.getEvent())) {
                server.getBroadcastOperations().sendEvent(packetData.getEvent(), packetData.getDataStr());
            }
            return;
        }
        // 发送给指定的人
        UUID $sessionId = userIdSessionMap.get(to);
        if ($sessionId == null) {
            LOG.info(String.format("sessionId is null, userId is %s", to));
            return;
        }
        SocketIOClient client = server.getClient($sessionId);
        if (client != null) {
           // if (client != null && client.isChannelOpen()) {
            if (StringUtils.isNotBlank(packetData.getEvent())) {
                LOG.info("packet to：" + to + ";data：" + packetData.toString());
                client.sendEvent(packetData.getEvent(), packetData.getDataStr());
            }
            return;
        }
        else {
            LOG.info(String.format("client is null or channel is not open, userId is %s", to));
        }
        // client已关闭 则退出；需要延时处理退出操作
        delayExit($sessionId);
    }

    public void delayExit(UUID sessionId) {
        if (sessionId == null) {
            return;
        }
        exit(sessionId);
    }


    public void checkSessionRemove() {
        synchronized (checkSessionRemoveMap) {
            if (checkSessionRemoveMap.size() > 0) {
                Set<Map.Entry<UUID, Long>> entries = checkSessionRemoveMap.entrySet();
                Iterator<Map.Entry<UUID, Long>> iterators = entries.iterator();
                while (iterators.hasNext()) {
                     Map.Entry<UUID, Long> entry = iterators.next();
                     long expireTimes = entry.getValue();
                     if(expireTimes > System.currentTimeMillis()) {
                        continue;
                     }
                     UUID oldSession = entry.getKey();
                     if (oldSession == null) {
                         continue;
                     }
                     SocketIOClient client = server.getClient(oldSession);
                     if (client == null || !client.isChannelOpen()) {
                         String userId = sessionUserIdMap.remove(oldSession.toString());
                         if (userId != null) {
                             LOG.info("user exit userId :" + userId + "; sessionId : "  + oldSession.toString());
                             UUID currentSession = userIdSessionMap.get(userId);
                             if (currentSession == null || currentSession.toString().equals(oldSession.toString())) {
                                 userIdSessionMap.remove(userId);
                                 try {
                                     // 清除数据
                                   unregisterRouter(Constants.ROUTER_KEY + userId);
                                 } catch (Exception e) {
                                     e.printStackTrace();
                                 }

                             }
                             sessionTokenMap.remove(oldSession.toString());
                         }
                     }
                     else {
                         LOG.info("session not need remove sessionId : " + oldSession.toString());
                     }
                    // 删除掉
                    iterators.remove();
                }
            }
        }
    }


    public void send(UUID sessionId,  PacketData packetData) {
        if (sessionId == null || packetData == null) {
            LOG.info("sessionId is null");
            return;
        }
        SocketIOClient client = server.getClient(sessionId);
        if (client == null) {

            LOG.info("client is null and sessionId is " + sessionId.toString() + "; packetData:" + packetData.toString());
            return;
        }
        if (StringUtils.isBlank(packetData.getEvent())) {
            LOG.error("packetData event is null");
            return;
        }
        try {
            LOG.info("packet sessionId：" + sessionId + ";data：" + packetData.toString());
            // 不验证client.isChannelOpen;
            client.sendEvent(packetData.getEvent(), packetData.getDataStr());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public String getUserId(UUID sessionId) {
       return sessionUserIdMap.get(sessionId.toString());
    }

    public boolean unregisterRouter(String userIdKey) {
        String router = stringRedisTemplate.opsForValue().get(userIdKey);
        if (StringUtils.isNotBlank(router)) {
            stringRedisTemplate.delete(userIdKey);
            stringRedisTemplate.boundValueOps("leader_server_load:" + router).increment(-1);
        }
        return true;
    }
}
