package com.tiantian.leader.socketio.dispatcher;

import com.rabbitmq.client.*;
import com.tiantian.leader.guice.GuiceManger;
import com.tiantian.leader.socketio.domain.PacketData;
import com.tiantian.leader.socketio.manager.IOClientsManager;
import com.tiantian.leader.socketio.settings.LeaderSocketConfig;
import com.tiantian.leader.socketio.settings.RabbitmqConfig;
import com.tiantian.leader.utils.PushUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import us.bpsm.edn.parser.Parseable;
import us.bpsm.edn.parser.Parser;
import us.bpsm.edn.parser.Parsers;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static us.bpsm.edn.parser.Parsers.defaultConfiguration;

/**
 *
 */
public class GameWorker implements Runnable {
    private Logger LOG = LoggerFactory.getLogger(GameWorker.class);
    private Connection connection;
    private Channel channel;
    private QueueingConsumer consumer;
    QueueingConsumer.Delivery delivery;

    public GameWorker(){
    }

    @Override
    public void run() {
        try {
            initWorker();
            createConsumer();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("consumer init work failed! " + e.toString());
        } catch (TimeoutException e) {
            e.printStackTrace();
            throw new RuntimeException("consumer init work failed! " + e.toString());
        }
        while (true) {
            try {
                delivery = consumer.nextDelivery();
            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException("consumer queue has been interrupted! " + e.toString());

            } catch (Exception e) {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                    throw new RuntimeException("consumer queue sleep has been interrupted! " + e.toString());
                }
                continue;
            }
            String message = new String(delivery.getBody());
            handleMessage(message);
            try {
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("consumer queue ack failed! " + e.toString());
            }
        }
    }

    public void initWorker() throws IOException, TimeoutException {
        RabbitmqConfig config = RabbitmqConfig.getInstance();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(config.getUsername());
        factory.setPassword(config.getPassword());
        factory.setVirtualHost(config.getVhost());
//        factory.setHost(config.getHost());
//        factory.setPort(config.getPort());
        factory.setAutomaticRecoveryEnabled(true);
        String[] hosts = config.getHost().split(",");
        Address[] addrs = new Address[hosts.length];
        for (int i = 0; i < hosts.length; i++) {
            String[] hostAndPort = hosts[i].split(":");
            addrs[i] = new Address(hostAndPort[0].trim(), Integer.parseInt(hostAndPort[1].trim()));
        }
        connection = factory.newConnection(addrs);
        channel = connection.createChannel();
        channel.queueDeclare(queueName(), true, false, false, null);
    }
    public void createConsumer() throws IOException {
        consumer = new QueueingConsumer(channel);
        boolean autoAck = false;
        channel.basicConsume(queueName(), autoAck, consumer);
    }


    public String queueName() {
        return "com.tiantian.queues.game." + LeaderSocketConfig.getInstance().getServerId();
    }

    public void handleMessage(String message) {
        String[] messages = message.split(":");
        if (messages.length != 4) {
            return;
        }
        String version = messages[0];
        String type = messages[1];
        String source = messages[2];
        String eDetail = messages[3];
        BASE64Decoder decoder = new BASE64Decoder();
        String userData = null;
        try {
            userData = new String(decoder.decodeBuffer(eDetail));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parseable pbr = Parsers.newParseable(userData);
        Parser p = Parsers.newParser(defaultConfiguration());
        PacketData packetData = new PacketData();
        Map map = (Map<?, ?>) p.nextValue(pbr);
        String event = (String)map.get("event");  // user_apply_credit
        String to = (String) map.get("to");
        packetData.setEvent(event);
        packetData.setTo(to);
        packetData.setDataStr((String) map.get("dataStr"));
        IOClientsManager ioClientsManager = GuiceManger.INSTANCE.get(IOClientsManager.class);
        ioClientsManager.send(packetData);
        // 通知ios
        if ("user_apply_credit".equalsIgnoreCase(event)) {
            try {
                PushUtils.instance().push("你有一条新的申请消息", to);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
