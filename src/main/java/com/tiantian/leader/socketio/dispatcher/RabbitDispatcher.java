package com.tiantian.leader.socketio.dispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 */
public class RabbitDispatcher {
    private Logger LOG = LoggerFactory.getLogger(RabbitDispatcher.class);
    private ExecutorService exec;
    private static final int THREAD_NUM = 5;

//    public static void main(String[] argv) {
//        SystemDispatcher dispatcher = new SystemDispatcher();
//        dispatcher.init(argv);
//        dispatcher.start();
//    }

    public void init() {
        exec = Executors.newFixedThreadPool(THREAD_NUM * 2);
    }

    public void start() {
        for (int i = 0; i < THREAD_NUM; i++) {
             exec.execute(new GameWorker());
        }

        exec.shutdown();
    }

    public void stop() {
        //try
        //{
        //    // awaitTermination返回false即超时会继续循环，返回true即线程池中的线程执行完成主线程跳出循环往下执行，每隔10秒循环一次
        //    while (!exec.awaitTermination(10, TimeUnit.SECONDS));
        //}
        //catch (InterruptedException e)
        //{
        //    e.printStackTrace();
        //}
        LOG.info("stoped");
    }

    public void destroy() {

    }
}
