package com.tiantian.leader.utils;

import com.alibaba.fastjson.JSON;
import com.tiantian.system.proxy_client.NotifierIface;
import com.tiantian.system.thrift.notifier.NoticeType;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class SmsUtils {

    public static String sendRegisterCode(String mobile, String shortCode) {

        String msg = "手机号码注册验证码：["+shortCode+"],5分钟内有效，如非本人操作，请忽略。\n";
        Map<String ,String> map = new HashMap<>();
        map.put("mobile", mobile);
        map.put("msgContent", msg);
        map.put("corpMsgId", "");
        map.put("ext", "");
        map.put("type", "register_leader_code");
        map.put("shortCode", shortCode);
        Map<String, String> messageMap = generate("single", JSON.toJSONString(map));
        try {
            // 分发到game的队列中
            NotifierIface.instance().iface().notify(NoticeType.SMS.getValue(), "LeaderService", messageMap);
            return msg;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String sendUpdatePwdCode(String mobile, String shortCode) {
        String msg = "修改密码验证码：["+shortCode+"],5分钟内有效，如非本人操作，请忽略。";
        Map<String ,String> map = new HashMap<>();
        map.put("mobile", mobile);
        map.put("msgContent", msg);
        map.put("corpMsgId", "");
        map.put("ext", "");
        map.put("type", "leader_update_pwd_code");
        map.put("shortCode", shortCode);
        Map<String, String> messageMap = generate("single", JSON.toJSONString(map));
        try {
            // 分发到game的队列中
            NotifierIface.instance().iface().notify(NoticeType.SMS.getValue(), "LeaderService", messageMap);
            return msg;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Map<String, String> generate(String type, String data) {
        Map<String, String> map = new HashMap<>();
        map.put("type", type);
        map.put("data", data);
        return map;
    }
}
