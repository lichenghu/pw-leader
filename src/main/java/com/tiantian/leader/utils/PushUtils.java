package com.tiantian.leader.utils;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import com.tiantian.leader.cache.LocalCache;
import com.tiantian.leader.model.LoginSession;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
/**
 *
 */
public class PushUtils {
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(4);
    private static BlockingQueue<Notice> blockingArrayQueue = new ArrayBlockingQueue<>(5000);
    private static AtomicBoolean started = new AtomicBoolean(false);

    private static class PushUtilsHolder {
        private final static PushUtils instance = new PushUtils();
    }

    public static PushUtils instance() {
        return PushUtilsHolder.instance;
    }

    private void start() {
            if (started.compareAndSet(false, true)) {

                new Thread(() -> {
                    while (true) {
                        try {
                            final Notice notice = blockingArrayQueue.take();
                            EXECUTOR_SERVICE.execute(() -> push2One(notice.content, notice.token, notice.platform));
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
    }

    private PushUtils () {
        InputStream fileInput  = this.getClass().getClassLoader().getResourceAsStream("config/apns.p12");
        InputStream fileInput2  = this.getClass().getClassLoader().getResourceAsStream("config/leader-dis.p12");
        try {
            service = APNS.newService().withCert(fileInput, PWD).withProductionDestination().build();
            enter_service = APNS.newService().withCert(fileInput2, ENTER_PWD).withProductionDestination().build();
        }finally {
            try {
                fileInput.close();
                fileInput2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


  //  private static final String PATH = "/Users/lichenghu/Workspace/pw-leader/src/main/resources/config/apns.p12";
    private String PWD = "winner";
    private ApnsService service ;
    private  String ENTER_PWD = "winner"; // 企业版
    private  ApnsService enter_service;

    private static class Notice {
        String content, token, platform;
        public Notice(String content, String token, String platform) {
            this.content = content;
            this.token = token;
            this.platform = platform;
        }
    }

    public void push(String content, String leaderId) {
        start();
        LoginSession session = LocalCache.getSessionById(leaderId);
        if (session == null) {
             return;
        }
        try {
            blockingArrayQueue.put(new Notice(content, session.getDeviceToke(), session.getPlatform()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void push2One(String content, String token, String platform) {
        String payload = APNS.newPayload().sound("default").badge(1).alertBody(content).build();
        if ("ios".equalsIgnoreCase(platform)) {
            ApnsNotification result = enter_service.push(token, payload);
        } else {
            ApnsNotification result = service.push(token, payload);
        }
//        try {
//            PushNotificationPayload pyLoad = new PushNotificationPayload();
//            pyLoad.addCustomAlertBody(content);
//            pyLoad.addBadge(1);
//            pyLoad.addSound("default");
//            long begin = System.currentTimeMillis();
//            PushedNotifications p = Push.payload(pyLoad, PATH, PWD, true, token);
//            PushedNotification pn = p.get(0);
//            System.out.println(pn);
//            System.out.println(System.currentTimeMillis() - begin);
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
