package com.tiantian.leader.utils;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.tiantian.leader.model.Vip;
import org.apache.commons.io.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class VipUtils {
    private static List<Vip> vipList;
    private static Map<Integer, Vip> mapVip;

    public static VipUtils INSTANCE = new VipUtils();

    private VipUtils () {
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream("config/vip.json");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        vipList = JSON.parseArray(configString, Vip.class);
        mapVip = Maps.newConcurrentMap();
        for (Vip vip : vipList) {
             mapVip.put(vip.getVipLvl(), vip);
        }
    }

    public List<Vip> get() {
        return vipList;
    }

    public Vip get(int vipLvl) {
        return mapVip.get(vipLvl);
    }
}
