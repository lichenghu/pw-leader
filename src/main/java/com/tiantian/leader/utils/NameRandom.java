package com.tiantian.leader.utils;

import java.util.Random;

/**
 *
 */
public class NameRandom {
    public static final String[] NAME_STRS = new String[] {
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x" ,"y", "z",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
    };


    public static String randomName() {
        Random random = new Random();
        String name = "PW";
        for (int i = 0; i < 6; i++) {
            int index = random.nextInt(NAME_STRS.length);
            name += NAME_STRS[index];
        }
        return name;
    }
}
