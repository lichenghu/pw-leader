/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.tiantian.clubmtt.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (0.9.3)", date = "2017-02-07")
public class MttJoinOrCancelGameResult implements org.apache.thrift.TBase<MttJoinOrCancelGameResult, MttJoinOrCancelGameResult._Fields>, java.io.Serializable, Cloneable, Comparable<MttJoinOrCancelGameResult> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("MttJoinOrCancelGameResult");

  private static final org.apache.thrift.protocol.TField STATUS_FIELD_DESC = new org.apache.thrift.protocol.TField("status", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField ERR_MSG_FIELD_DESC = new org.apache.thrift.protocol.TField("errMsg", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField GAME_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("gameId", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField LEFT_USERS_FIELD_DESC = new org.apache.thrift.protocol.TField("leftUsers", org.apache.thrift.protocol.TType.I32, (short)4);
  private static final org.apache.thrift.protocol.TField IS_SIGN_DELAY_FIELD_DESC = new org.apache.thrift.protocol.TField("isSignDelay", org.apache.thrift.protocol.TType.BOOL, (short)5);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new MttJoinOrCancelGameResultStandardSchemeFactory());
    schemes.put(TupleScheme.class, new MttJoinOrCancelGameResultTupleSchemeFactory());
  }

  public String status; // required
  public String errMsg; // required
  public String gameId; // required
  public int leftUsers; // required
  public boolean isSignDelay; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    STATUS((short)1, "status"),
    ERR_MSG((short)2, "errMsg"),
    GAME_ID((short)3, "gameId"),
    LEFT_USERS((short)4, "leftUsers"),
    IS_SIGN_DELAY((short)5, "isSignDelay");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // STATUS
          return STATUS;
        case 2: // ERR_MSG
          return ERR_MSG;
        case 3: // GAME_ID
          return GAME_ID;
        case 4: // LEFT_USERS
          return LEFT_USERS;
        case 5: // IS_SIGN_DELAY
          return IS_SIGN_DELAY;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __LEFTUSERS_ISSET_ID = 0;
  private static final int __ISSIGNDELAY_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.STATUS, new org.apache.thrift.meta_data.FieldMetaData("status", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.ERR_MSG, new org.apache.thrift.meta_data.FieldMetaData("errMsg", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.GAME_ID, new org.apache.thrift.meta_data.FieldMetaData("gameId", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.LEFT_USERS, new org.apache.thrift.meta_data.FieldMetaData("leftUsers", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.IS_SIGN_DELAY, new org.apache.thrift.meta_data.FieldMetaData("isSignDelay", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.BOOL)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(MttJoinOrCancelGameResult.class, metaDataMap);
  }

  public MttJoinOrCancelGameResult() {
  }

  public MttJoinOrCancelGameResult(
    String status,
    String errMsg,
    String gameId,
    int leftUsers,
    boolean isSignDelay)
  {
    this();
    this.status = status;
    this.errMsg = errMsg;
    this.gameId = gameId;
    this.leftUsers = leftUsers;
    setLeftUsersIsSet(true);
    this.isSignDelay = isSignDelay;
    setIsSignDelayIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public MttJoinOrCancelGameResult(MttJoinOrCancelGameResult other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetStatus()) {
      this.status = other.status;
    }
    if (other.isSetErrMsg()) {
      this.errMsg = other.errMsg;
    }
    if (other.isSetGameId()) {
      this.gameId = other.gameId;
    }
    this.leftUsers = other.leftUsers;
    this.isSignDelay = other.isSignDelay;
  }

  public MttJoinOrCancelGameResult deepCopy() {
    return new MttJoinOrCancelGameResult(this);
  }

  @Override
  public void clear() {
    this.status = null;
    this.errMsg = null;
    this.gameId = null;
    setLeftUsersIsSet(false);
    this.leftUsers = 0;
    setIsSignDelayIsSet(false);
    this.isSignDelay = false;
  }

  public String getStatus() {
    return this.status;
  }

  public MttJoinOrCancelGameResult setStatus(String status) {
    this.status = status;
    return this;
  }

  public void unsetStatus() {
    this.status = null;
  }

  /** Returns true if field status is set (has been assigned a value) and false otherwise */
  public boolean isSetStatus() {
    return this.status != null;
  }

  public void setStatusIsSet(boolean value) {
    if (!value) {
      this.status = null;
    }
  }

  public String getErrMsg() {
    return this.errMsg;
  }

  public MttJoinOrCancelGameResult setErrMsg(String errMsg) {
    this.errMsg = errMsg;
    return this;
  }

  public void unsetErrMsg() {
    this.errMsg = null;
  }

  /** Returns true if field errMsg is set (has been assigned a value) and false otherwise */
  public boolean isSetErrMsg() {
    return this.errMsg != null;
  }

  public void setErrMsgIsSet(boolean value) {
    if (!value) {
      this.errMsg = null;
    }
  }

  public String getGameId() {
    return this.gameId;
  }

  public MttJoinOrCancelGameResult setGameId(String gameId) {
    this.gameId = gameId;
    return this;
  }

  public void unsetGameId() {
    this.gameId = null;
  }

  /** Returns true if field gameId is set (has been assigned a value) and false otherwise */
  public boolean isSetGameId() {
    return this.gameId != null;
  }

  public void setGameIdIsSet(boolean value) {
    if (!value) {
      this.gameId = null;
    }
  }

  public int getLeftUsers() {
    return this.leftUsers;
  }

  public MttJoinOrCancelGameResult setLeftUsers(int leftUsers) {
    this.leftUsers = leftUsers;
    setLeftUsersIsSet(true);
    return this;
  }

  public void unsetLeftUsers() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __LEFTUSERS_ISSET_ID);
  }

  /** Returns true if field leftUsers is set (has been assigned a value) and false otherwise */
  public boolean isSetLeftUsers() {
    return EncodingUtils.testBit(__isset_bitfield, __LEFTUSERS_ISSET_ID);
  }

  public void setLeftUsersIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __LEFTUSERS_ISSET_ID, value);
  }

  public boolean isIsSignDelay() {
    return this.isSignDelay;
  }

  public MttJoinOrCancelGameResult setIsSignDelay(boolean isSignDelay) {
    this.isSignDelay = isSignDelay;
    setIsSignDelayIsSet(true);
    return this;
  }

  public void unsetIsSignDelay() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __ISSIGNDELAY_ISSET_ID);
  }

  /** Returns true if field isSignDelay is set (has been assigned a value) and false otherwise */
  public boolean isSetIsSignDelay() {
    return EncodingUtils.testBit(__isset_bitfield, __ISSIGNDELAY_ISSET_ID);
  }

  public void setIsSignDelayIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __ISSIGNDELAY_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case STATUS:
      if (value == null) {
        unsetStatus();
      } else {
        setStatus((String)value);
      }
      break;

    case ERR_MSG:
      if (value == null) {
        unsetErrMsg();
      } else {
        setErrMsg((String)value);
      }
      break;

    case GAME_ID:
      if (value == null) {
        unsetGameId();
      } else {
        setGameId((String)value);
      }
      break;

    case LEFT_USERS:
      if (value == null) {
        unsetLeftUsers();
      } else {
        setLeftUsers((Integer)value);
      }
      break;

    case IS_SIGN_DELAY:
      if (value == null) {
        unsetIsSignDelay();
      } else {
        setIsSignDelay((Boolean)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case STATUS:
      return getStatus();

    case ERR_MSG:
      return getErrMsg();

    case GAME_ID:
      return getGameId();

    case LEFT_USERS:
      return getLeftUsers();

    case IS_SIGN_DELAY:
      return isIsSignDelay();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case STATUS:
      return isSetStatus();
    case ERR_MSG:
      return isSetErrMsg();
    case GAME_ID:
      return isSetGameId();
    case LEFT_USERS:
      return isSetLeftUsers();
    case IS_SIGN_DELAY:
      return isSetIsSignDelay();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof MttJoinOrCancelGameResult)
      return this.equals((MttJoinOrCancelGameResult)that);
    return false;
  }

  public boolean equals(MttJoinOrCancelGameResult that) {
    if (that == null)
      return false;

    boolean this_present_status = true && this.isSetStatus();
    boolean that_present_status = true && that.isSetStatus();
    if (this_present_status || that_present_status) {
      if (!(this_present_status && that_present_status))
        return false;
      if (!this.status.equals(that.status))
        return false;
    }

    boolean this_present_errMsg = true && this.isSetErrMsg();
    boolean that_present_errMsg = true && that.isSetErrMsg();
    if (this_present_errMsg || that_present_errMsg) {
      if (!(this_present_errMsg && that_present_errMsg))
        return false;
      if (!this.errMsg.equals(that.errMsg))
        return false;
    }

    boolean this_present_gameId = true && this.isSetGameId();
    boolean that_present_gameId = true && that.isSetGameId();
    if (this_present_gameId || that_present_gameId) {
      if (!(this_present_gameId && that_present_gameId))
        return false;
      if (!this.gameId.equals(that.gameId))
        return false;
    }

    boolean this_present_leftUsers = true;
    boolean that_present_leftUsers = true;
    if (this_present_leftUsers || that_present_leftUsers) {
      if (!(this_present_leftUsers && that_present_leftUsers))
        return false;
      if (this.leftUsers != that.leftUsers)
        return false;
    }

    boolean this_present_isSignDelay = true;
    boolean that_present_isSignDelay = true;
    if (this_present_isSignDelay || that_present_isSignDelay) {
      if (!(this_present_isSignDelay && that_present_isSignDelay))
        return false;
      if (this.isSignDelay != that.isSignDelay)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_status = true && (isSetStatus());
    list.add(present_status);
    if (present_status)
      list.add(status);

    boolean present_errMsg = true && (isSetErrMsg());
    list.add(present_errMsg);
    if (present_errMsg)
      list.add(errMsg);

    boolean present_gameId = true && (isSetGameId());
    list.add(present_gameId);
    if (present_gameId)
      list.add(gameId);

    boolean present_leftUsers = true;
    list.add(present_leftUsers);
    if (present_leftUsers)
      list.add(leftUsers);

    boolean present_isSignDelay = true;
    list.add(present_isSignDelay);
    if (present_isSignDelay)
      list.add(isSignDelay);

    return list.hashCode();
  }

  @Override
  public int compareTo(MttJoinOrCancelGameResult other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetStatus()).compareTo(other.isSetStatus());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetStatus()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.status, other.status);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetErrMsg()).compareTo(other.isSetErrMsg());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetErrMsg()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.errMsg, other.errMsg);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetGameId()).compareTo(other.isSetGameId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetGameId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.gameId, other.gameId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetLeftUsers()).compareTo(other.isSetLeftUsers());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetLeftUsers()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.leftUsers, other.leftUsers);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetIsSignDelay()).compareTo(other.isSetIsSignDelay());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetIsSignDelay()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.isSignDelay, other.isSignDelay);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("MttJoinOrCancelGameResult(");
    boolean first = true;

    sb.append("status:");
    if (this.status == null) {
      sb.append("null");
    } else {
      sb.append(this.status);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("errMsg:");
    if (this.errMsg == null) {
      sb.append("null");
    } else {
      sb.append(this.errMsg);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("gameId:");
    if (this.gameId == null) {
      sb.append("null");
    } else {
      sb.append(this.gameId);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("leftUsers:");
    sb.append(this.leftUsers);
    first = false;
    if (!first) sb.append(", ");
    sb.append("isSignDelay:");
    sb.append(this.isSignDelay);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class MttJoinOrCancelGameResultStandardSchemeFactory implements SchemeFactory {
    public MttJoinOrCancelGameResultStandardScheme getScheme() {
      return new MttJoinOrCancelGameResultStandardScheme();
    }
  }

  private static class MttJoinOrCancelGameResultStandardScheme extends StandardScheme<MttJoinOrCancelGameResult> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, MttJoinOrCancelGameResult struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // STATUS
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.status = iprot.readString();
              struct.setStatusIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // ERR_MSG
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.errMsg = iprot.readString();
              struct.setErrMsgIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // GAME_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.gameId = iprot.readString();
              struct.setGameIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // LEFT_USERS
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.leftUsers = iprot.readI32();
              struct.setLeftUsersIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // IS_SIGN_DELAY
            if (schemeField.type == org.apache.thrift.protocol.TType.BOOL) {
              struct.isSignDelay = iprot.readBool();
              struct.setIsSignDelayIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, MttJoinOrCancelGameResult struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.status != null) {
        oprot.writeFieldBegin(STATUS_FIELD_DESC);
        oprot.writeString(struct.status);
        oprot.writeFieldEnd();
      }
      if (struct.errMsg != null) {
        oprot.writeFieldBegin(ERR_MSG_FIELD_DESC);
        oprot.writeString(struct.errMsg);
        oprot.writeFieldEnd();
      }
      if (struct.gameId != null) {
        oprot.writeFieldBegin(GAME_ID_FIELD_DESC);
        oprot.writeString(struct.gameId);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(LEFT_USERS_FIELD_DESC);
      oprot.writeI32(struct.leftUsers);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(IS_SIGN_DELAY_FIELD_DESC);
      oprot.writeBool(struct.isSignDelay);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class MttJoinOrCancelGameResultTupleSchemeFactory implements SchemeFactory {
    public MttJoinOrCancelGameResultTupleScheme getScheme() {
      return new MttJoinOrCancelGameResultTupleScheme();
    }
  }

  private static class MttJoinOrCancelGameResultTupleScheme extends TupleScheme<MttJoinOrCancelGameResult> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, MttJoinOrCancelGameResult struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetStatus()) {
        optionals.set(0);
      }
      if (struct.isSetErrMsg()) {
        optionals.set(1);
      }
      if (struct.isSetGameId()) {
        optionals.set(2);
      }
      if (struct.isSetLeftUsers()) {
        optionals.set(3);
      }
      if (struct.isSetIsSignDelay()) {
        optionals.set(4);
      }
      oprot.writeBitSet(optionals, 5);
      if (struct.isSetStatus()) {
        oprot.writeString(struct.status);
      }
      if (struct.isSetErrMsg()) {
        oprot.writeString(struct.errMsg);
      }
      if (struct.isSetGameId()) {
        oprot.writeString(struct.gameId);
      }
      if (struct.isSetLeftUsers()) {
        oprot.writeI32(struct.leftUsers);
      }
      if (struct.isSetIsSignDelay()) {
        oprot.writeBool(struct.isSignDelay);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, MttJoinOrCancelGameResult struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(5);
      if (incoming.get(0)) {
        struct.status = iprot.readString();
        struct.setStatusIsSet(true);
      }
      if (incoming.get(1)) {
        struct.errMsg = iprot.readString();
        struct.setErrMsgIsSet(true);
      }
      if (incoming.get(2)) {
        struct.gameId = iprot.readString();
        struct.setGameIdIsSet(true);
      }
      if (incoming.get(3)) {
        struct.leftUsers = iprot.readI32();
        struct.setLeftUsersIsSet(true);
      }
      if (incoming.get(4)) {
        struct.isSignDelay = iprot.readBool();
        struct.setIsSignDelayIsSet(true);
      }
    }
  }

}

