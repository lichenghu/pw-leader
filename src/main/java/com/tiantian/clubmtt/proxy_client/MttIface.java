package com.tiantian.clubmtt.proxy_client;

import com.tiantian.framework.thrift.client.ClientPool;
import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.clubmtt.settings.ClubMttConfig;
import com.tiantian.clubmtt.thrift.MttService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class MttIface extends IFace<MttService.Iface> {
    private static class MttIfaceHolder {
        private final static MttIface instance = new MttIface();
    }

    private MttIface() {
    }
    public static MttIface instance() {
        return MttIfaceHolder.instance;
    }

    @Override
    protected TServiceClient createClient(TProtocol tProtocol) {
        return new MttService.Client(tProtocol);
    }

    @Override
    protected ClientPool createPool() {
        return new ClientPool(ClubMttConfig.getInstance().getHost(), ClubMttConfig.getInstance().getPort());
    }

    @Override
    protected Class<MttService.Iface> faceClass() {
        return MttService.Iface.class;
    }
}
