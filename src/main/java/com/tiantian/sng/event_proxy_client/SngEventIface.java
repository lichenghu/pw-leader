package com.tiantian.sng.event_proxy_client;

import com.tiantian.framework.thrift.client.ClientPool;
import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.sng.settings.SngEventConfig;
import com.tiantian.sng.thrift.event.SngEventService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class SngEventIface extends IFace<SngEventService.Iface> {

    private static class SngEventIfaceHolder {
        private final static SngEventIface instance = new SngEventIface();
    }
    private SngEventIface() {
    }

    public static SngEventIface instance() {
        return SngEventIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new SngEventService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new ClientPool(SngEventConfig.getInstance().getHost(),
                SngEventConfig.getInstance().getPort());
    }

    @Override
    protected Class<SngEventService.Iface> faceClass() {
        return SngEventService.Iface.class;
    }
}
