package com.tiantian.stranger.proxy_client;

import com.tiantian.framework.thrift.client.ClientPool;
import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.stranger.settings.StrangerEventConfig;
import com.tiantian.stranger.thrift.event.StrangerEventService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;


/**
 *
 */
public class StrangerEventIface extends IFace<StrangerEventService.Iface>{
    private static class StrangerEventIfaceHolder {
        private final static StrangerEventIface instance = new StrangerEventIface();
    }
    private StrangerEventIface() {
    }

    public static StrangerEventIface instance() {
        return StrangerEventIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new StrangerEventService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new ClientPool(StrangerEventConfig.getInstance().getHost(),
                StrangerEventConfig.getInstance().getPort());
    }

    @Override
    protected Class<StrangerEventService.Iface> faceClass() {
        return StrangerEventService.Iface.class;
    }
}
