package thrfit;

import com.tiantian.fc.thrift.core.FriendCoreService;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

/**
 *
 */
public class ClientUtils {
    private static FriendCoreService.Iface clientProxy;

    private static class ClientUtilsHolder {
        private final static ClientUtils instance = new ClientUtils();
    }

    private ClientUtils() {
        ClassLoader classLoader = FriendCoreService.Iface.class.getClassLoader();
        clientProxy = (FriendCoreService.Iface) Proxy.newProxyInstance(classLoader,
                new Class[]{FriendCoreService.Iface.class},
                (proxy, method, args) -> {
                    TSocket ttSocket =  ClientPool.getInstance().borrowObject();
                    try {
                        TFastFramedTransport fastTransport = new TFastFramedTransport(ttSocket);
                        TCompactProtocol protocol = new TCompactProtocol(fastTransport);
                        FriendCoreService.Client client = new FriendCoreService.Client(protocol);
                        //设置成可以访问
                        method.setAccessible(true);
                        return method.invoke(client, args);
                    }
                    catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e ) {
                        if (e instanceof InvocationTargetException)  {
                            Throwable throwable = ((InvocationTargetException)e).getTargetException();
                            throwable.printStackTrace();
                            throw throwable;
                        }
                        e.printStackTrace();
                        throw e;
                    }
                    finally {
                         ClientPool.getInstance().returnObject(ttSocket);
                    }
                });
    }

    public static ClientUtils instance() {
        return ClientUtilsHolder.instance;
    }

    public FriendCoreService.Iface iface() {
        return clientProxy;
    }


}
