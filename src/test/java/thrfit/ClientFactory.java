package thrfit;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class ClientFactory extends BasePooledObjectFactory<TSocket> {
    private Logger LOG = LoggerFactory.getLogger(ClientFactory.class);
    private static final Set<Integer> RESTARTABLE_CAUSES = new HashSet<Integer>(
            Arrays.asList(TTransportException.NOT_OPEN, TTransportException.END_OF_FILE, TTransportException.TIMED_OUT,
                    TTransportException.UNKNOWN));

    private String host;
    private int port;

    public ClientFactory(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public TSocket create() throws Exception {
        TSocket transport = new TSocket(host, port);
        transport.open();
        return transport;
    }

    @Override
    public PooledObject<TSocket> wrap(TSocket obj) {
        return new DefaultPooledObject<>(obj);
    }

    public void activateObject(PooledObject<TSocket> p) throws Exception {
        TSocket t = p.getObject();
        Socket s = t.getSocket();
        try {
            s.sendUrgentData(0xFF);
        }
        catch (Exception e) {
            e.printStackTrace();
            super.destroyObject(p);
        }

    }

    @Override
    public boolean validateObject(PooledObject<TSocket> p) {
        TSocket t = p.getObject();
        boolean isValid = false;
        if (null != t) {
            Socket s = t.getSocket();
            try {
                s.sendUrgentData(0xFF);
                isValid = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                isValid = false;
                if (e instanceof TTransportException) {
                    TTransportException te = (TTransportException) e;
                    if (RESTARTABLE_CAUSES.contains(te.getType())) {
                        reconnectOrThrowException(t);
                        try {
                            s.sendUrgentData(0xFF);
                            isValid = true;
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            isValid = false;
                        }
                    }
                } else if (e instanceof ConnectException) {
                    reconnectOrThrowException(t);
                    try {
                        s.sendUrgentData(0xFF);
                        isValid = true;
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        isValid = false;
                    }
                }
            }
        }
        return isValid;
    }

    private void reconnectOrThrowException(TTransport transport) {
        LOG.info("thrift client reconnect");
        transport.close();
        try {
            transport.open();
        }
        catch (TTransportException e) {
            e.printStackTrace();
        }
    }
}
