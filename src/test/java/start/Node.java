package start;

/**
 *
 */
public class Node implements Comparable<Node>{
    public Coord coord; //坐标
    public Node parent;
    public int G; // G 是一个准确的值,是起点到当前节点的代价
    public int H; // H 是个估值, 当前节点到目的节点的估计代价

    public Node(int x, int y) {
        this.coord = new Coord(x, y);
    }

    public Node(Coord coord, Node parent, int G, int H) {
        this.coord = coord;
        this.parent = parent;
        this.G = G;
        this.H = H;
    }

    @Override
    public int compareTo(Node o) {
        if (o == null) {
            return -1;
        }
        if (G + H > o.G + o.H) {
            return 1;
        } else if (G + H < o.G + o.H) {
            return -1;
        }
        return 0;
    }
}
